#!/usr/bin/env python3

import os
import sys

import gitlab

approvals = set()

if circumvent_reason := os.getenv("CI_BREAK_GLASS_REASON"):
    print(f"Circumventing approval check.\nReason: {circumvent_reason}")
    sys.exit(0)

gl = gitlab.Gitlab("https://gitlab.wikimedia.org", private_token=os.environ["GITLAB_CI_API_TOKEN"])
project = gl.projects.get(os.environ["CI_MERGE_REQUEST_PROJECT_PATH"])
mr = project.mergerequests.get(int(os.environ["CI_MERGE_REQUEST_IID"]))
author = mr.author["username"]
for discussion in mr.discussions.list(get_all=True):
    for note in discussion.asdict()["notes"]:
        if note["system"] is True and note["body"] == "approved this merge request":
            approver = note["author"]["username"]
            if approver != author:
                approvals.add(approver)

if not approvals:
    print("An approval is required to merge this MR")
    sys.exit(1)
