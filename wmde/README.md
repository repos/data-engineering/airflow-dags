# [WMDE](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)

DAGs maintained by the Software Analytics team at Wikimedia Deutschland.

### Helpful Links

- The associated DAG jobs: [repos/wmde/analytics/hql](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql)
- The tests for these DAGs: [airflow-dags/tests/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/tests/wmde)
- To report an issue or make a request: [WMDE SWE Analytics on Phabricator](https://phabricator.wikimedia.org/project/view/5408/)
