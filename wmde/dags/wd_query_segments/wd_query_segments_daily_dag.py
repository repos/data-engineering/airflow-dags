"""
Derives daily partitions of Wikidata Query service queries based on various criteria.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_query_segments
"""

import os
from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPLETE,
    COMPUTE,
    DAILY,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    PARTITIONS,
    WAIT_FOR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.rest_external_task import RestExternalTaskSensor

# Note: Get Certificate Authority bundle for TLS encryption using the CA public key.
os.environ["REQUESTS_CA_BUNDLE"] = "/etc/ssl/certs/ca-certificates.crt"

# MARK: Identifiers

# Note: Upstream DAG
# See: https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/search/dags/process_sparql_query.py
PROCESS_SPARQL_QUERY_HOURLY = "process_sparql_query_hourly"

WD_QUERY_SEGMENTS = "wd_query_segments"  # task id
WD_QUERY_SEGMENTS_DAILY = f"{WD_QUERY_SEGMENTS}_{DAILY}"  # DAG id
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WD_QUERY_SEGMENTS}"

# MARK: Properties

props = DagProperties(
    # HDFS source table:
    hive_discovery_processed_external_sparql_query="discovery.processed_external_sparql_query",
    # HDFS destination table:
    hive_wmde_wd_query_segments_daily=f"wmde.{WD_QUERY_SEGMENTS_DAILY}",
    # Task Hive query:
    hql_wd_query_segments_daily=f"{GITLAB_HQL_TASK_DIR}/{WD_QUERY_SEGMENTS_DAILY}.hql",
    # TMP export directory:
    tmp_dir_wd_query_segments_daily=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_QUERY_SEGMENTS_DAILY}",
    # TMP export query:
    hql_gen_csv_wd_query_segments_daily=f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WD_QUERY_SEGMENTS_DAILY}.hql",
    # Archive export directory:
    pub_data_dir_wd_query_segments_daily=(f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_QUERY_SEGMENTS_DAILY}"),
    # Archive export file:
    archive_csv_wd_query_segments_daily=f"{WD_QUERY_SEGMENTS_DAILY}.csv",
    # Metadata:
    start_date=datetime(2024, 7, 1),
    sla=timedelta(hours=12),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "from_hive",
        "requires_discovery_processed_external_sparql_query",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=WD_QUERY_SEGMENTS_DAILY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    sensor = RestExternalTaskSensor(
        task_id=f"{WAIT_FOR}_{PROCESS_SPARQL_QUERY_HOURLY}_{PARTITIONS}",
        external_instance_uri="https://airflow-search.discovery.wmnet:30443",
        external_dag_id=PROCESS_SPARQL_QUERY_HOURLY,
        external_task_id=COMPLETE,
        check_existence=True,
    )

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_QUERY_SEGMENTS_DAILY}",
        sql=props.hql_wd_query_segments_daily,
        query_parameters={
            "source_table": props.hive_discovery_processed_external_sparql_query,
            "destination_table": props.hive_wmde_wd_query_segments_daily,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    # gen_csv = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_QUERY_SEGMENTS_DAILY}",
    #     sql=props.hql_gen_csv_wd_query_segments_daily,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_query_segments_daily,
    #         "destination_directory": props.tmp_dir_wd_query_segments_daily,
    #     },
    # )

    # MARK: Archive Datasets

    # archive_csv = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_QUERY_SEGMENTS_DAILY}",
    #     source_directory=props.tmp_dir_wd_query_segments_daily,
    #     archive_file=props.pub_data_dir_wd_query_segments_daily + "/" + props.archive_csv_wd_query_segments_daily,
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # MARK: Execute DAG

    sensor >> compute  # >> gen_csv >> archive_csv
