"""
Derives monthly aggregates of Wikidata edits based on the device type of the user.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_device_type_edits
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPUTE,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HADOOP_NAME_NODE,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    INTERVAL_START_TO_MONTH,
    MONTHLY,
    PARTITIONS,
    WAIT_FOR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.url import URLSensor

# MARK: Identifiers

WD_DEVICE_TYPE_EDITS = "wd_device_type_edits"  # task id
WD_DEVICE_TYPE_EDITS_MONTHLY = f"{WD_DEVICE_TYPE_EDITS}_{MONTHLY}"  # DAG id
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WD_DEVICE_TYPE_EDITS}"

# MARK: Properties

props = DagProperties(
    # HDFS source tables:
    hive_event_mediawiki_page_change_v1="event.mediawiki_page_change_v1",
    hive_event_mediawiki_revision_tags_change="event.mediawiki_revision_tags_change",
    hive_wmf_raw_mediawiki_private_cu_changes="wmf_raw.mediawiki_private_cu_changes",
    # HDFS destination table:
    hive_wmde_wd_device_type_edits_monthly=f"wmde.{WD_DEVICE_TYPE_EDITS_MONTHLY}",
    # Task Hive query:
    hql_wd_device_type_edits_monthly=f"{GITLAB_HQL_TASK_DIR}/{WD_DEVICE_TYPE_EDITS_MONTHLY}.hql",
    # TMP export directory:
    tmp_dir_wd_device_type_edits_monthly=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_DEVICE_TYPE_EDITS_MONTHLY}",
    # TMP export query:
    hql_gen_csv_wd_device_type_edits_monthly=f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WD_DEVICE_TYPE_EDITS_MONTHLY}.hql",
    # Archive export directory:
    pub_data_dir_wd_device_type_edits_monthly=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_DEVICE_TYPE_EDITS_MONTHLY}"
    ),
    # Archive export file:
    archive_csv_wd_device_type_edits_monthly=f"{WD_DEVICE_TYPE_EDITS_MONTHLY}.csv",
    # Metadata:
    start_date=datetime(2024, 8, 1),
    sla=timedelta(days=2),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "from_hive",
        "monthly",
        "requires_event_mediawiki_page_change_v1",
        "requires_event_mediawiki_revision_tags_change",
        "requires_wmf_raw_mediawiki_private_cu_changes",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=WD_DEVICE_TYPE_EDITS_MONTHLY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensors

    sensors = []
    sensors.append(dataset("hive_event_mediawiki_page_change_v1").get_sensor_for(dag))
    sensors.append(dataset("hive_event_mediawiki_revision_tags_change").get_sensor_for(dag))
    sensors.append(
        URLSensor(
            task_id=f"{WAIT_FOR}_{props.hive_wmf_raw_mediawiki_private_cu_changes.replace('.', '_')}_{PARTITIONS}",
            url=(
                f"{HADOOP_NAME_NODE}/wmf/data/raw/mediawiki_private/tables/cu_changes"
                f"/month={INTERVAL_START_TO_MONTH}/_PARTITIONED"
            ),
            poke_interval=timedelta(hours=1).total_seconds(),
        )
    )

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_DEVICE_TYPE_EDITS_MONTHLY}",
        sql=props.hql_wd_device_type_edits_monthly,
        query_parameters={
            "source_mw_page_change_v1": props.hive_event_mediawiki_page_change_v1,
            "source_mw_revision_tags_change": props.hive_event_mediawiki_revision_tags_change,
            "source_mw_private_cu_changes": props.hive_wmf_raw_mediawiki_private_cu_changes,
            "destination_table": props.hive_wmde_wd_device_type_edits_monthly,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    # gen_csv = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_DEVICE_TYPE_EDITS_MONTHLY}",
    #     sql=props.hql_gen_csv_wd_device_type_edits_monthly,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_device_type_edits_monthly,
    #         "destination_directory": props.tmp_dir_wd_device_type_edits_monthly,
    #     },
    # )

    # MARK: Archive Datasets

    # archive_csv = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_DEVICE_TYPE_EDITS_MONTHLY}",
    #     source_directory=props.tmp_dir_wd_device_type_edits_monthly,
    #     archive_file=(
    #         props.pub_data_dir_wd_device_type_edits_monthly + "/" + props.archive_csv_wd_device_type_edits_monthly
    #     ),
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # MARK: Execute DAG

    sensors >> compute  # >> gen_csv >> archive_csv
