"""
Derives monthly counts of Wikidata coeditors per Wikimedia wiki and distinct over all wikis.

# Attn: This DAG relies on a temporary table. Separate runs cannot run in parallel as data will leak or be deleted.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_coeditors
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPUTE,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    MONTHLY,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

WD_COEDITORS = "wd_coeditors"  # task id
WD_COEDITORS_MONTHLY = f"{WD_COEDITORS}_{MONTHLY}"  # DAG id
WD_COEDITORS_BY_WIKI_MONTHLY = f"{WD_COEDITORS}_by_wiki_{MONTHLY}"  # output id
WD_COEDITORS_DISTINCT_MONTHLY = f"{WD_COEDITORS}_distinct_{MONTHLY}"  # output id
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WD_COEDITORS}"

# MARK: Properties

props = DagProperties(
    # HDFS source table:
    hive_wmf_mediawiki_history="wmf.mediawiki_history",
    # HDFS temporary results table:
    tmp_mwh_wiki_editor_activity_flags_monthly="wmde.tmp_mwh_wiki_editor_activity_flags_monthly",
    # HDFS destination tables:
    hive_wmde_wd_coeditors_by_wiki_monthly=f"wmde.{WD_COEDITORS_BY_WIKI_MONTHLY}",
    hive_wmde_wd_coeditors_distinct_monthly=f"wmde.{WD_COEDITORS_DISTINCT_MONTHLY}",
    # Task Hive query:
    hql_wd_coeditors_monthly=f"{GITLAB_HQL_TASK_DIR}/{WD_COEDITORS_MONTHLY}.hql",
    # TMP export directories:
    tmp_dir_wd_coeditors_by_wiki_monthly=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_COEDITORS_BY_WIKI_MONTHLY}",
    tmp_dir_wd_coeditors_distinct_monthly=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_COEDITORS_DISTINCT_MONTHLY}",
    # TMP export queries:
    hql_gen_csv_wd_coeditors_by_wiki_monthly=(f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WD_COEDITORS_BY_WIKI_MONTHLY}.hql"),
    hql_gen_csv_wd_coeditors_distinct_monthly=(f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WD_COEDITORS_DISTINCT_MONTHLY}.hql"),
    # Archive export directories:
    pub_data_dir_wd_coeditors_by_wiki_monthly=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_COEDITORS_BY_WIKI_MONTHLY}"
    ),
    pub_data_dir_wd_coeditors_distinct_monthly=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_COEDITORS_DISTINCT_MONTHLY}"
    ),
    # Archive export files:
    archive_csv_wd_coeditors_by_wiki_monthly=f"{WD_COEDITORS_BY_WIKI_MONTHLY}.csv",
    archive_csv_wd_coeditors_distinct_monthly=f"{WD_COEDITORS_DISTINCT_MONTHLY}.csv",
    # Metadata:
    start_date=datetime(2024, 6, 1),
    sla=timedelta(days=2),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "from_hive",
        "monthly",
        "requires_wmf_mediawiki_history",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=WD_COEDITORS_MONTHLY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    sensor = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_COEDITORS_MONTHLY}",
        sql=props.hql_wd_coeditors_monthly,
        query_parameters={
            "source_table": props.hive_wmf_mediawiki_history,
            "tmp_mwh_wiki_editor_activity_flags_monthly": props.tmp_mwh_wiki_editor_activity_flags_monthly,
            "dest_hive_wmde_wd_coeditors_by_wiki_monthly": props.hive_wmde_wd_coeditors_by_wiki_monthly,
            "dest_hive_wmde_wd_coeditors_distinct_monthly": props.hive_wmde_wd_coeditors_distinct_monthly,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    # gen_csv_by_wiki = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_COEDITORS_BY_WIKI_MONTHLY}",
    #     sql=props.hql_gen_csv_wd_coeditors_by_wiki_monthly,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_coeditors_by_wiki_monthly,
    #         "destination_directory": props.tmp_dir_wd_coeditors_by_wiki_monthly,
    #     },
    # )

    # gen_csv_distinct = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_COEDITORS_DISTINCT_MONTHLY}",
    #     sql=props.hql_gen_csv_wd_coeditors_distinct_monthly,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_coeditors_distinct_monthly,
    #         "destination_directory": props.tmp_dir_wd_coeditors_distinct_monthly,
    #     },
    # )

    # MARK: Archive Datasets

    # archive_csv_by_wiki = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_COEDITORS_BY_WIKI_MONTHLY}",
    #     source_directory=props.tmp_dir_wd_coeditors_by_wiki_monthly,
    #     archive_file=props.pub_data_dir_wd_coeditors_by_wiki_monthly
    #     + "/"
    #     + props.archive_csv_wd_coeditors_by_wiki_monthly,
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # archive_csv_distinct = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_COEDITORS_DISTINCT_MONTHLY}",
    #     source_directory=props.tmp_dir_wd_coeditors_distinct_monthly,
    #     archive_file=props.pub_data_dir_wd_coeditors_distinct_monthly
    #     + "/"
    #     + props.archive_csv_wd_coeditors_distinct_monthly,
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # MARK: Execute DAG

    sensor >> compute  # >> [[gen_csv_by_wiki >> archive_csv_by_wiki], [gen_csv_distinct >> archive_csv_distinct]]
