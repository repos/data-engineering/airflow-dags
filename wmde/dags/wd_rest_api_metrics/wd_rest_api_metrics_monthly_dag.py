"""
Derives monthly distinct aggregates for user agents and ips that are using the Wikidata REST API.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_rest_api_metrics
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    ARCHIVE_CSV,
    COMPUTE,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    MONTHLY,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

WD_REST_API_METRICS = "wd_rest_api_metrics"  # task id
WD_REST_API_METRICS_MONTHLY = f"{WD_REST_API_METRICS}_{MONTHLY}"  # DAG id
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{WD_REST_API_METRICS}"

# MARK: Properties

props = DagProperties(
    # HDFS source table:
    hive_wmf_webrequest="wmf.webrequest",
    # HDFS destination table:
    hive_wmde_wd_rest_api_metrics_monthly=f"wmde.{WD_REST_API_METRICS_MONTHLY}",
    # Task Hive query:
    hql_wd_rest_api_metrics_monthly=f"{GITLAB_HQL_TASK_DIR}/{WD_REST_API_METRICS_MONTHLY}.hql",
    # TMP export directory:
    tmp_dir_wd_rest_api_metrics_monthly=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_REST_API_METRICS_MONTHLY}",
    # TMP export query:
    hql_gen_csv_wd_rest_api_metrics_monthly=f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV}_{WD_REST_API_METRICS_MONTHLY}.hql",
    # Archive export directory:
    pub_data_dir_wd_rest_api_metrics_monthly=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_REST_API_METRICS_MONTHLY}"
    ),
    # Archive export file:
    archive_csv_wd_rest_api_metrics_monthly=f"{WD_REST_API_METRICS_MONTHLY}.csv",
    # Metadata:
    start_date=datetime(2024, 2, 1),
    sla=timedelta(days=2),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "from_hive",
        "monthly",
        "requires_wmf_webrequest",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=WD_REST_API_METRICS_MONTHLY,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_REST_API_METRICS_MONTHLY}",
        sql=props.hql_wd_rest_api_metrics_monthly,
        query_parameters={
            "source_table": props.hive_wmf_webrequest,
            "destination_table": props.hive_wmde_wd_rest_api_metrics_monthly,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    gen_csv = SparkSqlOperator(
        task_id=f"{GEN_CSV}_{WD_REST_API_METRICS_MONTHLY}",
        sql=props.hql_gen_csv_wd_rest_api_metrics_monthly,
        query_parameters={
            "source_table": props.hive_wmde_wd_rest_api_metrics_monthly,
            "destination_directory": props.tmp_dir_wd_rest_api_metrics_monthly,
        },
    )

    # MARK: Archive Datasets

    archive_csv = HDFSArchiveOperator(
        task_id=f"{ARCHIVE_CSV}_{WD_REST_API_METRICS_MONTHLY}",
        source_directory=props.tmp_dir_wd_rest_api_metrics_monthly,
        archive_file=props.pub_data_dir_wd_rest_api_metrics_monthly
        + "/"
        + props.archive_csv_wd_rest_api_metrics_monthly,
        expected_filename_ending=".csv",
        check_done=True,
    )

    # MARK: Execute DAG

    sensor >> compute >> gen_csv >> archive_csv
