#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Wikidata and lead images for consumption by ALIS & SLIS - weekly."""

from datetime import datetime

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from mergedeep import merge

from platform_eng.config import dag_config
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

props = DagProperties(
    dag_id="wikidata_and_lead_images",
    start_date=datetime(2025, 2, 14),
    schedule=dag_config.schedule,
    timeout=dag_config.timeout,
    catchup=dag_config.catchup,
    sensors_poke_interval=dag_config.sensors_poke_interval,
    alerts_email=dag_config.structured_content_alerts_email,
    tags=dag_config.tags,
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",
    # Spark job
    hive_db="analytics_platform_eng",
    coalesce=4,  # The smaller the less files per partition & the longer execution time
    conda_env=dag_config.artifact("image-suggestions-1.0.0-v1.0.0.conda.tgz"),
    # Spark configuration - applied to the whole DAG.
    # Regular job + 6 GB driver + 12 GB executor overhead.
    # See https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Spark#Regular_jobs
    # and https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Spark#Executors.
    driver_memory="8g",
    executor_cores=4,
    executor_memory="8g",
    spark_conf={
        "spark.dynamicAllocation.maxExecutors": 64,
        "spark.executor.memoryOverhead": "12g",
        "spark.sql.shuffle.partitions": 256,
        "spark.sql.sources.partitionOverwriteMode": "dynamic",
    },
)

args = merge(
    {},
    dag_config.default_args,
    {
        "email": props.alerts_email,
        "driver_memory": props.driver_memory,
        "executor_cores": props.executor_cores,
        "executor_memory": props.executor_memory,
        "conf": props.spark_conf,
    },
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        # The weekly snapshot is on Mondays, i.e., start of scheduled week
        weekly = "{{ data_interval_start | start_of_current_week | to_ds }}"
    else:
        weekly = props.weekly_snapshot
    if not props.monthly_snapshot:
        # Set the most recent monthly snapshot given the weekly one.
        # Here's the template chain with examples:
        # data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month
        # 2023-05-01          | 2023-05-07          | 2023-04-01              | "2023-04"
        # 2023-05-29          | 2023-06-04          | 2023-05-01              | "2023-05"
        monthly = "{{ data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month }}"
    else:
        monthly = props.monthly_snapshot

    # Wait for weekly tables
    weekly_sensor = NamedHivePartitionSensor(
        partition_names=[
            f"wmf.wikidata_entity/snapshot={weekly}",
            f"wmf.wikidata_item_page_link/snapshot={weekly}",
        ],
        task_id="weekly_sensor",
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for monthly tables.
    # Create as many parallel tasks as the monthly tables.
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    monthly_tables = [
        "category",
        "categorylinks",
        "page",
        "page_props",
        "pagelinks",
    ]
    monthly_sensors = []
    for table in monthly_tables:
        url_sensor = URLSensor(
            url=f"{dag_config.hadoop_name_node}/wmf/data/raw/mediawiki/tables/{table}/snapshot={monthly}/_PARTITIONED",
            task_id=f"{table}_sensor",
            poke_interval=props.sensors_poke_interval,
        )
        monthly_sensors.append(url_sensor)
    # `wmf_raw.mediawiki_private_linktarget` is private and has a different HDFS path
    private_table = "linktarget"
    monthly_sensors.append(
        URLSensor(
            url=f"{dag_config.hadoop_name_node}/wmf/data/raw/mediawiki_private/tables/{private_table}/snapshot={monthly}/_PARTITIONED",
            task_id=f"private_{private_table}_sensor",
            poke_interval=props.sensors_poke_interval,
        )
    )

    args = [props.hive_db, weekly, props.coalesce]
    wikidata_and_lead_images = SparkSubmitOperator.for_virtualenv(
        task_id="wikidata_and_lead_images",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/image_suggestions/wikidata_and_lead_images.py",
        application_args=args,
        launcher="skein",
    )

    # Tasks order
    (
        [
            weekly_sensor,
            *monthly_sensors,
        ]
        >> wikidata_and_lead_images
    )
