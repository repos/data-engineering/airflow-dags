#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""SEAL: SEction ALignment machine learning models for Wikipedia articles - weekly.
https://gitlab.wikimedia.org/repos/structured-data/seal
"""

from datetime import datetime

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from mergedeep import merge

from platform_eng.config import dag_config
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters


props = DagProperties(
    dag_id="SEAL",
    start_date=datetime(2023, 9, 18),
    schedule=dag_config.schedule,
    timeout=dag_config.timeout,
    catchup=dag_config.catchup,
    sensors_poke_interval=dag_config.sensors_poke_interval,
    alerts_email=dag_config.structured_content_alerts_email,
    tags=dag_config.tags,
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",  # `gather_sections_content` task
    # Spark jobs
    conda_env=dag_config.artifact("seal-0.4.0-v0.4.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/seal",
    embeddings_model=dag_config.artifact("labse.tgz"),
    embeddings_model_extraction_dir="labse",  # Spark executors will see the embedding model here
    probability_threshold=0.9,  # `train_eval_predict` task
    # Spark configurations
    sections_driver_memory="4g",
    sections_executor_memory="8g",
    sections_executor_cores=4,
    sections_spark_conf={
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.sql.shuffle.partitions": 2048,
        "spark.reducer.maxReqsInFlight": 1,
        "spark.shuffle.io.retryWait": "60s",
    },
    # Enough memory on the driver to cache and serialize the encoder
    embeddings_driver_memory="8g",
    # Enough memory on each executor to be able to retain the broadcasted encoder
    embeddings_executor_memory="12g",
    # Too many cores in the presence of a big broadcast variable
    # would lead to the executor running out of memory
    embeddings_executor_cores=2,
    embeddings_spark_conf={
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.sql.shuffle.partitions": 512,
        "spark.executor.memoryOverhead": "6g",
    },
    # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Extra_large_jobs
    features_driver_memory="32g",
    features_executor_memory="12g",
    features_executor_cores=4,
    features_spark_conf={
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.sql.shuffle.partitions": 4096,
        "spark.executor.memoryOverhead": "6g",
        # Mitigate `FetchFailed` due to `Timeout`
        "spark.shuffle.service.index.cache.size": 2048,
        "spark.stage.maxConsecutiveAttempts": 10,
        "spark.reducer.maxReqsInFlight": 1,
        "spark.shuffle.io.maxRetries": 10,
        "spark.shuffle.io.retryWait": "60s",
    },
    # The model is trained locally, so all training data is pulled back to the driver
    # NOTE These settings might make the driver unstable
    # NOTE The production cluster's maximum allowed driver memory is 48g
    model_driver_memory="46g",
    model_executor_memory="8g",
    model_executor_cores=4,
    model_spark_conf={
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.sql.shuffle.partitions": 512,
        "spark.executor.memoryOverhead": "4g",
        "spark.reducer.maxReqsInFlight": 1,
        "spark.shuffle.io.retryWait": "300s",
        "spark.driver.maxResultSize": 0,
        # Mitigate `SparkException: Could not execute broadcast`
        "spark.sql.broadcastTimeout": "600s",
    },
)
args = merge(
    {},
    dag_config.default_args,
    {
        "email": props.alerts_email,
    },
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        # The weekly snapshot is on Mondays, i.e., start of scheduled week
        weekly = "{{ data_interval_start | start_of_current_week | to_ds }}"
    else:
        weekly = props.weekly_snapshot
    if not props.monthly_snapshot:
        # Set the most recent monthly snapshot given the weekly one.
        # Here's the template chain with examples:
        # data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month
        # 2023-05-01          | 2023-05-07          | 2023-04-01              | "2023-04"
        # 2023-05-29          | 2023-06-04          | 2023-05-01              | "2023-05"
        monthly = "{{ data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month }}"
    else:
        monthly = props.monthly_snapshot

    # Wait for Wikidata - weekly
    wait_for_wikidata = NamedHivePartitionSensor(
        task_id="wait_for_wikidata",
        partition_names=[f"wmf.wikidata_item_page_link/snapshot={weekly}"],
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for wikitext - monthly
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    wait_for_wikitext = URLSensor(
        task_id="wait_for_wikitext",
        # URL for `wmf.mediawiki_wikitext_current`
        url=f"{dag_config.hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={monthly}/_PARTITIONED",
        poke_interval=props.sensors_poke_interval,
    )

    # Gather relevant content from Wikipedia sections
    # TODO wait for the updated version of section topics instead
    sections_content_args = [
        weekly,
        monthly,
        "--work-dir",
        props.work_dir,
    ]
    gather_sections_content = SparkSubmitOperator.for_virtualenv(
        task_id="gather_sections_content",
        driver_memory=props.sections_driver_memory,
        executor_memory=props.sections_executor_memory,
        executor_cores=props.sections_executor_cores,
        conf=props.sections_spark_conf,
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/seal/sections.py",
        application_args=sections_content_args,
        launcher="skein",
    )

    # Encode sections into embeddings
    embeddings_args = [
        weekly,
        "--work-dir",
        props.work_dir,
    ]
    encode_embeddings = SparkSubmitOperator.for_virtualenv(
        task_id="encode_embeddings",
        driver_memory=props.embeddings_driver_memory,
        executor_memory=props.embeddings_executor_memory,
        executor_cores=props.embeddings_executor_cores,
        conf=props.embeddings_spark_conf,
        archives=f"{props.embeddings_model}#{props.embeddings_model_extraction_dir}",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/seal/embeddings.py",
        application_args=embeddings_args,
        launcher="skein",
    )

    # Extract features
    features_args = [
        weekly,
        "--work-dir",
        props.work_dir,
    ]
    extract_features = SparkSubmitOperator.for_virtualenv(
        task_id="extract_features",
        driver_memory=props.features_driver_memory,
        executor_memory=props.features_executor_memory,
        executor_cores=props.features_executor_cores,
        conf=props.features_spark_conf,
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/seal/features.py",
        application_args=features_args,
        launcher="skein",
    )

    # Train & evaluate the SEAL model, then batch-predict
    train_eval_predict_args = [
        weekly,
        "--work-dir",
        props.work_dir,
        "--threshold",
        props.probability_threshold,
    ]
    train_eval_predict = SparkSubmitOperator.for_virtualenv(
        task_id="train_eval_predict",
        driver_memory=props.model_driver_memory,
        executor_memory=props.model_executor_memory,
        executor_cores=props.model_executor_cores,
        conf=props.model_spark_conf,
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/seal/model.py",
        application_args=train_eval_predict_args,
        launcher="skein",
    )

    # Tasks order
    (
        [wait_for_wikidata, wait_for_wikitext]
        >> gather_sections_content
        >> encode_embeddings
        >> extract_features
        >> train_eval_predict
    )
