#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""SLIS: section-level image suggestions - weekly.
https://gitlab.wikimedia.org/repos/structured-data/image-suggestions
"""

from datetime import datetime
from os import path

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from mergedeep import merge

from platform_eng.config import dag_config
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

props = DagProperties(
    dag_id="SLIS",
    start_date=datetime(2024, 10, 24),
    schedule=dag_config.schedule,
    timeout=dag_config.timeout,
    catchup=dag_config.catchup,
    sensors_poke_interval=dag_config.sensors_poke_interval,
    alerts_email=dag_config.structured_content_alerts_email,
    tags=dag_config.tags,
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",
    cirrus_snapshot="",
    # Spark jobs
    hive_db="analytics_platform_eng",
    coalesce=4,  # The smaller the less files per partition & the longer execution time
    conda_env=dag_config.artifact("image-suggestions-1.0.0-v1.0.0.conda.tgz"),
    section_topics_base_dir="/user/analytics-platform-eng/structured-data/section_topics",
    section_topics_dir="topics",
    article_images_dir="images",
    denylist_dir="section_titles_denylist",
    section_alignment_suggestions_dir="section_alignment_image_suggestions",
    # Spark configuration - applied to the whole DAG
    driver_memory="8g",
    executor_cores=4,
    executor_memory="8g",
    spark_conf={
        "spark.dynamicAllocation.maxExecutors": 64,
        "spark.sql.shuffle.partitions": 256,
        "spark.sql.sources.partitionOverwriteMode": "dynamic",
    },
)
args = merge(
    {},
    dag_config.default_args,
    {
        "email": props.alerts_email,
        "driver_memory": props.driver_memory,
        "executor_cores": props.executor_cores,
        "executor_memory": props.executor_memory,
        "conf": props.spark_conf,
    },
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        # The weekly snapshot is on Mondays, i.e., start of scheduled week
        weekly = "{{ data_interval_start | start_of_current_week | to_ds }}"
    else:
        weekly = props.weekly_snapshot
    if not props.monthly_snapshot:
        # Set the most recent monthly snapshot given the weekly one.
        # Here's the template chain with examples:
        # data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month
        # 2023-05-01          | 2023-05-07          | 2023-04-01              | "2023-04"
        # 2023-05-29          | 2023-06-04          | 2023-05-01              | "2023-05"
        monthly = "{{ data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month }}"
    else:
        monthly = props.monthly_snapshot
    if not props.cirrus_snapshot:
        # The Cirrus search index snapshot is 1 day before, `YYYYMMDD` format
        cirrus_weekly = "{{ data_interval_start | start_of_current_week | start_of_previous_day | to_ds_nodash }}"
    else:
        cirrus_weekly = props.cirrus_snapshot

    # Wait for weekly tables
    weekly_sensor = NamedHivePartitionSensor(
        partition_names=[
            f"wmf.wikidata_entity/snapshot={weekly}",
            f"structured_data.commons_entity/snapshot={weekly}",
        ],
        task_id="weekly_sensor",
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for monthly tables.
    # Create as many parallel tasks as the monthly tables.
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    monthly_mediawiki_tables = [
        "imagelinks",
        "page",
        "revision",
    ]
    monthly_sensors = []
    for table in monthly_mediawiki_tables:
        url_sensor = URLSensor(
            url=f"{dag_config.hadoop_name_node}/wmf/data/raw/mediawiki/tables/{table}/snapshot={monthly}/_PARTITIONED",
            task_id=f"{table}_sensor",
            poke_interval=props.sensors_poke_interval,
        )
        monthly_sensors.append(url_sensor)

    # Wait for Wikidata & lead images DAG - weekly
    wikidata_and_lead_images_sensor = ExternalTaskSensor(
        task_id="wikidata_and_lead_images_sensor",
        external_dag_id="wikidata_and_lead_images",
    )

    # Wait for Cirrus search index weighted tags - weekly
    cirrus_index_sensor = NamedHivePartitionSensor(
        partition_names=[
            f"discovery.cirrus_index_without_content/cirrus_replica=codfw/snapshot={cirrus_weekly}",
        ],
        task_id="cirrus_index_sensor",
        poke_interval=props.sensors_poke_interval,
    )

    section_topics_base_dir = props.section_topics_base_dir

    # Wait for section topics - weekly
    # NOTE The _SUCCESS file tells us that the data has finished being written
    section_topics_path = path.join(section_topics_base_dir, props.section_topics_dir, weekly)
    section_topics_sensor = URLSensor(
        url=f"{dag_config.hadoop_name_node}{section_topics_path}/_SUCCESS",
        task_id="section_topics_sensor",
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for section alignment suggestions - weekly
    section_alignment_suggestions_path = path.join(
        section_topics_base_dir, props.section_alignment_suggestions_dir, weekly
    )
    section_alignment_suggestions_sensor = URLSensor(
        url=f"{dag_config.hadoop_name_node}{section_alignment_suggestions_path}/_SUCCESS",
        task_id="section_alignment_suggestions_sensor",
        poke_interval=props.sensors_poke_interval,
    )

    # Generate SLIS
    article_images_path = path.join(section_topics_base_dir, props.article_images_dir, weekly)
    denylist_path = path.join(section_topics_base_dir, props.denylist_dir, weekly)
    args = [
        props.hive_db,
        weekly,
        props.coalesce,
        section_topics_path,
        section_alignment_suggestions_path,
        article_images_path,
        "--denylist",
        denylist_path,
    ]
    slis = SparkSubmitOperator.for_virtualenv(
        task_id="slis",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/image_suggestions/slis.py",
        launcher="skein",
        application_args=args,
    )

    # Check SLIS count
    slis_count = BashOperator(
        task_id="slis_count",
        retries=0,
        email_on_failure=True,
        bash_command=(
            f"raw=$(hive -e \"SELECT COUNT(*) FROM {props.hive_db}.image_suggestions_suggestions WHERE snapshot='{weekly}' AND section_index IS NOT NULL\"); "
            "slis=$(echo $raw | cut -d ' ' -f2); "
            'echo "=====> There are $slis SLIS <====="; '
            "if [[ $slis -eq 0 ]]; then exit 42; fi"
        ),
    )

    # Generate boolean tags for wiki search indices.
    # https://phabricator.wikimedia.org/T299884
    args = [props.hive_db, weekly, props.coalesce, "slis"]
    wiki_indices = SparkSubmitOperator.for_virtualenv(
        task_id="wiki_indices",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/image_suggestions/wiki_indices.py",
        application_args=args,
        launcher="skein",
    )

    # Tasks order
    (
        [
            weekly_sensor,
            *monthly_sensors,
            wikidata_and_lead_images_sensor,
            cirrus_index_sensor,
            section_topics_sensor,
            section_alignment_suggestions_sensor,
        ]
        >> slis
        >> slis_count
        >> wiki_indices
    )
