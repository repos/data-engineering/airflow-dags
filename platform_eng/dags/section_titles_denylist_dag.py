#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Section titles denylist - weekly."""

from datetime import datetime
from os import path

from airflow import DAG
from mergedeep import merge

from platform_eng.config import dag_config
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters


props = DagProperties(
    dag_id="section_titles_denylist",
    start_date=datetime(2024, 9, 18),
    schedule=dag_config.schedule,
    timeout=dag_config.timeout,
    catchup=dag_config.catchup,
    sensors_poke_interval=dag_config.sensors_poke_interval,
    alerts_email=dag_config.structured_content_alerts_email,
    tags=dag_config.tags,
    # The default snapshot is set within the DAG via templates.
    # Use this property to override them.
    weekly_snapshot="",
    conda_env=dag_config.artifact("section-topics-1.1.0-v1.1.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section_topics",
    seal_path="/user/analytics-platform-eng/structured-data/seal/alignments/{weekly}",
)
args = merge(
    {},
    dag_config.default_args,
    {
        "email": props.alerts_email,
    },
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        # The weekly snapshot is on Mondays, i.e., start of scheduled week
        weekly = "{{ data_interval_start | start_of_current_week | to_ds }}"
    else:
        weekly = props.weekly_snapshot

    seal_path = props.seal_path.format(weekly=weekly)
    wait_for_seal = URLSensor(
        task_id="wait_for_seal",
        url=f"{dag_config.hadoop_name_node}/{seal_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    denylist_dir = path.join(props.work_dir, "section_titles_denylist")
    gather_section_titles_denylist = SparkSubmitOperator.for_virtualenv(
        task_id="gather_section_titles_denylist",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/scripts/gather_section_titles_denylist.py",
        application_args=[
            "--aligned-sections",
            seal_path,
            "--output",
            path.join(denylist_dir, weekly),
        ],
        launcher="skein",
    )

    # remove all but the last 4 outputs
    cleanup_gather_section_titles_denylist = SimpleSkeinOperator(
        task_id="cleanup_gather_section_titles_denylist",
        script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $(hdfs dfs -ls {denylist_dir} | head -n -4 | tr -s ' ' | cut -d' ' -f8)",
    )

    wait_for_seal >> gather_section_titles_denylist >> cleanup_gather_section_titles_denylist
