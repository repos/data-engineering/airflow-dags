#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Feed Cassandra with image suggestions - weekly."""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from mergedeep import merge

from platform_eng.config import dag_config
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

props = DagProperties(
    dag_id="cassandra",
    start_date=datetime(2025, 1, 16),
    schedule=dag_config.schedule,
    timeout=dag_config.timeout,
    slis_timeout=timedelta(days=3),  # Wait 3 days for SLIS, then skip
    catchup=dag_config.catchup,
    alerts_email=dag_config.structured_content_alerts_email,
    tags=dag_config.tags,
    # The default snapshot is set within the DAG via templates.
    # Use this property to override it.
    weekly_snapshot="",
    # Spark configuration - applied to the whole DAG.
    # Regular jobs, see https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Spark#Regular_jobs
    driver_memory="2g",
    executor_cores=4,
    executor_memory="8g",
    spark_conf={
        "spark.dynamicAllocation.maxExecutors": 64,
        "spark.sql.shuffle.partitions": 256,
    },
    # Cassandra
    hive_db="analytics_platform_eng",
    spark_cassandra_connector_uri=dag_config.artifact("spark-cassandra-connector-assembly-3.2.0-WMF-1.jar"),
    hive_to_cassandra_jar_uri=dag_config.artifact("refinery-job-0.2.8-shaded.jar"),
    # NOTE Set as many parallel loaders as the amount of Cassandra hosts
    coalesce=6,
    # Data check
    conda_env=dag_config.artifact("image-suggestions-1.0.0-v1.0.0.conda.tgz"),
)

args = merge(
    {},
    dag_config.default_args,
    {
        "email": props.alerts_email,
        "driver_memory": props.driver_memory,
        "executor_cores": props.executor_cores,
        "executor_memory": props.executor_memory,
        "conf": props.spark_conf,
    },
)
cassandra_conf = merge(
    {},
    {
        **dag_config.cassandra_default_conf,
        "spark.yarn.maxAppAttempts": 1,
    },
    args["conf"],
)


def build_cassandra_insert_query(cassandra_table, coalesce, hive_columns, hive_db, hive_table, snapshot):
    # Build a quoted `INSERT` query to feed Cassandra with image suggestions from Hive

    return (
        f"INSERT INTO aqs.image_suggestions.{cassandra_table} "
        f"SELECT /* + COALESCE({coalesce}) */ {hive_columns} "
        f"FROM {hive_db}.{hive_table} "
        f"WHERE snapshot='{snapshot}'"
    )


with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        # The weekly snapshot is on Mondays, i.e., start of scheduled week
        weekly = "{{ data_interval_start | start_of_current_week | to_ds }}"
    else:
        weekly = props.weekly_snapshot

    # Wait for ALIS
    alis_dag_sensor = ExternalTaskSensor(
        task_id="wait_for_ALIS",
        external_dag_id="ALIS",
    )

    # Wait for SLIS, with specific timeout
    slis_dag_sensor = ExternalTaskSensor(
        task_id="wait_for_SLIS",
        external_dag_id="SLIS",
        timeout=props.slis_timeout,
    )

    # Feed Cassandra with image suggestions.
    # https://phabricator.wikimedia.org/T299885
    # NOTE We create an Airflow task for every target Cassandra table
    #      See https://phabricator.wikimedia.org/T293808
    spark_cassandra_connector_uri = props.spark_cassandra_connector_uri
    hive_to_cassandra_jar_uri = props.hive_to_cassandra_jar_uri
    coalesce = props.coalesce

    # `suggestions` table:
    # - initial schema at https://phabricator.wikimedia.org/T293808
    # - section heading addition at https://phabricator.wikimedia.org/T328670
    # - section index and page QID addition at https://phabricator.wikimedia.org/T336424
    query = build_cassandra_insert_query(
        cassandra_table="suggestions",
        coalesce=coalesce,
        hive_columns=(
            "wiki,page_id,id,image,origin_wiki,confidence,"
            "found_on,kind,page_rev,section_heading,section_index,page_qid"
        ),
        hive_db=props.hive_db,
        hive_table="image_suggestions_suggestions",
        snapshot=weekly,
    )
    feed_suggestions = SparkSqlOperator(
        task_id="feed_suggestions",
        trigger_rule="all_done",
        sql=query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein",
    )

    # `title_cache` table
    query = build_cassandra_insert_query(
        cassandra_table="title_cache",
        coalesce=coalesce,
        hive_columns="wiki,page_id,page_rev,title",
        hive_db=props.hive_db,
        hive_table="image_suggestions_title_cache",
        snapshot=weekly,
    )
    feed_title_cache = SparkSqlOperator(
        task_id="feed_title_cache",
        trigger_rule="all_done",
        sql=query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein",
    )

    # `instanceof_cache` table
    query = build_cassandra_insert_query(
        cassandra_table="instanceof_cache",
        coalesce=coalesce,
        hive_columns="wiki,page_id,page_rev,instance_of",
        hive_db=props.hive_db,
        hive_table="image_suggestions_instanceof_cache",
        snapshot=weekly,
    )
    feed_instanceof_cache = SparkSqlOperator(
        task_id="feed_instanceof_cache",
        trigger_rule="all_done",
        sql=query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein",
    )

    # Push metrics to the Prometheus gateway:
    # - explicitly pass `conda_env`
    # - invoke Python & script from the `venv` relative path
    check_data_and_push_metrics = SimpleSkeinOperator(
        task_id="check_data_and_push_metrics",
        trigger_rule="all_done",
        files={"venv": props.conda_env},
        script="venv/bin/python venv/lib/python3.10/site-packages/image_suggestions/data_check.py",
    )

    # Tasks order
    (
        [
            alis_dag_sensor,
            slis_dag_sensor,
        ]
        >> feed_suggestions
        >> feed_title_cache
        >> feed_instanceof_cache
        >> check_data_and_push_metrics
    )
