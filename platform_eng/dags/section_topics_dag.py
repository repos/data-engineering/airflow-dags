#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Section topics & section alignment image suggestions - weekly.
https://gitlab.wikimedia.org/repos/structured-data/section-topics
"""

from datetime import datetime
from os import path

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from mergedeep import merge

from platform_eng.config import dag_config
from platform_eng.config.utils import latest_html_dumps_snapshot_before
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters


props = DagProperties(
    dag_id="section_topics",
    start_date=datetime(2022, 9, 7),
    schedule=dag_config.schedule,
    timeout=dag_config.timeout,
    catchup=dag_config.catchup,
    sensors_poke_interval=dag_config.sensors_poke_interval,
    alerts_email=dag_config.structured_content_alerts_email,
    tags=dag_config.tags,
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    monthly_snapshot="",
    html_tables_snapshot="",
    # Spark jobs
    conda_env=dag_config.artifact("section-topics-1.1.0-v1.1.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section_topics",
    seal_path="/user/analytics-platform-eng/structured-data/seal/alignments/{weekly}",  # Must be absolute
    max_target_images=0,  # Section alignment image suggestions for sections with no images
    # Spark configurations
    topics_driver_memory="4g",
    topics_executor_cores=4,
    topics_executor_memory="8g",
    topics_spark_conf={
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.sql.shuffle.partitions": 2048,
        "spark.executor.memoryOverhead": "3g",
        "spark.reducer.maxReqsInFlight": 1,
        "spark.shuffle.io.retryWait": "60s",
        # Uncomment in case of `FetchFailed` due to `NetworkTimeout`, see
        # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Scaling_the_driver,
        # https://towardsdatascience.com/fetch-failed-exception-in-apache-spark-decrypting-the-most-common-causes-b8dff21075c
        # "spark.stage.maxConsecutiveAttempts": 10,
        # "spark.shuffle.io.maxRetries": 10,
    },
    images_driver_memory="16g",
    images_executor_cores=4,
    images_executor_memory="8g",
    images_spark_conf={
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.sql.shuffle.partitions": 1024,
    },
)
args = merge(
    {},
    dag_config.default_args,
    {
        "email": props.alerts_email,
    },
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
    user_defined_filters=filters,
    user_defined_macros={
        "latest_html_dumps_snapshot_before": latest_html_dumps_snapshot_before,
    },
) as dag:
    # NOTE We pass input date snapshots via a mix of Airflow's built-in templates and
    #      `wmf_airflow_common` custom ones.
    #      See https://airflow.apache.org/docs/apache-airflow/stable/templates-ref.html
    #      and https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/templates/time_filters.py
    if not props.weekly_snapshot:
        # The weekly snapshot is on Mondays, i.e., start of scheduled week
        weekly = "{{ data_interval_start | start_of_current_week | to_ds }}"
    else:
        weekly = props.weekly_snapshot
    if not props.monthly_snapshot:
        # Set the most recent monthly snapshot given the weekly one.
        # Here's the template chain with examples:
        # data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month
        # 2023-05-01          | 2023-05-07          | 2023-04-01              | "2023-04"
        # 2023-05-29          | 2023-06-04          | 2023-05-01              | "2023-05"
        monthly = "{{ data_interval_start | end_of_current_week | start_of_previous_month | to_ds_month }}"
    else:
        monthly = props.monthly_snapshot
    if not props.html_tables_snapshot:
        html_tables_snapshot = "{{ latest_html_dumps_snapshot_before(data_interval_end) }}"
    else:
        html_tables_snapshot = props.html_tables_snapshot

    work_dir = props.work_dir

    # Wait for Wikidata page links - weekly
    wait_for_wikidata = NamedHivePartitionSensor(
        task_id="wait_for_wikidata",
        partition_names=[f"wmf.wikidata_item_page_link/snapshot={weekly}"],
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for wikitext - monthly
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    wait_for_wikitext = URLSensor(
        task_id="wait_for_wikitext",
        # URL for `wmf.mediawiki_wikitext_current`
        url=f"{dag_config.hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={monthly}/_PARTITIONED",
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for section titles denylist - weekly
    denylist_path = path.join(work_dir, "section_titles_denylist", weekly)
    wait_for_section_titles_denylist = URLSensor(
        task_id="wait_for_section_titles_denylist",
        url=f"{dag_config.hadoop_name_node}{denylist_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for badly parsed pages - monthly
    check_bad_parsing_path = path.join(work_dir, "bad_parsing", monthly)
    wait_for_check_bad_parsing = URLSensor(
        task_id="wait_for_check_bad_parsing",
        url=f"{dag_config.hadoop_name_node}{check_bad_parsing_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # Wait for sections with HTML tables - bi-weekly
    detect_html_tables_path = path.join(work_dir, "target_wikis_tables", html_tables_snapshot)
    wait_for_detect_html_tables = URLSensor(
        task_id="wait_for_detect_html_tables",
        url=f"{dag_config.hadoop_name_node}{detect_html_tables_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # Generate Wikidata QIDs filters
    qids_filters_sparql_dir = "venv/lib/python3.10/site-packages/section_topics/scripts/sparql"
    qids_filters_output_dir = path.join(work_dir, "qids_filters")
    qids_filters = {
        f"{qids_filters_sparql_dir}/qids_for_all_points_in_time.sparql": path.join(
            qids_filters_output_dir, "qids_for_all_points_in_time", weekly
        ),
        f"{qids_filters_sparql_dir}/qids_for_media_outlets.sparql": path.join(
            qids_filters_output_dir, "qids_for_media_outlets", weekly
        ),
    }
    fetch_qids_filters = [
        SparkSubmitOperator.for_virtualenv(
            task_id=f"fetch_{path.basename(sparql).split('.')[0]}",
            virtualenv_archive=props.conda_env,
            entry_point="lib/python3.10/site-packages/section_topics/scripts/fetch_qids_from_wikidata.py",
            application_args=[
                sparql,
                "--output",
                output,
            ],
            launcher="skein",
        )
        for sparql, output in qids_filters.items()
    ]

    # Remove all but the last 4 outputs for each Wikidata QID filter
    old_files_cmd = " && ".join(
        [
            f"hdfs dfs -ls {path.dirname(output)} | head -n -4 | tr -s ' ' | cut -d' ' -f8"
            for sparql, output in qids_filters.items()
        ]
    )
    cleanup_fetch_qids_filters = SimpleSkeinOperator(
        task_id="cleanup_fetch_qids",
        script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $({old_files_cmd})",
    )

    # # Generate media prefixes
    # NOTE Not currently used - uncomment to enable
    # media_prefixes_dir = path.join(work_dir, 'media_prefixes')
    # media_prefixes_path = path.join(media_prefixes_dir, weekly)
    # collect_media_prefixes = SparkSubmitOperator.for_virtualenv(
    #     task_id="collect_media_prefixes",
    #     virtualenv_archive=props.conda_env,
    #     entry_point="lib/python3.10/site-packages/section_topics/scripts/collect_media_prefixes.py",
    #     application_args=[
    #         "--output", media_prefixes_path,
    #     ],
    #     launcher="skein",
    # )
    # # Remove all but the last 4 outputs
    # cleanup_collect_media_prefixes = SimpleSkeinOperator(
    #     task_id="cleanup_collect_media_prefixes",
    #     script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $(hdfs dfs -ls {media_prefixes_dir} | head -n -4 | tr -s ' ' | cut -d' ' -f8)",
    # )

    # Generate section topics and article images with default optional arguments
    topics_path = path.join(work_dir, "topics", weekly)
    images_path = path.join(work_dir, "images", weekly)
    pipeline = SparkSubmitOperator.for_virtualenv(
        task_id="pipeline",
        driver_memory=props.topics_driver_memory,
        executor_memory=props.topics_executor_memory,
        executor_cores=props.topics_executor_cores,
        conf=props.topics_spark_conf,
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/pipeline.py",
        application_args=[
            weekly,
            "--output",
            topics_path,
            "--images-output",
            images_path,
            "--page-filter",
            check_bad_parsing_path,
            "--section-title-filter",
            denylist_path,
            "--table-filter",
            detect_html_tables_path,
            *[arg for sparql, output in qids_filters.items() for arg in ["--qid-filter", output]],
            # NOTE Media prefixes not currently used - uncomment to enable
            # "--handle-media", path.join(work_dir, 'media_links', weekly),
            # "--media-prefixes", media_prefixes_path,
        ],
        launcher="skein",
    )

    # `format` won't affect any value overridden in DAG properties
    seal_path = props.seal_path.format(weekly=weekly)
    wait_for_seal = URLSensor(
        task_id="wait_for_seal",
        url=f"{dag_config.hadoop_name_node}/{seal_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    # Generate section alignment image suggestions
    suggestions_path = path.join(work_dir, "section_alignment_image_suggestions", weekly)
    section_alignment_image_suggestions = SparkSubmitOperator.for_virtualenv(
        task_id="section_alignment_image_suggestions",
        driver_memory=props.images_driver_memory,
        executor_memory=props.images_executor_memory,
        executor_cores=props.images_executor_cores,
        conf=props.images_spark_conf,
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/section_alignment_image_suggestions.py",
        application_args=[
            "--output",
            suggestions_path,
            "--section-images",
            images_path,
            "--section-alignments",
            seal_path,
            "--max-target-images",
            props.max_target_images,
        ],
        launcher="skein",
    )

    # Tasks order
    (
        [
            wait_for_wikidata,
            wait_for_wikitext,
            wait_for_section_titles_denylist,
            wait_for_check_bad_parsing,
            wait_for_detect_html_tables,
            fetch_qids_filters >> cleanup_fetch_qids_filters,
            # NOTE Media prefixes not currently used - uncomment to enable
            # collect_media_prefixes >> cleanup_collect_media_prefixes,
        ]
        >> pipeline
        >> wait_for_seal
        >> section_alignment_image_suggestions
    )
