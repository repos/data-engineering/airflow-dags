from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy import DummyOperator

from platform_eng.config.dag_config import (
    dataset,
    default_args,
)

from platform_eng.config.dag_config import create_easy_dag
# import the analytics instance alerts_email since the analytics team
# (Data Engineering) is the owner of this DAG
from analytics.config.dag_config import alerts_email

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator


props = DagProperties(
    dag_start_date=datetime(2025, 1, 18),
    target_dataset_id="iceberg_wmf_content_mediawiki_content_history_v1",
    # SLA and alert email
    dag_sla=timedelta(hours=2),
    alerts_email=alerts_email,
)

with create_easy_dag(
    dag_id="test_produced_by_config",
    description="Canary DAG to keep testing produced_by config",
    doc_md=__doc__,
    start_date=props.dag_start_date,
    schedule="@daily",
    tags=["daily", "from_dag", "requires_iceberg_wmf_content"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:

    wait_for = dataset(props.target_dataset_id) \
        .get_sensor_for(dag, airflow_api_kerberos_enabled=False)

    complete = DummyOperator(task_id="complete")

    wait_for >> complete
