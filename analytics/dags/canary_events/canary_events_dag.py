"""
### Generates canary events for testing the event pipeline

Note: This DAG executes twice an hour at HH:02 and HH:35. The goal is to generate
canary events with timestamps corresponding to the task execution time. To achieve
this, we use the data_interval_end timestamp in the prepare_scripts_commands task.
While it's possible to schedule the task at the data_interval_start using a
custom Timetable, this straightforward approach is much simpler.
"""

import json
import logging
import os
import re
import subprocess
from datetime import datetime, timedelta
from pprint import pformat
from typing import List

import fsspec
from airflow.decorators import task
from airflow.operators.python import get_current_context

from analytics.config.dag_config import alerts_email, artifact, create_easy_dag
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.util import is_running_in_kubernetes

dag_id = "canary_events"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2024, 5, 21, 12, 2),
    sla=timedelta(hours=6),
    email=alerts_email,
    stream_config_url="https://meta.wikimedia.org/w/api.php?action=streamconfigs&constraints=canary_events_enabled=1",
    refinery_job_jar=artifact("refinery-job-0.2.44-shaded.jar"),
    catchup=False,
    schema_base_uris=[
        "https://schema.discovery.wmnet/repositories/primary/jsonschema",
        "https://schema.discovery.wmnet/repositories/secondary/jsonschema",
    ],
    event_stream_config_uri="https://meta.wikimedia.org/w/api.php",
    event_intake_service_urls_file="file:///etc/refinery/event_intake_service_urls.yaml",
    # This parameter points to a config file imported in the Yarn container.
    # As the path always changes, but in the same time needs to be absolute (in ProduceCanaryEvents),
    # we are using the subcall `pwd`.
    event_intake_service_urls_file_uri="file://$(pwd)/event_intake_service_urls.yaml",
    # Parallelization params. We would like canary events to be produced before Gobblin jobs start at HH:15.
    max_active_tasks=20,  # Bump max_active_tasks to schedule more small jobs see comment in `produce_canary_event`
    max_active_runs=2,
    max_active_tis_per_dag=40,  # max_active_tasks * max_active_runs
    # Canary events should be generated as fast as possible. Gobblin relies on those to be produced at a certain time.
    # It allows Gobblin to generate _IMPORTED flags.
    airflow_pool="canary_events",
    driver_memory="2G",
    vcores=1,
)

logger = logging.getLogger(__name__)

STREAM_NAMES_XCOM_KEY = "stream_names"

# Jinja template to retrieve the stream name from the XCom providing a ti is in the context
STREAM_NAME_RETRIEVER_TEMPLATE = f"ti.xcom_pull(key='{STREAM_NAMES_XCOM_KEY}')[ti.map_index]"


def _push_streams_names_to_xcom(stream_names: List[str]) -> None:
    """Push the stream names to the XCom to conveniently set it only once and retrieve them by index later."""
    get_current_context()["task_instance"].xcom_push(key=STREAM_NAMES_XCOM_KEY, value=stream_names)


# Customize app names on Yarn by including the hour in the name
# e.g. produce_canary_event__2024080817__2_event_eventgate_analytics_test_event
CUSTOM_YARN_TASK_NAME = (
    f"{{{{ task_instance_key_str }}}}{{{{ data_interval_start | to_hour }}}}__"
    f"{{{{ ti.map_index }}}}_"
    f"{{{{ {STREAM_NAME_RETRIEVER_TEMPLATE} | remove_non_alphanumeric_characters }}}}"
)


def remove_non_alphanumeric_characters(text: str) -> str:
    """Remove non-alphanumeric characters from a string."""
    print(text)
    return re.sub(r"\W+", "_", text)


def resolve_classpath():
    cmd = subprocess.run(
        [f'{os.environ["HADOOP_HOME"]}/bin/hadoop', "classpath", "--glob"], capture_output=True, encoding="utf-8"
    )
    return cmd.stdout.strip()


with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="2,35 * * * *",  # 2 times per hour: HH:02 and HH:35
    tags=["hourly", "from_hive", "to_iceberg"],
    default_args={
        "sla": props.sla,
        "map_index_template": f"{{{{ {STREAM_NAME_RETRIEVER_TEMPLATE} }}}}",
        "max_active_tis_per_dag": props.max_active_tis_per_dag,
        "pool": props.airflow_pool or "default_pool",
    },
    catchup=props.catchup,
    max_active_tasks=props.max_active_tasks,
    max_active_runs=props.max_active_runs,
    user_defined_filters={
        "remove_non_alphanumeric_characters": remove_non_alphanumeric_characters,
    },
) as dag:

    @task(
        do_xcom_push=True,
        templates_dict={"dt": "{{ data_interval_end | to_iso8601 }}"},
    )
    def fetch_streams_configurations(**kwargs) -> List[dict]:
        url = props.stream_config_url
        logger.info(f"Fetching streams from {url}")
        if is_running_in_kubernetes():
            os.environ["CLASSPATH"] = resolve_classpath()
        with fsspec.open(url) as f:
            stream_names: List[str] = [
                conf["stream"] for conf in json.load(f)["streams"].values() if "destination_event_service" in conf
            ]
        logger.info(f"{len(stream_names)} stream(s) to produce canary events:\n{pformat(stream_names)}")
        _push_streams_names_to_xcom(stream_names)
        return [
            {
                "script": (
                    "spark3-submit --master local[1] "
                    "--class org.wikimedia.analytics.refinery.job.ProduceCanaryEvents "
                    f"{props.refinery_job_jar} "
                    f"--schema_base_uris={','.join(props.schema_base_uris)} "
                    f"--event_stream_config_uri={props.event_stream_config_uri} "
                    f"--event_service_config_uri={props.event_intake_service_urls_file_uri} "
                    f"--stream_name {stream_name} "
                    f"--timestamp {kwargs['templates_dict']['dt']} "
                    "--dry_run false"
                )
            }
            for stream_name in stream_names
        ]

    # We are using SimpleSkeinOperator instead of SparkSubmitOperator. Why:
    # * ProduceCanaryEvents expects event_service_config_uri to be an absolute path uri with protocol
    # * In the context of a Skein container we can rebuild  the absolute path with `$(pwd)`
    # * SparkSubmitOperator puts the spark arguments into simple quotes
    # * Simple quotes does not allow the substitution of the $(pwd) variable
    # * Whereas SimpleSkewinOperator allows the substitution of the $(pwd) variable
    produce_canary_event = SimpleSkeinOperator.partial(
        task_id="produce_canary_event",
        files={os.path.basename(props.event_intake_service_urls_file_uri): props.event_intake_service_urls_file},
        sla=None,  # SLA are not supported for mapped tasks
        queue="launchers",  # Note: the jobs use the `launchers` queue to not overload the `production`one
        name=CUSTOM_YARN_TASK_NAME,
    )

    produce_canary_event.expand_kwargs(fetch_streams_configurations())
