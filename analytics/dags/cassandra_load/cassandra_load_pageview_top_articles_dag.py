"""
Loads Hive pageview top articles into Cassandra

The loading is done with 2 dags: daily and monthly.

"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Data source and destination.
    hive_pageview_table="wmf.pageview_hourly",
    hive_disallowed_cassandra_articles_table="wmf.disallowed_cassandra_articles",
    cassandra_pageview_top_articles_table="aqs.local_group_default_T_top_pageviews.data",
    # DAG start dates.
    daily_dag_start_date=datetime(2023, 7, 18),
    monthly_dag_start_date=datetime(2023, 7, 1),
    # HQL query paths
    daily_dag_hql=f"{hql_directory}/cassandra/daily/load_cassandra_pageview_top_articles_daily.hql",
    monthly_dag_hql=f"{hql_directory}/cassandra/monthly/load_cassandra_pageview_top_articles_monthly.hql",
    # SLAs and alerts email.
    daily_dag_sla=timedelta(hours=10),
    # Spark settings for daily run
    daily_executor_memory="8G",
    daily_executor_cores=2,
    daily_driver_memory="8G",
    daily_driver_cores=2,
    # Spark settings for monthly run
    monthly_dag_sla=timedelta(days=5),
    monthly_executor_memory="24G",
    monthly_executor_cores=3,
    monthly_driver_memory="16G",
    monthly_driver_cores=2,
    alerts_email=alerts_email,
)


default_tags = [
    "from_hive",
    "to_cassandra",
    "uses_hql",
    "requires_wmf_disallowed_cassandra_articles",
    "requires_wmf_pageview_hourly",
]


default_query_parameters = {
    "source_table": props.hive_pageview_table,
    "disallowed_cassandra_articles_table": props.hive_disallowed_cassandra_articles_table,
    "destination_table": props.cassandra_pageview_top_articles_table,
    # Setting coalesce_partitions to 6 is important
    # as it defines how many parallel loaders we use for cassandra.
    # We currently use 6 as there are 6 cassandra hosts
    "coalesce_partitions": 6,
}


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_top_articles_daily",
    doc_md="Loads Hive pageview top articles data in cassandra daily",
    start_date=props.daily_dag_start_date,
    schedule="@daily",
    tags=default_tags + ["daily"],
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    sensor = dataset("hive_wmf_pageview_hourly").get_sensor_for(daily_dag)

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.daily_dag_hql,
        query_parameters={
            **default_query_parameters,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
        },
        # Spark resources configuration needs to be tweaked for this job
        # as it processes relatively big data
        executor_memory=props.daily_executor_memory,
        executor_cores=props.daily_executor_cores,
        driver_memory=props.daily_driver_memory,
        driver_cores=props.daily_driver_cores,
        conf={
            # Hack: We don't want to override default_args["conf"] already set,
            # we just want to add to them, so we reset them in our override.
            **daily_dag.default_args["conf"],
            "spark.dynamicAllocation.maxExecutors": 64,
            "spark.executor.memoryOverhead": 2048,
        },
        spark_datahub_lineage_enabled=False,  # Cassandra tables are not part of Hive dataPlatform
    )

    sensor >> etl


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_top_articles_monthly",
    doc_md="Loads Hive pageview top articles data in cassandra monthly",
    start_date=props.monthly_dag_start_date,
    schedule="@monthly",
    tags=default_tags + ["monthly"],
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    sensor = dataset("hive_wmf_pageview_hourly").get_sensor_for(monthly_dag)

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.monthly_dag_hql,
        query_parameters={
            **default_query_parameters,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "work_partitions": 1024,
        },
        # Spark resources configuration needs to be tweaked for this job
        # as it processes relatively big data
        executor_memory=props.monthly_executor_memory,
        executor_cores=props.monthly_executor_cores,
        driver_memory=props.monthly_driver_memory,
        driver_cores=props.monthly_driver_cores,
        conf={
            # Hack: We don't want to override default_args["conf"] already set,
            # we just want to add to them, so we reset them in our override.
            **monthly_dag.default_args["conf"],
            "spark.dynamicAllocation.maxExecutors": 96,
            "spark.executor.memoryOverhead": 6144,
        },
        spark_datahub_lineage_enabled=False,  # Cassandra tables are not part of Hive dataPlatform
    )

    sensor >> etl
