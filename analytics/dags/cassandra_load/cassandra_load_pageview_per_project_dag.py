"""
Loads Hive pageview per project into Cassandra

The loading is done with 3 dags: hourly, daily and monthly.

"""

from datetime import datetime, timedelta

from airflow.sensors.external_task import ExternalTaskSensor

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.helper import get_hourly_execution_dates_fn

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Data source and destination.
    hive_projectview_table="wmf.projectview_hourly",
    cassandra_pageview_per_project_table="aqs.local_group_default_T_pageviews_per_project_v2.data",
    # DAG start dates.
    hourly_dag_start_date=datetime(2023, 7, 18, 0),
    daily_dag_start_date=datetime(2023, 7, 18),
    monthly_dag_start_date=datetime(2023, 7, 1),
    # HQL query paths
    hourly_dag_hql=f"{hql_directory}/cassandra/hourly/load_cassandra_pageview_per_project_hourly.hql",
    daily_dag_hql=f"{hql_directory}/cassandra/daily/load_cassandra_pageview_per_project_daily.hql",
    monthly_dag_hql=f"{hql_directory}/cassandra/monthly/load_cassandra_pageview_per_project_monthly.hql",
    # SLAs and alerts email.
    hourly_dag_sla=timedelta(hours=6),
    daily_dag_sla=timedelta(hours=10),
    monthly_dag_sla=timedelta(days=5),
    alerts_email=alerts_email,
)


default_tags = ["from_hive", "to_cassandra", "uses_hql", "requires_wmf_projectview_hourly"]


default_query_parameters = {
    "source_table": props.hive_projectview_table,
    "destination_table": props.cassandra_pageview_per_project_table,
    # Setting coalesce_partitions to 6 is important
    # as it defines how many parallel loaders we use for cassandra.
    # We currently use 6 as there are 6 cassandra hosts
    "coalesce_partitions": 6,
}


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_per_project_hourly",
    doc_md="Loads Hive pageview per project data in cassandra hourly",
    start_date=props.hourly_dag_start_date,
    schedule="@hourly",
    tags=default_tags + ["hourly"],
    sla=props.hourly_dag_sla,
    email=props.alerts_email,
) as hourly_dag:
    # T383804 - Cascading backfill experiment
    # The sensor has been converted from a NamedHivePartitionSensor to an ExternalTaskSensor to wait for the upstream
    # task to finish before starting the current task. Necessary for cascading backfill.
    sensor = ExternalTaskSensor(
        task_id="wait_for_wmf_projectview_hourly_partitions",
        external_dag_id="projectview_hourly",
        external_task_id="aggregrate_pageview_to_projectview",
        poll_interval=5 * 60,  # 5 minutes
    )

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.hourly_dag_hql,
        query_parameters={
            **default_query_parameters,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
        },
        # The default spark resources configuration is enough for this job
        # as it processes relatively small data
        spark_datahub_lineage_enabled=False,  # Cassandra tables are not part of Hive dataPlatform
    )

    sensor >> etl


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_per_project_daily",
    doc_md="Loads Hive pageview per project data in cassandra daily",
    start_date=props.daily_dag_start_date,
    schedule="@daily",
    tags=default_tags + ["daily"],
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    # T383804 - Cascading backfill experiment
    # The sensor has been converted from a NamedHivePartitionSensor to an ExternalTaskSensor to wait for the upstream
    # task to finish before starting the current task. Necessary for cascading backfill.
    sensor = ExternalTaskSensor(
        task_id="wait_for_wmf_projectview_hourly_partitions",
        external_dag_id="projectview_hourly",
        external_task_id="aggregrate_pageview_to_projectview",
        execution_date_fn=get_hourly_execution_dates_fn(num_hours=24),
        poll_interval=5 * 60,  # 5 minutes
    )

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.daily_dag_hql,
        query_parameters={
            **default_query_parameters,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
        },
        spark_datahub_lineage_enabled=False,  # Cassandra tables are not part of Hive dataPlatform
        # The default spark resources configuration is enough for this job
        # as it processes relatively small data
    )

    sensor >> etl


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_per_project_monthly",
    doc_md="Loads Hive pageview per project data in cassandra monthly",
    start_date=props.monthly_dag_start_date,
    schedule="@monthly",
    tags=default_tags + ["monthly"],
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    # T383804 - Cascading backfill experiment
    # The sensor has been converted from a NamedHivePartitionSensor to an ExternalTaskSensor to wait for the upstream
    # task to finish before starting the current task. Necessary for cascading backfill.
    sensor = ExternalTaskSensor(
        task_id="wait_for_wmf_projectview_hourly_partitions",
        external_dag_id="projectview_hourly",
        external_task_id="aggregrate_pageview_to_projectview",
        execution_date_fn=get_hourly_execution_dates_fn(monthly=True),
        poll_interval=5 * 60,  # 5 minutes
    )

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.monthly_dag_hql,
        query_parameters={
            **default_query_parameters,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
        },
        # The default spark resources configuration is enough for this job
        # as it processes relatively small data
        spark_datahub_lineage_enabled=False,
    )

    sensor >> etl
