"""
Generates the 5 main Iceberg datasets of the Commons Impact Metrics data product monthly.
    * wmf_contributors.commons_category_metrics_snapshot
    * wmf_contributors.commons_media_file_metrics_snapshot
    * wmf_contributors.commons_pageviews_per_category_monthly
    * wmf_contributors.commons_pageviews_per_media_file_monthly
    * wmf_contributors.commons_edits

Uses the following data sources:
    * wmf_raw.mediawiki_page
    * wmf_raw.mediawiki_image
    * wmf_raw.mediawiki_imagelinks
    * wmf_raw.mediawiki_categorylinks
    * wmf_raw.mediawiki_revision
    * wmf_raw.mediawiki_private_actor
    * wmf.pageview_hourly
    * canonical_data.wikis

Only categories and media files that belong to category trees
present in the Commons category allow-list will be reported on.
"""

from datetime import datetime, timedelta

from airflow.models.baseoperator import chain
from airflow.sensors.external_task import ExternalTaskSensor

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    hdfs_temp_directory,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.partitions_builder import hours_of_month

# Folder in the refinery repository where the queries live.
query_folder = f"{hql_directory}/commons_impact_metrics"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted after re-deploying this script.
props = DagProperties(
    start_date=datetime(2023, 11, 1),
    # Source tables.
    mediawiki_page_table="wmf_raw.mediawiki_page",
    mediawiki_image_table="wmf_raw.mediawiki_image",
    mediawiki_imagelinks_table="wmf_raw.mediawiki_imagelinks",
    mediawiki_categorylinks_table="wmf_raw.mediawiki_categorylinks",
    mediawiki_revision_table="wmf_raw.mediawiki_revision",
    mediawiki_private_actor_table="wmf_raw.mediawiki_private_actor",
    wmf_pageview_hourly_table="wmf.pageview_hourly",
    canonical_data_wikis_table="canonical_data.wikis",
    # Temporary tables created and used during computation.
    # A suffix with the snapshot value will be added to them (_YYYY-MM).
    category_and_media_table="tmp.category_and_media",
    category_and_media_with_usage_map_table="tmp.category_and_media_with_usage_map",
    category_and_media_with_usage_map_location=f"{hdfs_temp_directory}/category_and_media_with_usage_map",
    # Output tables.
    commons_category_metrics_snapshot_table="wmf_contributors.commons_category_metrics_snapshot",
    commons_media_file_metrics_snapshot_table="wmf_contributors.commons_media_file_metrics_snapshot",
    commons_pageviews_per_category_monthly_table="wmf_contributors.commons_pageviews_per_category_monthly",
    commons_pageviews_per_media_file_monthly_table="wmf_contributors.commons_pageviews_per_media_file_monthly",
    commons_edits_table="wmf_contributors.commons_edits",
    # Spark logic.
    category_graph_jar=artifact("refinery-job-0.2.37-shaded.jar"),
    category_graph_allow_list_url=(
        "https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-"
        "/raw/main/analytics/dags/commons/commons_category_allow_list.tsv"
    ),
    category_graph_max_distance_to_primary=7,
    category_graph_intermediate_partitions=1024,
    category_graph_output_partitions=64,
    category_graph_checkpoint_directory=f"{hdfs_temp_directory}/commons_impact_metrics_checkpoints",
    category_graph_spark_config={
        "executor_cores": 4,
        "executor_memory": "32G",
        "driver_cores": 2,
        "driver_memory": "8G",
        "conf": {
            "spark.dynamicAllocation.maxExecutors": 64,
            "spark.executor.memoryOverhead": "3G",
            "spark.sql.shuffle.partitions": 1024,
            "spark.yarn.maxAppAttempts": 1,
            "spark.graphx.pregel.checkpointInterval": 10,
        },
    },
    # HQL logic.
    category_and_media_with_usage_map_hql=f"{query_folder}/category_and_media_with_usage_map.hql",
    commons_category_metrics_snapshot_hql=f"{query_folder}/commons_category_metrics_snapshot.hql",
    commons_media_file_metrics_snapshot_hql=f"{query_folder}/commons_media_file_metrics_snapshot.hql",
    commons_pageviews_per_category_monthly_hql=f"{query_folder}/commons_pageviews_per_category_monthly.hql",
    commons_pageviews_per_media_file_monthly_hql=f"{query_folder}/commons_pageviews_per_media_file_monthly.hql",
    commons_edits_hql=f"{query_folder}/commons_edits.hql",
    hql_output_partitions=4,
    hql_spark_config={
        "executor_cores": 4,
        "executor_memory": "32G",
        "driver_cores": 2,
        "driver_memory": "8G",
        "conf": {
            "spark.dynamicAllocation.maxExecutors": 32,
            "spark.executor.memoryOverhead": "2G",
            "spark.yarn.maxAppAttempts": 2,
        },
    },
    # Data clean up logic.
    data_clean_up_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # Alerting.
    sla=timedelta(days=10),
    alerts_email=alerts_email,
)

tags = [
    "monthly",
    "from_hive",
    "to_iceberg",
    "uses_spark",
    "uses_hql",
    "requires_wmf_raw_mediawiki_page",
    "requires_wmf_raw_mediawiki_image",
    "requires_wmf_raw_mediawiki_imagelinks",
    "requires_wmf_raw_mediawiki_categorylinks",
    "requires_wmf_raw_mediawiki_revision",
    "requires_wmf_raw_mediawiki_private_actor",
    "requires_wmf_pageview_hourly",
    "requires_canonical_data_wikis",
]

with create_easy_dag(
    dag_id="commons_impact_metrics_monthly",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=tags,
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # Convenient values derived from the DAG run's data interval start.
    year_month = "{{data_interval_start | to_ds_month}}"
    year_month_underscore = "{{data_interval_start | to_ds_month_underscore}}"
    category_and_media_table = f"{props.category_and_media_table}_{year_month_underscore}"
    category_graph_checkpoint_directory = f"{props.category_graph_checkpoint_directory}/{year_month}"
    category_and_media_with_usage_map_table = f"{props.category_and_media_with_usage_map_table}_{year_month_underscore}"
    category_and_media_with_usage_map_location = f"{props.category_and_media_with_usage_map_location}/{year_month}"

    wait_for_mediawiki_page = ExternalTaskSensor(
        task_id="wait_for_mediawiki_page",
        external_dag_id="mediawiki_history_load",
        external_task_id="write_page_table_partitioned_file",
    )

    wait_for_mediawiki_image = ExternalTaskSensor(
        task_id="wait_for_mediawiki_image",
        external_dag_id="mediawiki_history_load",
        external_task_id="write_image_table_partitioned_file",
    )

    wait_for_mediawiki_imagelinks = ExternalTaskSensor(
        task_id="wait_for_mediawiki_imagelinks",
        external_dag_id="mediawiki_history_load",
        external_task_id="write_imagelinks_table_partitioned_file",
    )

    wait_for_mediawiki_categorylinks = ExternalTaskSensor(
        task_id="wait_for_mediawiki_categorylinks",
        external_dag_id="mediawiki_history_load",
        external_task_id="write_categorylinks_table_partitioned_file",
    )

    wait_for_mediawiki_revision = ExternalTaskSensor(
        task_id="wait_for_mediawiki_revision",
        external_dag_id="mediawiki_history_load",
        external_task_id="write_revision_table_partitioned_file",
    )

    wait_for_mediawiki_private_actor = ExternalTaskSensor(
        task_id="wait_for_mediawiki_private_actor",
        external_dag_id="mediawiki_history_load",
        external_task_id="write_actor_table_partitioned_file",
    )

    wait_for_wmf_pagewiew_hourly = ExternalTaskSensor(
        task_id="wait_for_wmf_pagewiew_hourly",
        external_dag_id="pageview_hourly",
        external_task_id="move_data_to_archive",
        execution_date_fn=hours_of_month,
    )

    compute_category_graph = SparkSubmitOperator(
        task_id="compute_category_graph",
        application=props.category_graph_jar,
        java_class="org.wikimedia.analytics.refinery.job.CommonsCategoryGraphBuilder",
        application_args={
            "--page-table": props.mediawiki_page_table,
            "--categorylinks-table": props.mediawiki_categorylinks_table,
            "--mediawiki-snapshot": year_month,
            "--category-allow-list-url": props.category_graph_allow_list_url,
            "--max-distance-to-primary": props.category_graph_max_distance_to_primary,
            "--output-table": category_and_media_table,
            "--intermediate-partitions": props.category_graph_intermediate_partitions,
            "--output-partitions": props.category_graph_output_partitions,
            "--checkpoint-directory": category_graph_checkpoint_directory,
        },
        **props.category_graph_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    compute_usage_map = SparkSqlOperator(
        task_id="compute_usage_map",
        sql=props.category_and_media_with_usage_map_hql,
        query_parameters={
            "category_and_media_table": category_and_media_table,
            "mediawiki_page_table": props.mediawiki_page_table,
            "mediawiki_imagelinks_table": props.mediawiki_imagelinks_table,
            "pageview_hourly_table": props.wmf_pageview_hourly_table,
            "canonical_data_wikis_table": props.canonical_data_wikis_table,
            "category_and_media_with_usage_map_table": category_and_media_with_usage_map_table,
            "category_and_media_with_usage_map_location": category_and_media_with_usage_map_location,
            "snapshot": year_month,
            "coalesce_partitions": props.hql_output_partitions,
        },
        **props.hql_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    compute_category_metrics = SparkSqlOperator(
        task_id="compute_category_metrics",
        sql=props.commons_category_metrics_snapshot_hql,
        query_parameters={
            "category_and_media_with_usage_map_table": category_and_media_with_usage_map_table,
            "commons_category_metrics_snapshot_table": props.commons_category_metrics_snapshot_table,
            "year_month": year_month,
            "coalesce_partitions": props.hql_output_partitions,
        },
        **props.hql_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    compute_media_file_metrics = SparkSqlOperator(
        task_id="compute_media_file_metrics",
        sql=props.commons_media_file_metrics_snapshot_hql,
        query_parameters={
            "category_and_media_with_usage_map_table": category_and_media_with_usage_map_table,
            "mediawiki_image_table": props.mediawiki_image_table,
            "commons_media_file_metrics_snapshot_table": props.commons_media_file_metrics_snapshot_table,
            "year_month": year_month,
            "coalesce_partitions": props.hql_output_partitions,
        },
        **props.hql_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    compute_pageviews_per_category = SparkSqlOperator(
        task_id="compute_pageviews_per_category",
        sql=props.commons_pageviews_per_category_monthly_hql,
        query_parameters={
            "category_and_media_with_usage_map_table": category_and_media_with_usage_map_table,
            "commons_pageviews_per_category_monthly_table": props.commons_pageviews_per_category_monthly_table,
            "year_month": year_month,
            "coalesce_partitions": props.hql_output_partitions,
        },
        **props.hql_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    compute_pageviews_per_media_file = SparkSqlOperator(
        task_id="compute_pageviews_per_media_file",
        sql=props.commons_pageviews_per_media_file_monthly_hql,
        query_parameters={
            "category_and_media_with_usage_map_table": category_and_media_with_usage_map_table,
            "commons_pageviews_per_media_file_monthly_table": props.commons_pageviews_per_media_file_monthly_table,
            "year_month": year_month,
            "coalesce_partitions": props.hql_output_partitions,
        },
        **props.hql_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    compute_edits = SparkSqlOperator(
        task_id="compute_edits",
        sql=props.commons_edits_hql,
        query_parameters={
            "category_and_media_with_usage_map_table": category_and_media_with_usage_map_table,
            "mediawiki_revision_table": props.mediawiki_revision_table,
            "mediawiki_actor_table": props.mediawiki_private_actor_table,
            "commons_edits_table": props.commons_edits_table,
            "year_month": year_month,
            "coalesce_partitions": props.hql_output_partitions,
        },
        **props.hql_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    drop_category_and_media = SparkSqlOperator(
        task_id="drop_category_and_media",
        sql=f"DROP TABLE IF EXISTS {category_and_media_table};",
        **props.data_clean_up_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    drop_category_and_media_with_usage_map = SparkSqlOperator(
        task_id="drop_category_and_media_with_usage_map",
        sql=f"DROP TABLE IF EXISTS {category_and_media_with_usage_map_table};",
        **props.data_clean_up_spark_config,
        spark_datahub_lineage_enabled=False,  # Temp tables put too much noise into DataHub
    )

    chain(
        # Wait for source data.
        wait_for_mediawiki_page,
        wait_for_mediawiki_image,
        wait_for_mediawiki_imagelinks,
        wait_for_mediawiki_categorylinks,
        wait_for_mediawiki_revision,
        wait_for_mediawiki_private_actor,
        wait_for_wmf_pagewiew_hourly,
        # Compute output data.
        compute_category_graph,
        compute_usage_map,
        compute_category_metrics,
        compute_media_file_metrics,
        compute_pageviews_per_category,
        compute_pageviews_per_media_file,
        compute_edits,
        # Clean up temporary data.
        drop_category_and_media,
        drop_category_and_media_with_usage_map,
    )
