"""
Loads the latest MediaWiki history reduced Druid datasource name
to Cassandra's AQS config table.

The loading happens monthly, after mediawiki_history_reduced is in Druid.
AQS will read the config to automatically determine which snapshot to use.

NOTE: We've chosen to temporarily use Cassandra as a config store for AQS
as a convenient interim solution until the Dataset Config System
(https://phabricator.wikimedia.org/T354557) is available.
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    hql_directory,
)
from wmf_airflow_common.config.dag_default_args import druid_default_conf
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.druid import DruidSegmentSensor

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Data source and destination.
    mw_history_reduced_datasource="mediawiki_history_reduced_{{data_interval_start | to_ds_month_underscore}}",
    cassandra_aqs_config_table="aqs.aqs.config",
    # DAG start date.
    start_date=datetime(2024, 1, 1),
    # HQL query path.
    load_cassandra_hql=f"{hql_directory}/cassandra/load_cassandra_aqs_config.hql",
    # SLA and alerts email.
    sla=timedelta(days=4),
    alerts_email=alerts_email,
    # Public Druid API host for datasource sensing.
    public_druid_api_host="druid1007.eqiad.wmnet",
    # Make the memory tunable temporarily, just to try and figure out what skein is doing
    driver_memory="2G",
    executor_memory="2G",
)


with create_easy_cassandra_loading_dag(
    dag_id="mediawiki_history_shapshot_config",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "to_cassandra", "uses_hql", "requires_druid_mediawiki_history_reduced"],
    sla=props.sla,
    email=props.alerts_email,
    default_args=druid_default_conf,
) as dag:
    # Do not use dataset().get_sensor_for() since we need to sense
    # all segments since the start of wiki time (2001-01).
    # The mediawiki_history_reduced datasource is loaded to Druid
    # as a collection of monthly segments since 2001-01.
    sensor = DruidSegmentSensor(
        task_id="wait_for_druid_data",
        # Explicitly set the Druid API host to the public one.
        druid_api_host=props.public_druid_api_host,
        datasource=props.mw_history_reduced_datasource,
        from_timestamp=datetime(2001, 1, 1).isoformat(),
        to_timestamp="{{data_interval_end.isoformat()}}",
        granularity="@monthly",
        poke_interval=timedelta(minutes=60).total_seconds(),
    )

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.load_cassandra_hql,
        query_parameters={
            "property_name": "mediawiki_history_reduced_druid_datasource",
            "property_value": props.mw_history_reduced_datasource,
            "aqs_config_table": props.cassandra_aqs_config_table,
        },
        # This job just loads a single row of data.
        # Use a minimalistic Spark config.
        # TODO We should be able to set master="local" here, but it seems
        # Skein local mode does not interact with Cassandra correctly.
        driver_cores=1,
        driver_memory=props.driver_memory,
        executor_cores=1,
        executor_memory=props.executor_memory,
        spark_datahub_lineage_enabled=False,
    )

    sensor >> etl
