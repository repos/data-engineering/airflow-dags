import functools
from typing import List, Set

import requests
from airflow.decorators import task


def fetch_dblist(url: str, dag_id: str) -> Set[str]:
    response = requests.get(
        url=url,
        headers={
            "User-Agent": f"WMF-Airflow-Analytics-{dag_id}/1.0.0 "
            "(https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags)"
        },
        timeout=60,
    )
    response.raise_for_status()
    content = response.text
    dblist = content.splitlines()
    if dblist[0].startswith("#"):  # removes comment from first line
        dblist.pop(0)
    return set(dblist)


@task(do_xcom_push=True)
def calculate_effective_dblist(
    dag_id: str, dblist_include_urls: List[str], dblist_exclude_urls: List[str], dblist_exclude: List[str]
) -> List[str]:
    includes_lists = [fetch_dblist(url, dag_id) for url in dblist_include_urls]
    includes_set: Set[str] = functools.reduce(lambda a, b: a | b, includes_lists, set())

    excludes_lists = [fetch_dblist(url, dag_id) for url in dblist_exclude_urls]
    excludes_set: Set[str] = functools.reduce(lambda a, b: a | b, excludes_lists, set())
    excludes_set = excludes_set | set(dblist_exclude)

    effective_set = includes_set - excludes_set
    effective_ordered_list = sorted(list(effective_set))

    return effective_ordered_list
