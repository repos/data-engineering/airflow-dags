"""
This job is part of the MediaWiki Content pipelines (formerly known as the Dumps 2.0 pipelines).

This takes up-to-date metadata and content from Iceberg table wmf_content.mediawiki_content_history_v1 and publishes
it as XML.

The business logic is encapsulated in a spark scala job that's part of the refinery-job jar:
https://github.com/wikimedia/analytics-refinery-source/blob/master/refinery-job/src/main/scala/org/wikimedia/analytics/refinery/job/mediawikidumper/MediawikiDumper.scala
"""
from datetime import datetime, timedelta

from mergedeep import merge

from analytics.config.dag_config import artifact, create_easy_dag
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2025, 1, 14),
    sla=timedelta(days=10),
    refinery_job_jar=artifact("refinery-job-0.2.29-shaded.jar"),
    hive_mediawiki_content_history_v1_table="wmf_content.mediawiki_content_history_v1",
    # TODO: when we go live, update puppet: modules/dumps/manifests/web/fetches/stats.pp
    output_hdfs_root_path="/wmf/data/archive/content_dump_test",
    # Spark job tuning
    driver_memory="16G",
    driver_cores="4",
    executor_memory="8G",
    executor_cores="2",
    num_executors="32",
    spark_executor_memoryOverhead="2G",
)


with create_easy_dag(
    dag_id="mw_content_publish_mw_content_history_to_xml",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=[
        "monthly",
        "from_iceberg",
        "to_xml",
        "requires_mediawiki_content_history_v1",
        "uses_spark",
        "mediawiki_content",
    ],
    sla=props.sla,
    email="xcollazo@wikimedia.org"  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    # TODO: Iceberg sensors are not ready yet
    # sensor = dataset("iceberg_wmf_dumps_wikitext_raw").get_sensor_for(dag)

    # TODO: pass lists of wikis to job
    publishers = [
        SparkSubmitOperator(
            task_id=f"publish_{wiki_id}_to_xml",
            executor_memory=props.executor_memory,
            executor_cores=props.executor_cores,
            driver_memory=props.driver_memory,
            driver_cores=props.driver_cores,
            conf=merge(
                {},
                dag.default_args["conf"],
                {
                    "spark.dynamicAllocation.maxExecutors": props.num_executors,
                    "spark.executor.memoryOverhead": props.spark_executor_memoryOverhead,
                },
            ),
            application=props.refinery_job_jar,
            java_class="org.wikimedia.analytics.refinery.job.mediawikidumper.MediawikiDumper",
            application_args=[
                # The name of the wiki: enwiki, frwiki, etc.
                "--wiki_id",
                wiki_id,
                # Date in YYYY-MM-DD. The job will publish data until this date 00:00
                "--publish_until",
                "{{data_interval_start | to_ds}}",
                # The Iceberg table to read data from
                "--source_table",
                props.hive_mediawiki_content_history_v1_table,
                # The output folder where the XML files will be written
                "--output_folder",
                props.output_hdfs_root_path + f"/{wiki_id}/{{{{data_interval_start | to_ds_nodash}}}}/",
                # NOTE: these optional parameters may need to vary by wiki
                # Overhead in kB to add to the revision page contents to estimate the page size
                # (optional) "--page_size_overhead",
                # The maximum size of the target XML files in MB
                # (optional) "--max_target_file_size",
                # The minimum size of a chunk of pages in MB. Internally used for partitioning. See Upper
                # (optional) "--min_chunk_size",
            ],
            spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
        )
        for wiki_id in ["simplewiki"]
    ]
