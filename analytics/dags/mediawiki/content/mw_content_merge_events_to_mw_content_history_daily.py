"""
This job is part of the MediaWiki Content pipelines (formerly known as the Dumps 2.0 pipelines).

It consumes content event data from multiple streams and merges them into an
Iceberg table containing all existing (wiki_id, revisions) tuples. This target table is meant to be
an intermediary table to do analytics from, and to generate dumps from.

This pipeline is implemented in PySpark. The jobs de-duplicate incoming events and mutate the target table via
Iceberg MERGE INTOs and DELETEs. Although these are PySpark jobs, the vast majority of the code is SQL. We do need some
spark glue code to generate pushdown predicates to the constructed SQL statements.

Code for the pyspark jobs at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/process_events.py
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/process_visibility_events.py
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    artifact,
    create_easy_dag,
    dataset,
    pool,
    spark_3_3_2_conf,
)
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # DAG settings
    start_date=datetime(2025, 1, 14),
    sla=timedelta(hours=12),
    conda_env=artifact("mediawiki-content-dump-0.3.0-v0.3.0.conda.tgz"),
    # target table
    hive_mediawiki_content_history_v1_table="wmf_content.mediawiki_content_history_v1",
    # source tables
    hive_mediawiki_page_content_change_table="event.mediawiki_page_content_change_v1",
    hive_revision_visibility_change="event.mediawiki_revision_visibility_change",
    hive_mediawiki_content_history_reconcile_enriched_v1="event.mediawiki_content_history_reconcile_enriched_v1",
    # Spark job tuning
    driver_memory="16G",
    driver_cores="4",
    executor_memory="16G",
    executor_cores="2",
    max_executors="64",
    spark_driver_maxResultSize="8G",
    # try to keep shuffler partitions as low as possible so that the final file fanout is low as well
    # but when doing copy-on-write, we require 1024 partitions to handle the rewrites of files. T377999 & T376713.
    spark_sql_shuffle_partitions="1024",
    # avoid java.lang.StackOverflowError when generating MERGE INTO predicate pushdowns
    spark_extraJavaOptions="-Xss8m",
    # Fetching HDFS BlockLocations at query plan time has a time penalty of multiple minutes per query
    # With this flag we could disable this at the cost of not having locality
    spark_sql_iceberg_locality_enabled="true",
)


with create_easy_dag(
    dag_id="mw_content_merge_events_to_mw_content_history_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=[
        "daily",
        "from_hive",
        "to_iceberg",
        "requires_mediawiki_page_content_change_v1",
        "requires_mediawiki_revision_visibility_change",
        "uses_spark",
        "mediawiki_content",
    ],
    sla=props.sla,
    max_active_runs=1,  # MERGEs will step into each other, let's run them serially.
) as dag:
    content_sensor = dataset("hive_event_mediawiki_page_content_change_v1").get_sensor_for(dag)
    visibility_sensor = dataset("hive_event_mediawiki_revision_visibility_change").get_sensor_for(dag)
    reconciliation_sensor = dataset("hive_event_mediawiki_content_history_reconcile_enriched_v1").get_sensor_for(dag)

    common_spark_conf = {
        **spark_3_3_2_conf,
        "spark.driver.maxResultSize": props.spark_driver_maxResultSize,
        "spark.dynamicAllocation.maxExecutors": props.max_executors,
        "spark.sql.shuffle.partitions": props.spark_sql_shuffle_partitions,
        "spark.sql.iceberg.locality.enabled": props.spark_sql_iceberg_locality_enabled,
    }

    util.dict_add_or_append_string_value(
        common_spark_conf, "spark.driver.extraJavaOptions", props.spark_extraJavaOptions
    )
    util.dict_add_or_append_string_value(
        common_spark_conf, "spark.executor.extraJavaOptions", props.spark_extraJavaOptions
    )

    # Usage: process_events.py  --source_table source.table
    #                           --target_table target.table
    #                           --year 2023
    #                           --month 2
    #                           --day 15
    #                           --event_categories revisions page_deletes
    content_args = [
        "--source_table",
        props.hive_mediawiki_page_content_change_table,
        "--target_table",
        props.hive_mediawiki_content_history_v1_table,
        "--year",
        "{{data_interval_start.year}}",
        "--month",
        "{{data_interval_start.month}}",
        "--day",
        "{{data_interval_start.day}}",
        "--event_categories",
        "revisions",
        "page_deletes",
        "page_moves",
    ]

    process_events = SparkSubmitOperator.for_virtualenv(
        task_id="spark_process_events",
        virtualenv_archive=props.conda_env,
        entry_point="bin/process_events.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf=common_spark_conf,
        launcher="skein",
        application_args=content_args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
        # This pool with 1 slot allows us to have multiple DAGs with
        # MERGE INTOs that effectively run serially against table "wmf_dumps.wikitext_raw", thus mimicking a mutex.
        pool=pool("mutex_for_wmf_content_mediawiki_content_history_v1"),
        spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
    )

    # Usage: process_visibility_events.py  --source_table source.table
    #                                      --target_table target.table
    #                                      --year 2023
    #                                      --month 2
    #                                      --day 15
    visibility_args = [
        "--source_table",
        props.hive_revision_visibility_change,
        "--target_table",
        props.hive_mediawiki_content_history_v1_table,
        "--year",
        "{{data_interval_start.year}}",
        "--month",
        "{{data_interval_start.month}}",
        "--day",
        "{{data_interval_start.day}}",
    ]

    process_visibility_events = SparkSubmitOperator.for_virtualenv(
        task_id="spark_process_visibility_events",
        virtualenv_archive=props.conda_env,
        entry_point="bin/process_visibility_events.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf=common_spark_conf,
        launcher="skein",
        application_args=visibility_args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
        # This pool with 1 slot allows us to have multiple DAGs with
        # MERGE INTOs that effectively run serially against table "wmf_dumps.wikitext_raw", thus mimicking a mutex.
        pool=pool("mutex_for_wmf_content_mediawiki_content_history_v1"),
        spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
    )

    # Usage: process_events.py  --source_table source.table
    #                           --target_table target.table
    #                           --year 2023
    #                           --month 2
    #                           --day 15
    #                           --event_categories revisions page_deletes
    reconciliation_content_args = [
        "--source_table",
        props.hive_mediawiki_content_history_reconcile_enriched_v1,
        "--target_table",
        props.hive_mediawiki_content_history_v1_table,
        "--year",
        "{{data_interval_start.year}}",
        "--month",
        "{{data_interval_start.month}}",
        "--day",
        "{{data_interval_start.day}}",
        "--event_categories",
        "revisions",
        "page_deletes",
        "page_moves",
    ]

    process_reconciliation_events = SparkSubmitOperator.for_virtualenv(
        task_id="spark_process_reconciliation_events",
        virtualenv_archive=props.conda_env,
        entry_point="bin/process_events.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf=common_spark_conf,
        launcher="skein",
        application_args=reconciliation_content_args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
        # This pool with 1 slot allows us to have multiple DAGs with
        # MERGE INTOs that effectively run serially against table "wmf_dumps.wikitext_raw", thus mimicking a mutex.
        pool=pool("mutex_for_wmf_content_mediawiki_content_history_v1"),
        spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
    )

    (
        content_sensor
        >> process_events
        >> visibility_sensor
        >> process_visibility_events
        >> reconciliation_sensor
        >> process_reconciliation_events
    )
