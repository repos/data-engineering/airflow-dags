"""
This DAG is part of the MediaWiki Content pipelines (formerly known as the Dumps 2.0 pipelines).

This job checks for inconsistencies between the Analytics Replicas and wmf_content.mediawiki_content_history_v1. If
any are found, they are recorded on wmf_content.inconsistencies_of_mediawiki_content_history_v1. After all wikis have
been checked for inconsistencies, a separate job is started to emit the inconsistencies to Kafka. Inconsistencies are
marked as emitted in the inconsistencies table as they are generated.

The inconsistencies table can also be utilized to do data quality metrics and alerts.

This job is run daily and covers the last 24 hours of data.

More info about the pyspark job at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/consistency_check.py
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/emit_reconcile_events_to_kafka.py
"""

from datetime import datetime, timedelta

from airflow.decorators import task_group
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from airflow.sensors.external_task import ExternalTaskSensor
from mergedeep import merge

from analytics.config.dag_config import (
    artifact,
    create_easy_dag,
    pool,
    spark_3_3_2_conf,
)
from analytics.dags.mediawiki.content.mw_content_utils import calculate_effective_dblist
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.util import sanitize_string

props = DagProperties(
    # DAG settings
    start_date=datetime(2025, 1, 14),
    sla=timedelta(hours=12),
    conda_env=artifact("mediawiki-content-dump-0.3.0-v0.3.0.conda.tgz"),
    # list of URIs pointing to dblist of wikis we want to include. Will be deduplicated.
    dblist_include_urls=["https://noc.wikimedia.org/conf/dblists/open.dblist"],
    # list of URIs pointing to dblist of wikis we want to exclude. Will be deduplicated.
    dblist_exclude_urls=["https://noc.wikimedia.org/conf/dblists/private.dblist"],
    # manual list of wikis that we want to exclude
    dblist_exclude=[],
    # event stream settings
    event_stream_name="mediawiki.content_history_reconcile.v1",
    event_schema_version="1.2.0",
    # 7, 8 and 10 all live on different racks as per
    # https://github.com/wikimedia/operations-puppet/blob/3031ae80f7bd0cc98228342a14d92e5920cd0447/hieradata/common.yaml#L834-L863
    kafka_bootstrap_servers="kafka-jumbo1007.eqiad.wmnet:9092,kafka-jumbo1008.eqiad.wmnet:9092,"
    "kafka-jumbo1010.eqiad.wmnet:9092",
    # mariadb settings
    mariadb_username="research",
    # File path to the password file that the PySpark process will attempt to read.
    # The effective user of the process needs to have access to this file.
    mariadb_password_file="/user/analytics/mysql-analytics-research-client-pw.txt",
    mariadb_jdbc_driver_path=artifact("mysql-connector-j-8.2.0.jar"),
    # used to register our custom mediawiki-jdbc Spark datasource
    refinery_spark_jar_path=artifact("refinery-spark-0.2.54.jar"),
    # source tables
    hive_mediawiki_content_history_v1_table="wmf_content.mediawiki_content_history_v1",
    hive_wikis_table="canonical_data.wikis",
    hive_namespaces_table="wmf_raw.mediawiki_project_namespace_map",
    # target table
    hive_wikitext_inconsistent_rows_table="wmf_content.inconsistent_rows_of_mediawiki_content_history_v1",
    # metrics table and required jars
    hive_metrics_table="wmf_data_ops.data_quality_metrics",
    refinery_job_jar_path=artifact("refinery-job-0.2.54-shaded.jar"),
    refinery_core_jar_path=artifact("refinery-core-0.2.54.jar"),
    # reconcile Spark jobs tuning
    reconcile_driver_memory="8G",
    reconcile_driver_cores="4",
    reconcile_executor_memory="8G",
    reconcile_executor_cores="2",
    # metrics Spark jobs tuning
    metrics_driver_memory="16G",
    metrics_driver_cores="4",
    metrics_executor_memory="12G",
    metrics_executor_cores="2",
    # general Spark tuning
    max_executors="16",
    # keep shuffler partitions low so that the final file fanout is low as well
    spark_sql_shuffle_partitions="64",
    # Airflow task parallelization
    max_active_tasks=16,
    # For call to 'emit_reconcile_events_to_kafka.py': The maximum amount of events to generate per event category
    # (see code for categories) in one iteration of the algorithm.
    max_events_per_iteration=100_000,
)


with create_easy_dag(
    dag_id="mw_content_reconcile_mw_content_history_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=[
        "daily",
        "from_iceberg",
        "to_iceberg",
        "requires_mediawiki_content_history_v1",
        "uses_spark",
        "mediawiki_content",
    ],
    sla=props.sla,
    max_active_runs=1,
    max_active_tasks=props.max_active_tasks,
) as dag:
    # We want to pickup whatever the latest monthly snapshot of mediawiki_project_namespace_map is available.
    # This implies that, if we create a new wiki mid-month, we will effectively not be doing reconcile on it
    # until the next snapshot is available since we do INNER JOINs against this table. That is ok.
    namespace_sensor = NamedHivePartitionSensor(
        task_id="wait_for_wmf_raw_mediawiki_project_namespace_map",
        partition_names=[
            f"{props.hive_namespaces_table}/snapshot={{{{ data_interval_start | start_of_previous_month | to_ds_month() }}}}"  # noqa
        ],
        poke_interval=timedelta(minutes=60).total_seconds(),
        dag=dag,
    )

    merge_events_sensor = ExternalTaskSensor(
        task_id="wait_for_mw_content_merge_events_to_mw_content_history_daily_dag",
        external_dag_id="mw_content_merge_events_to_mw_content_history_daily",
    )

    common_spark_conf = {
        **spark_3_3_2_conf,
        "spark.dynamicAllocation.maxExecutors": props.max_executors,
        "spark.sql.shuffle.partitions": props.spark_sql_shuffle_partitions,
    }
    util.dict_add_or_append_string_value(common_spark_conf, "spark.jars", props.mariadb_jdbc_driver_path, ",")
    util.dict_add_or_append_string_value(common_spark_conf, "spark.jars", props.refinery_spark_jar_path, ",")

    emit_reconcile_events_spark_conf = merge({}, common_spark_conf)
    util.dict_add_or_append_string_value(
        emit_reconcile_events_spark_conf, "spark.jars.packages", "org.wikimedia:eventutilities-spark:1.4.1", ","
    )

    compute_metrics_spark_conf = merge({}, common_spark_conf)
    util.dict_add_or_append_string_value(compute_metrics_spark_conf, "spark.jars", props.refinery_job_jar_path, ",")
    util.dict_add_or_append_string_value(compute_metrics_spark_conf, "spark.jars", props.refinery_core_jar_path, ",")

    @task_group()
    def reconcile(wiki_id: str):
        # this task does consistency checks between MariaDB replicas and props.hive_mediawiki_content_history_v1_table.
        # Any inconsistencies found are logged in props.hive_wikitext_inconsistent_rows_table.
        spark_consistency_check = SparkSubmitOperator.for_virtualenv(
            task_id="spark_consistency_check",
            virtualenv_archive=props.conda_env,
            entry_point="bin/consistency_check.py",
            driver_memory=props.reconcile_driver_memory,
            driver_cores=props.reconcile_driver_cores,
            executor_memory=props.reconcile_executor_memory,
            executor_cores=props.reconcile_executor_cores,
            sla=None,  # SLA are not supported for mapped tasks
            # Override parallelization for this DAG to run more small jobs at once
            max_active_tis_per_dag=props.max_active_tasks,
            conf=common_spark_conf,
            launcher="skein",
            application_args=[
                "--wiki_id",
                wiki_id,
                "--target_table",
                props.hive_mediawiki_content_history_v1_table,
                "--results_table",
                props.hive_wikitext_inconsistent_rows_table,
                # the time window is the frequency of this DAG, which AOTW, is 1 day
                "--min_timestamp",
                "{{ data_interval_start | to_dt() }}",
                "--max_timestamp",
                "{{ data_interval_end | to_dt() }}",
                "--computation_class",
                "last-24h",
                "--computation_dt",
                "{{ data_interval_end | to_dt() }}",
                "--mariadb_username",
                props.mariadb_username,
                "--mariadb_password_file",
                props.mariadb_password_file,
            ],
            use_virtualenv_spark=True,
            default_env_vars={
                "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
                "SPARK_CONF_DIR": "/etc/spark3/conf",
            },
            spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
        )

        # this task consumes inconsistencies from props.hive_wikitext_inconsistent_rows_table
        # and emits page_change_v1 events to kafka to stream props.event_stream_name
        # this stream then is enriched separately by a flink app
        spark_emit_reconcile_events_to_kafka = SparkSubmitOperator.for_virtualenv(
            task_id="spark_emit_reconcile_events_to_kafka",
            virtualenv_archive=props.conda_env,
            entry_point="bin/emit_reconcile_events_to_kafka.py",
            driver_memory=props.reconcile_driver_memory,
            driver_cores=props.reconcile_driver_cores,
            executor_memory=props.reconcile_executor_memory,
            executor_cores=props.reconcile_executor_cores,
            sla=None,  # SLA are not supported for mapped tasks
            # Override parallelization for this DAG to run more small jobs at once
            max_active_tis_per_dag=props.max_active_tasks,
            conf=emit_reconcile_events_spark_conf,
            launcher="skein",
            application_args=[
                "--wiki_id",
                wiki_id,
                "--inconsistent_rows_table",
                props.hive_wikitext_inconsistent_rows_table,
                "--wikis_table",
                props.hive_wikis_table,
                "--namespaces_table",
                props.hive_namespaces_table,
                "--namespaces_table_snapshot",
                "{{ data_interval_start | start_of_previous_month | to_ds_month() }}",
                "--computation_class",
                "last-24h",
                "--computation_dt",
                "{{ data_interval_end | to_dt() }}",
                "--max_events_per_iteration",
                props.max_events_per_iteration,
                "--event_stream_name",
                props.event_stream_name,
                "--event_schema_version",
                props.event_schema_version,
                "--kafka_bootstrap_servers",
                props.kafka_bootstrap_servers,
                "--mariadb_username",
                props.mariadb_username,
                "--mariadb_password_file",
                props.mariadb_password_file,
            ],
            use_virtualenv_spark=True,
            default_env_vars={
                "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
                "SPARK_CONF_DIR": "/etc/spark3/conf",
            },
            spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
        )

        spark_consistency_check >> spark_emit_reconcile_events_to_kafka

    dblist = calculate_effective_dblist(
        dag_id=dag.dag_id,
        dblist_include_urls=props.dblist_include_urls,
        dblist_exclude_urls=props.dblist_exclude_urls,
        dblist_exclude=props.dblist_exclude,
    )

    compute_metrics = SparkSubmitOperator.for_virtualenv(
        task_id="compute_metrics",
        virtualenv_archive=props.conda_env,
        entry_point="bin/compute_metrics.py",
        driver_memory=props.metrics_driver_memory,
        driver_cores=props.metrics_driver_cores,
        executor_memory=props.metrics_executor_memory,
        executor_cores=props.metrics_executor_cores,
        sla=None,  # SLA are not supported for mapped tasks
        # Override parallelization for this DAG to run more small jobs at once
        max_active_tis_per_dag=props.max_active_tasks,
        conf=compute_metrics_spark_conf,
        launcher="skein",
        application_args=[
            "--wiki_ids",
            "{{ (task_instance.xcom_pull(task_ids='calculate_effective_dblist') or [])|join(',') }}",  # noqa: E501
            "--content_table",
            props.hive_mediawiki_content_history_v1_table,
            "--inconsistent_rows_table",
            props.hive_wikitext_inconsistent_rows_table,
            "--metrics_table",
            props.hive_metrics_table,
            "--min_timestamp",
            "{{ data_interval_start | to_dt() }}",
            "--max_timestamp",
            "{{ data_interval_end | to_dt() }}",
            "--run_id",
            sanitize_string("{{ run_id }}"),
        ],
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
        spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
        # Temporarily make sure writes to data_quality_metrics are serialized
        # See https://phabricator.wikimedia.org/T386114
        pool=pool("mutex_for_wmf_data_ops_data_quality_metrics"),
        # since metrics computation is currently quite costly (2-4 hours), let's retry at most once for now
        retries=1,
    )

    namespace_sensor >> merge_events_sensor >> reconcile.expand(wiki_id=dblist) >> compute_metrics
