"""
* This job formats and archives mediacounts data.

Note
* This job waits for the mediacounts partition data for the day,
* to be available before running the hql query.
* output of the hql is put in the temporary folder /wmf/tmp/analytics/mediacounts_archive_daily,
* and then archived into /wmf/data/archive/mediacounts/daily
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    archive_directory,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "mediacounts_archive_daily"
var_props = VariableProperties(f"{dag_id}_config")
source_table = "wmf.mediacounts"
format_version = 0


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 3, 1)),
    schedule="@daily",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
    tags=["daily", "from_hive", "to_hdfs", "to_hive", "uses_archiver", "uses_hql", "requires_wmf_mediacounts"],
) as dag:
    sensor = dataset("hive_wmf_mediacounts").get_sensor_for(dag)

    temporary_directory = var_props.get(
        "temporary_directory", f"{hadoop_name_node}/wmf/tmp/analytics/{dag_id}/{{{{data_interval_start|to_ds_nodash}}}}"
    )

    etl1 = SparkSqlOperator(
        task_id="extract_data_into_single_file",
        sql=var_props.get("hql_path", f"{hql_directory}/mediacounts/mediacounts_archive_daily.hql"),
        query_parameters={
            "source_table": source_table,
            "destination_directory": temporary_directory,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        executor_memory="6G",
        spark_datahub_lineage_enabled=True,
    )

    etl2 = HDFSArchiveOperator(
        task_id="move_data_to_archive",
        source_directory=temporary_directory,
        archive_file=var_props.get(
            "archive_file",
            f"{archive_directory}/mediacounts/daily/{{{{data_interval_start.year}}}}/mediacounts."
            + f"{{{{data_interval_start|to_ds}}}}.v{str(format_version).zfill(2)}.tsv.bz2",
        ),
        expected_filename_ending=".bz2",
        check_done=True,
    )

    sensor >> etl1 >> etl2
