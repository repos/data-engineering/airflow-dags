"""
### 2024-12 Backfill pageview hourly
"""

from datetime import datetime

from analytics.config.dag_config import (
    archive_directory,
    create_easy_dag,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "backfill_pageview_hourly_2024_12"

props = DagProperties(
    start_date=datetime(2024, 7, 4, 7, 2),
    end_date=datetime(2024, 12, 5, 0, 0),
    email="",
    aggregate_hql_path=f"{hql_directory}/pageview/hourly/aggregate_pageview_actor_to_pageview_hourly.hql",
    source_table="wmf.pageview_actor",
    destination_table="wmf.pageview_hourly",
    temporary_directory=f"{hadoop_name_node}/wmf/tmp/analytics/{dag_id}/{{{{data_interval_start|to_ds_hour_nodash}}}}",
    archive_hql_path=f"{hql_directory}/pageview/hourly/transform_pageview_to_legacy_format.hql",
    archive_file=(
        f"{archive_directory}/pageview/legacy/hourly/"
        + "{{data_interval_start.add(hours=1).year}}/{{data_interval_start|add_hours(1)|to_ds_month}}/pageviews"
        + "-{{data_interval_start|add_hours(1)|to_ds_nodash}}-{{data_interval_start|add_hours(1)|to_time_nodash}}.gz"
    ),
    driver_memory="4G",
    driver_cores="2",
    executor_memory="8G",
    executor_cores="2",
    record_version="0.0.1",
    coalesce_partitions=4,
    max_active_tasks=8,
    max_active_runs=8,
    max_active_tis_per_dag=8,
)

year = "{{data_interval_start.year}}"
month = "{{data_interval_start.month}}"
day = "{{data_interval_start.day}}"
hour = "{{data_interval_start.hour}}"


with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    end_date=props.end_date,
    schedule="@hourly",
    tags=[
        "hourly",
        "from_hive",
        "to_hive",
        "uses_hql",
        "uses_archiver",
        "requires_wmf_pageview_actor",
        "backfill",
    ],
    max_active_tasks=props.max_active_tasks,
    max_active_runs=props.max_active_runs,
) as dag:
    aggregate = SparkSqlOperator(
        task_id="backfill_aggregate_pageview_actor_to_pageview_hourly_2024_12",
        sql=props.aggregate_hql_path,
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "record_version": props.record_version,
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
            "coalesce_partitions": props.coalesce_partitions,
        },
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )

    prepare_archive = SparkSqlOperator(
        task_id="backfill_transform_pageview_2024_12",
        sql=props.archive_hql_path,
        query_parameters={
            "source_table": props.destination_table,
            "destination_directory": props.temporary_directory,
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
            "coalesce_partitions": 1,
        },
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )

    archive = HDFSArchiveOperator(
        task_id="backfill_move_data_to_archive_2024_12",
        source_directory=props.temporary_directory,
        archive_file=props.archive_file,
        expected_filename_ending=".gz",
        check_done=True,
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )

    aggregate >> prepare_archive >> archive
