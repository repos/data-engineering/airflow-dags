"""
### 2024-12 Backfill webrequest actor label hourly
"""

from datetime import datetime

from analytics.config.dag_config import create_easy_dag, hql_directory
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "backfill_webrequest_actor_label_hourly_2024_12"

props = DagProperties(
    # Same as webrequest_actor_metrics_rollup_hourly
    start_date=datetime(2024, 7, 4, 7, 2),
    end_date=datetime(2024, 12, 5, 0, 0),
    email="",
    hql_path=f"{hql_directory}/webrequest/actor/compute_webrequest_actor_label_hourly.hql",
    source_table="wmf.webrequest_actor_metrics_rollup_hourly",
    destination_table="wmf.webrequest_actor_label_hourly",
    version="0.2",
    max_executors=128,
    max_active_tasks=8,
    max_active_runs=8,
    max_active_tis_per_dag=8,
    catchup=True,
    coalesce_partitions=32,
    executor_memory="8G",
    executor_cores=2,
    memory_overhead=4096,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    end_date=props.end_date,
    schedule="@hourly",
    tags=[
        "hourly",
        "uses_hql",
        "from_hive",
        "to_hive",
        "backfill",
        "requires_wmf_webrequest_actor_metrics_rollup_hourly",
    ],
    max_active_tasks=props.max_active_tasks,
    max_active_runs=props.max_active_runs,
    email=props.email,
    catchup=props.catchup,
) as dag:
    SparkSqlOperator(
        task_id="backfill_compute_webrequest_actor_label_2024_12",
        sql=props.hql_path,
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "version": props.version,
            "coalesce_partitions": props.coalesce_partitions,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
        },
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.executor.memoryOverhead": props.memory_overhead,
        },
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )
