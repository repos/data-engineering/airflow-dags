"""
### 2024-12 Backfill pageview actor metrics hourly roll-up
"""

from datetime import datetime

from analytics.config.dag_config import artifact, create_easy_dag, hql_directory
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "backfill_pageview_actor_hourly_2024_12"

props = DagProperties(
    # Same as webrequest_actor_label_hourly
    start_date=datetime(2024, 7, 4, 7, 2),
    end_date=datetime(2024, 12, 5, 0, 0),
    email="",
    hql_path=f"{hql_directory}/pageview/actor/backfill_pageview_actor_2024_12.hql",
    refinery_hive_jar=artifact("refinery-hive-0.2.54-shaded.jar"),
    source_table="wmf.pageview_actor",
    actor_label_table="wmf.webrequest_actor_label_hourly",
    destination_table="wmf.backfill_pageview_actor_2024_12",
    coalesce_partitions=32,
    driver_memory="4G",
    executor_memory="8G",
    executor_cores=2,
    max_executors=128,
    memory_overhead="4G",
    catchup=True,
    max_active_tasks=8,
    max_active_runs=8,
    max_active_tis_per_dag=8,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    end_date=props.end_date,
    schedule="@hourly",
    tags=[
        "hourly",
        "from_hive",
        "to_hive",
        "uses_hql",
        "requires_wmf_pageview_actor_hourly",
        "requires_wmf_webrequest_actor_label_hourly",
        "backfill",
    ],
    max_active_tasks=props.max_active_tasks,
    max_active_runs=props.max_active_runs,
    email=props.email,
    catchup=props.catchup,
) as dag:
    SparkSqlOperator(
        task_id="backfill_compute_pageview_actor_hourly_2024_12",
        sql=props.hql_path,
        query_parameters={
            "refinery_hive_jar_path": props.refinery_hive_jar,
            "source_table": props.source_table,
            "actor_label_table": props.actor_label_table,
            "destination_table": props.destination_table,
            "coalesce_partitions": props.coalesce_partitions,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
        },
        driver_memory=props.driver_memory,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.executor.memoryOverhead": props.memory_overhead,
        },
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )
