"""
### 2024-12 Backfill webrequest actor metrics hourly
"""

from datetime import datetime

from analytics.config.dag_config import artifact, create_easy_dag, hql_directory
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "backfill_webrequest_actor_metrics_hourly_2024_12"

props = DagProperties(
    start_date=datetime(2024, 7, 3, 7, 2),
    end_date=datetime(2024, 12, 5, 0, 0),
    email="",
    hql_path=f"{hql_directory}/webrequest/actor/backfill_webrequest_actor_metrics_hourly_2024_12.hql",
    refinery_hive_jar=artifact("refinery-hive-0.2.54-shaded.jar"),
    source_table="wmf.pageview_actor",
    destination_table="wmf.webrequest_actor_metrics_hourly",
    version="0.2",
    coalesce_partitions=4,
    executor_memory="8G",
    executor_cores=2,
    max_executors=128,
    catchup=True,
    max_active_tasks=8,
    max_active_runs=8,
    max_active_tis_per_dag=8,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    end_date=props.end_date,
    schedule="@hourly",
    tags=["hourly", "uses_hql", "to_hive", "from_hive", "requires_wmf_pageview_actor", "backfill"],
    max_active_tasks=props.max_active_tasks,
    max_active_runs=props.max_active_runs,
    email=props.email,
    catchup=props.catchup,
) as dag:
    SparkSqlOperator(
        task_id="backfill_compute_webrequest_actor_metrics_2024_12",
        sql=props.hql_path,
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "refinery_hive_jar_path": props.refinery_hive_jar,
            "version": props.version,
            "coalesce_partitions": props.coalesce_partitions,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
        },
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
        },
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )
