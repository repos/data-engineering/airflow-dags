"""
### 2024-12 Backfill cassandra load pageview per article daily
"""

from datetime import datetime

from analytics.config.dag_config import create_easy_cassandra_loading_dag, hql_directory
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

props = DagProperties(
    # Same as pageview_hourly
    start_date=datetime(2024, 7, 4, 7, 2),
    end_date=datetime(2024, 12, 5, 0, 0),
    email="",
    hql_path=f"{hql_directory}/cassandra/daily/load_cassandra_pageview_per_article_daily.hql",
    source_table="wmf.pageview_hourly",
    hive_disallowed_cassandra_articles_table="wmf.disallowed_cassandra_articles",
    # Setting coalesce_partitions to 6 is important
    # as it defines how many parallel loaders we use for cassandra.
    # We currently use 6 as there are 6 cassandra hosts
    coalesce_partitions=6,
    executor_memory="8G",
    executor_cores=2,
    driver_memory="8G",
    driver_cores=2,
    max_executors=64,
    memory_overhead="2G",
    destination_table="aqs.local_group_default_T_pageviews_per_article_flat.data",
    catchup=True,
    max_active_tasks=2,
    max_active_runs=2,
    max_active_tis_per_dag=2,
)


with create_easy_cassandra_loading_dag(
    dag_id="backfill_cassandra_load_pageview_per_article_daily_2024_12",
    doc_md=__doc__,
    start_date=props.start_date,
    end_date=props.end_date,
    schedule="@daily",
    tags=[
        "daily",
        "from_hive",
        "to_cassandra",
        "uses_hql",
        "requires_wmf_disallowed_cassandra_articles",
        "requires_wmf_pageview_hourly",
        "backfill",
    ],
    max_active_tasks=props.max_active_tasks,
    max_active_runs=props.max_active_runs,
    catchup=props.catchup,
) as daily_dag:
    SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.hql_path,
        query_parameters={
            "source_table": props.source_table,
            "disallowed_cassandra_articles_table": props.hive_disallowed_cassandra_articles_table,
            "destination_table": props.destination_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "coalesce_partitions": props.coalesce_partitions,
        },
        # Spark resources configuration needs to be tweaked for this job
        # as it processes relatively big data
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        conf={
            # Hack: We don't want to override default_args["conf"] already set,
            # we just want to add to them, so we reset them in our override.
            **daily_dag.default_args["conf"],
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.executor.memoryOverhead": props.memory_overhead,
        },
        max_active_tis_per_dag=props.max_active_tis_per_dag,
    )
