"""
### Creates and archives a monthly dump of daily pageview data

Ouput
* Output is saved to a temporary location
* and then archived into /wmf/data/archive/pageview/complete/monthly
* eg: /wmf/data/archive/pageview/complete/monthly/2023/2023-01/pageviews-202301-user.bz2

Variables for Testing
{
    "dump_month_of_daily_pageviews_config":
    {
        "start_date": "2023-01-20",
        "hql_path": "/home/milimetric/refinery/hql/pageview/monthly/make_daily_dumps.hql",
        "temp_dir": "hdfs://analytics-hadoop/tmp/dump_test_month",
        "archive_file": "hdfs://analytics-hadoop/tmp/dump_test_archive_month",
        "default_args": {
            "owner": "analytics-privatedata",
            "email": "milimetric@wikimedia.org"
        }
    }
}
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    archive_directory,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "dump_month_of_daily_pageviews"
var_props = VariableProperties(f"{dag_id}_config")
agent_types = ["user", "automated"]
source_table = "wmf.pageview_hourly"

start_date = var_props.get_datetime("start_date", datetime(2023, 2, 28))
year = "{{data_interval_start.year}}"
month = "{{data_interval_start.month}}"
day = "{{data_interval_start.day}}"
make_dumps_hql = var_props.get("hql_path", f"{hql_directory}/pageview/monthly/make_daily_dumps.hql")
temporary_directory = var_props.get(
    "temp_dir", f"{hadoop_name_node}/wmf/tmp/analytics/{dag_id}/{{{{data_interval_start|to_ds_hour_nodash}}}}"
)
archive_file_prefix = var_props.get(
    "archive_file",
    f"{archive_directory}/pageview/complete/monthly/"
    "{{data_interval_start.year}}/{{data_interval_start|to_ds_month}}/"
    "pageviews-{{data_interval_start|to_ds_month_nodash}}-",
)


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=start_date,
    schedule="@monthly",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(days=3),
        },
    ),
    user_defined_filters=filters,
    tags=["monthly", "from_hive", "to_hdfs", "uses_archiver", "uses_hql", "requires_wmf_pageview_hourly"],
) as dag:
    sense_pageview_hourly = dataset("hive_wmf_pageview_hourly").get_sensor_for(dag)

    # make dumps for agent types in parallel
    # (if we decide to re-enable spider dumps, we can reconsider)
    for agent_type in agent_types:
        temp_dir = temporary_directory + "/" + agent_type

        make_dumps = SparkSqlOperator(
            task_id="make_dumps_for_" + agent_type,
            sql=make_dumps_hql,
            query_parameters={
                "agent_type": agent_type,
                "source_table": source_table,
                "destination_directory": temp_dir,
                "year": year,
                "month": month,
            },
            executor_memory="12G",
            executor_cores=4,
            conf={
                "spark.dynamicAllocation.maxExecutors": 64,
                "spark.executor.memoryOverhead": 8192,
            },
            spark_datahub_lineage_enabled=True,
        )

        archive_dumps = HDFSArchiveOperator(
            task_id="archive_month_of_pageviews_" + agent_type,
            source_directory=temp_dir,
            archive_file=archive_file_prefix + agent_type + ".bz2",
            expected_filename_ending=".bz2",
            check_done=True,
        )

        sense_pageview_hourly >> make_dumps >> archive_dumps
