"""
### Aggregates Pageview and Archives to Legacy Format.
* This job is responsible for aggregating pageview from pageview_actor,
* and then transforming and archiving this data into legacy format.

Output
* Output is appended into (year, month, day, hour) partitions in /wmf/data/wmf/pageview/hourly,
* and then archived into /wmf/data/archive/pageview/legacy/hourly

Variables for Testing
{
    "pageview_hourly_config":
    {
        "start_date": "2023-02-20",
        "destination_table": "milimetric.pageview_hourly",
        "aggregate_hql_path":
            "/home/milimetric/refinery/hql/pageview/hourly/aggregate_pageview_actor_to_pageview_hourly.hql",
        "temp_dir": "hdfs://analytics-hadoop/tmp/pageview_hourly_test",
        "archive_hql_path": "/home/milimetric/refinery/hql/pageview/hourly/transform_pageview_to_legacy_format.hql",
        "archive_file": "hdfs://analytics-hadoop/tmp/legacy_archive_test.gz",
        "default_args": {
            "owner": "analytics-privatedata",
            "email": "milimetric@wikimedia.org"
        }
    }
}
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    archive_directory,
    dataset,
    default_args,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "pageview_hourly"
var_props = VariableProperties(f"{dag_id}_config")
source_table = "wmf.pageview_actor"
destination_table = var_props.get("destination_table", "wmf.pageview_hourly")
aggregate_hql_path = var_props.get(
    "aggregate_hql_path", f"{hql_directory}/pageview/hourly/aggregate_pageview_actor_to_pageview_hourly.hql"
)
temporary_directory = var_props.get(
    "temp_dir", f"{hadoop_name_node}/wmf/tmp/analytics/{dag_id}/{{{{data_interval_start|to_ds_hour_nodash}}}}"
)
archive_hql_path = var_props.get(
    "archive_hql_path", f"{hql_directory}/pageview/hourly/transform_pageview_to_legacy_format.hql"
)
archive_file = var_props.get(
    "archive_file",
    f"{archive_directory}/pageview/legacy/hourly/"
    + "{{data_interval_start.add(hours=1).year}}/{{data_interval_start|add_hours(1)|to_ds_month}}/pageviews"
    + "-{{data_interval_start|add_hours(1)|to_ds_nodash}}-{{data_interval_start|add_hours(1)|to_time_nodash}}.gz",
)

driver_memory = var_props.get("driver_memory", "4G")
driver_cores = var_props.get("driver_cores", "2")
executor_memory = var_props.get("executor_memory", "8G")
executor_cores = var_props.get("executor_cores", "2")

start_date = var_props.get_datetime("start_date", datetime(2023, 2, 28))
year = "{{data_interval_start.year}}"
month = "{{data_interval_start.month}}"
day = "{{data_interval_start.day}}"
hour = "{{data_interval_start.hour}}"


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=start_date,
    schedule="@hourly",
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=6),
        },
    ),
    user_defined_filters=filters,
    tags=["hourly", "from_hive", "to_hdfs", "to_hive", "uses_archiver", "uses_hql", "requires_wmf_pageview_actor"],
) as dag:
    sense_pageview_actor = dataset("hive_wmf_pageview_actor").get_sensor_for(dag)

    aggregate = SparkSqlOperator(
        task_id="aggregate_pageview_actor_to_pageview_hourly",
        sql=aggregate_hql_path,
        query_parameters={
            "source_table": source_table,
            "destination_table": destination_table,
            "record_version": var_props.get("record_version", "0.0.1"),
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
            "coalesce_partitions": 4,
        },
        driver_memory=driver_memory,
        driver_cores=driver_cores,
        executor_memory=executor_memory,
        executor_cores=executor_cores,
        spark_datahub_lineage_enabled=True,
    )

    prepare_archive = SparkSqlOperator(
        task_id="transform_pageview",
        sql=archive_hql_path,
        query_parameters={
            "source_table": destination_table,
            "destination_directory": temporary_directory,
            "year": year,
            "month": month,
            "day": day,
            "hour": hour,
            "coalesce_partitions": 1,
        },
        driver_memory=driver_memory,
        driver_cores=driver_cores,
        executor_memory=executor_memory,
        executor_cores=executor_cores,
        spark_datahub_lineage_enabled=True,
    )

    archive = HDFSArchiveOperator(
        task_id="move_data_to_archive",
        source_directory=temporary_directory,
        archive_file=archive_file,
        expected_filename_ending=".gz",
        check_done=True,
    )

    sense_pageview_actor >> aggregate >> prepare_archive >> archive
