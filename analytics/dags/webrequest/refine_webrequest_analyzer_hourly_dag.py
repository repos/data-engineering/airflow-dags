import os
from datetime import datetime
from typing import List

from airflow.operators.python import BranchPythonOperator

from analytics.config.dag_config import (
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hdfs_temp_directory,
    pool,
)
from analytics.dags.anomaly_detection.anomaly_detection_dag_factory import should_alert
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.email import HdfsEmailOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator

"""
Replace invalid characters from input_string.
A helper function used to sanitize Airflow's run_id
identifier to a filename compatible with HDFS.
"""


def sanitize_string(input_string: str, invalid_chars: str = ":+", new: str = "_") -> str:
    sanitized_string = input_string
    for char in invalid_chars:
        sanitized_string = sanitized_string.replace(char, new)

    return sanitized_string


dag_id = "webrequest_analyzer"

# If empty no email will be sent.
# The alert detection and shipping code path
# will not be evaluated.
alerts_recipients: List[str] = [
    # TODO: Add email alias for webrequest alerting.
]

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2023, 12, 1),
    application=artifact("refinery-job-0.2.45-shaded.jar"),
    table="wmf.webrequest",
    metrics_table="wmf_data_ops.data_quality_metrics",  # TODO: when more DQ jos are added,
    # we should refactor this to a global config
    alerts_table="wmf_data_ops.data_quality_alerts",  # TODO: when more DQ jos are added,
    # we should refactor this to a global config
    year="{{ data_interval_start.year }}",
    month="{{ data_interval_start.month }}",
    day="{{ data_interval_start.day }}",
    hour="{{ data_interval_start.hour }}",
    refined_webrequest_location_base="hdfs:///wmf/data/wmf/webrequest",
    webrequest_source="text",
    alerts_recipients=",".join(alerts_recipients),
    alerts_output_path=os.path.join(hdfs_temp_directory, "webrequest_alerts", sanitize_string("{{ run_id }}")),
    hadoop_name_node=hadoop_name_node,
)


with create_easy_dag(dag_id=dag_id, start_date=props.start_date, schedule="@hourly") as dag:
    wait_for_webrequest = dataset("hive_wmf_webrequest_all_sources").get_sensor_for(dag)

    compute_metrics = SparkSubmitOperator(
        task_id="webrequest_metrics",
        application=props.application,
        java_class="org.wikimedia.analytics.refinery.job.dataquality.WebrequestMetrics",
        application_args={
            "--table": props.table,
            "--metrics_table": props.metrics_table,
            "--alerts_table": props.alerts_table,
            "--alerts_output_path": props.alerts_output_path,
            "--partition_map": f"year:{props.year},month:{props.month},day:{props.day},hour:{props.hour}",
            "--run_id": sanitize_string("{{ run_id }}"),
        },
        spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
        # Temporarily make sure writes to data_quality_metrics are serialized.
        # See https://phabricator.wikimedia.org/T386114
        pool=pool("mutex_for_wmf_data_ops_data_quality_metrics"),
    )

    # TODO(2024-01-18): email alerts are generated but not delivered for now.
    # To be enabled after a test period. To enable email dispatch,
    # add recipients to the alerts_recipients property.
    if props.alerts_recipients:
        # Branch operator to determine whether to send alert or not.
        has_alerts = BranchPythonOperator(
            task_id="should_alert",
            python_callable=should_alert,
            op_kwargs={
                "hadoop_name_node": props.hadoop_name_node,
                "anomaly_path": props.alerts_output_path,
            },
        )

        send_alerts = HdfsEmailOperator(
            task_id="send_email",
            to=props.alerts_recipients,
            subject="Webrequest data quality degradation - Airflow Analytics {{ dag.dag_id }} {{ ds }}",
            html_content=("Report content:<br/>"),
            embedded_file=props.alerts_output_path,
            hadoop_name_node=props.hadoop_name_node,
        )
        # To be enabled after a test period
        wait_for_webrequest >> compute_metrics >> has_alerts >> send_alerts
    else:
        wait_for_webrequest >> compute_metrics
