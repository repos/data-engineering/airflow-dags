"""
Generates 4 DAGs calculating unique-devices metrics per-domain/per-project-family
both daily and monthly.
The metrics are stored in both a Hive and a Iceberg tables and also archived as CSV files.
Note: To prevent codeduplication this code uses indexed-string parameters resolved in the loops
      creating the dags. This can be tricky to follow :S

"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    archive_directory,
    create_easy_dag,
    dataset,
    hdfs_temp_directory,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Time granularities to compute
    time_granularities=["daily", "monthly"],
    # Datasets to compute
    datasets=["per_domain", "per_project_family"],
    # start dates
    daily_start_date=datetime(2023, 10, 17),
    monthly_start_date=datetime(2023, 10, 1),
    # input tables
    pageview_actor_table="wmf.pageview_actor",
    pageview_actor_sensor_dataset="hive_wmf_pageview_actor",
    countries_table="canonical_data.countries",
    # String patterns configurations to be used for every dataset to compute
    # Tables
    unique_devices_hive_table="wmf.unique_devices_{dataset_name}_{time_granularity}",
    unique_devices_iceberg_table="wmf_readership.unique_devices_{dataset_name}_{time_granularity}",
    # HQL
    hql_hive=(hql_directory + "/unique_devices/{dataset_name}/unique_devices_{dataset_name}_{time_granularity}.hql"),
    hql_iceberg=(
        hql_directory + "/unique_devices/{dataset_name}/unique_devices_{dataset_name}_{time_granularity}_iceberg.hql"
    ),
    hql_dumps=(
        hql_directory + "/unique_devices/{dataset_name}/unique_devices_{dataset_name}_{time_granularity}_to_archive.hql"
    ),
    # Temporary dir
    temp_base_dir=hdfs_temp_directory + "/unique_devices/{dataset_name}/{time_granularity}",
    # Dumps destination
    dumps_base_dest=archive_directory + "/unique_devices/{dataset_name}",
    # Main spark job config
    # Note
    main_spark_job_config={
        "driver_cores": 2,
        "driver_memory": "4G",
        "executor_cores": 2,
        "executor_memory": "8G",
        "conf": {
            "spark.dynamicAllocation.maxExecutors": 64,
            "spark.yarn.executor.memoryOverhead": 2048,
            # TODO: Remove when hive job is removed in favor of iceberg
            "spark.sql.shuffle.partitions": 512,
        },
    },
    # SLAs and alerts email.
    daily_sla=timedelta(hours=6),
    monthly_sla=timedelta(days=1),
    alerts_email=alerts_email,
)


# Create one DAG per time-granularity and dataset type
for time_granularity in props.time_granularities:
    for dataset_name in props.datasets:
        with create_easy_dag(
            dag_id=f"unique_devices_{dataset_name}_{time_granularity}",
            doc_md=(
                f"""Calculates unique devices metrics {dataset_name} {time_granularity}.
                The metrics are stored in both a Hive and an Iceberg table, and also archived as CSV files."""
            ),
            start_date=getattr(props, f"{time_granularity}_start_date"),
            schedule=f"@{time_granularity}",
            tags=[
                time_granularity,
                "from_hive",
                "to_hdfs",
                "to_hive",
                "to_iceberg",
                "uses_archiver",
                "uses_hql",
                "requires_wmf_pageview_actor",
            ],
            sla=getattr(props, f"{time_granularity}_sla"),
            email=props.alerts_email,
        ) as dag:
            # Sensor to wait for the pageview actor partitions.
            sensor = dataset(props.pageview_actor_sensor_dataset).get_sensor_for(dag)

            # SQL operator for the computation of the data.
            compute = SparkSqlOperator(
                task_id=f"compute_{dataset_name}_{time_granularity}",
                sql=props.hql_hive.format(dataset_name=dataset_name, time_granularity=time_granularity),
                query_parameters={
                    "source_table": props.pageview_actor_table,
                    "destination_table": props.unique_devices_hive_table.format(
                        dataset_name=dataset_name, time_granularity=time_granularity
                    ),
                    "countries_table": props.countries_table,
                    "year": "{{data_interval_start.year}}",
                    "month": "{{data_interval_start.month}}",
                    # NOTE: For monthly queries, the day parameter is passed but not used
                    "day": "{{data_interval_start.day}}",
                    "coalesce_partitions": 1,
                },
                **props.main_spark_job_config,
                spark_datahub_lineage_enabled=True,
            )

            # SQL operator for the computation of the Iceberg data.
            compute_iceberg = SparkSqlOperator(
                task_id=f"compute_{dataset_name}_{time_granularity}_iceberg",
                sql=props.hql_iceberg.format(dataset_name=dataset_name, time_granularity=time_granularity),
                query_parameters={
                    "unique_devices_source_table": props.unique_devices_hive_table.format(
                        dataset_name=dataset_name, time_granularity=time_granularity
                    ),
                    "unique_devices_destination_table": props.unique_devices_iceberg_table.format(
                        dataset_name=dataset_name, time_granularity=time_granularity
                    ),
                    "year": "{{data_interval_start.year}}",
                    "month": "{{data_interval_start.month}}",
                    # NOTE: For monthly queries, the day parameter is passed but not used
                    "day": "{{data_interval_start.day}}",
                    "coalesce_partitions": 1,
                },
                spark_datahub_lineage_enabled=False,  # Iceberg tables not supported
                # Note: Currently no computation is done when loading Iceberg tables,
                # we copy from already loaded hive table to prevent redoing the same
                # expensive computation twice. Therefore spark default resources work
                # here for now. We'll need to use the props.main_spark_job_config when
                # we remove the Hive tables and do the computation in the Iceberg queries.
            )

            # SQL operator for the dump of the metrics.
            temp_directory = (
                props.temp_base_dir.format(dataset_name=dataset_name, time_granularity=time_granularity) + "/{{ds}}"
            )

            dump = SparkSqlOperator(
                task_id=f"dump_{dataset_name}_{time_granularity}",
                sql=props.hql_dumps.format(dataset_name=dataset_name, time_granularity=time_granularity),
                query_parameters={
                    "source_table": props.unique_devices_hive_table.format(
                        dataset_name=dataset_name, time_granularity=time_granularity
                    ),
                    "destination_directory": temp_directory,
                    "year": "{{data_interval_start.year}}",
                    "month": "{{data_interval_start.month}}",
                    # NOTE: For monthly queries, the day parameter is passed but not used
                    "day": "{{data_interval_start.day}}",
                },
                spark_datahub_lineage_enabled=True,
            )

            # Archive operator to cleanly move the dump to its final destination.
            archive_file_path = (
                props.dumps_base_dest.format(dataset_name=dataset_name)
                + "/{{data_interval_start.year}}/{{data_interval_start|to_ds_month}}/"
                + f"unique_devices_{dataset_name}_{time_granularity}-{{{{ds}}}}.gz"
            )

            archive = HDFSArchiveOperator(
                task_id=f"move_{dataset_name}_{time_granularity}_data_to_archive",
                source_directory=temp_directory,
                check_done=True,
                archive_file=archive_file_path,
                archive_parent_umask="022",
                archive_perms="644",
            )

            sensor >> compute >> compute_iceberg >> dump >> archive
