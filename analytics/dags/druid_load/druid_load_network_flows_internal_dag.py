"""
Loads event.network_flows_internal from Hive to Druid.

The loading is done with 2 DAGs: an hourly one and a daily.

The hourly DAG loads data to Druid as soon as it's available in Hive.

The daily DAG waits for a full day of data to be available in Hive,
plus waits for the hourly segments to be loaded in Druid, then reloads
the data for all the day to Druid as a single daily segment, which is
more efficient than hourly segments. This overrides any hourly segments.
The daily dag should be used for back-filling and re-runs. To do so,
set the property wait_for_druid_segments to False in the UI Variable.
"""

from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import (
    artifact,
    dataset,
    default_args,
    druid_default_conf,
)
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = "druid_load_network_flows_internal"
var_props = VariableProperties(f"{dag_id}_config")

# Common configuration for both the hourly and the daily DAGs.
common_hive_to_druid_config = {
    "database": "event",
    "table": "network_flows_internal",
    "druid_datasource": var_props.get("druid_datasource", "network_flows_internal"),
    "timestamp_column": "stamp_inserted",
    "timestamp_format": "auto",
    "query_granularity": "minute",
    "hadoop_queue": var_props.get("hadoop_queue", "production"),
    "hive_to_druid_jar": var_props.get("hive_to_druid_jar", artifact("refinery-job-0.2.11-shaded.jar")),
    "temp_directory": var_props.get("temp_directory", None),  # Override just for testing.
    "dimensions": [
        "ip_dst",
        "ip_proto",
        "ip_src",
        "port_dst",
        "port_src",
        "peer_ip_src",
        "ip_version",
        "region",
    ],
    "metrics": [
        "bytes",
        "packets",
    ],
}

default_tags = ["from_hive", "to_druid", "uses_spark", "requires_event_network_flows_internal"]

# Hourly DAG.
with DAG(
    dag_id=f"{dag_id}_hourly",
    doc_md="Loads network_flows_internal from Hive to Druid hourly.",
    start_date=var_props.get_datetime("hourly_start_date", datetime(2023, 3, 1, 0)),
    schedule="@hourly",
    tags=["hourly"] + default_tags,
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,  # type: ignore
        "sla": timedelta(hours=6),
    },
) as hourly_dag:
    sensor = dataset("hive_event_network_flows_internal").get_sensor_for(hourly_dag)

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_hours(1) | to_ds_hour}}",
        segment_granularity="hour",
        reduce_memory=8192,
        num_shards=1,
        **common_hive_to_druid_config,
    )

    sensor >> loader


# Daily DAG.
with DAG(
    dag_id=f"{dag_id}_daily",
    doc_md="Loads network_flows_internal from Hive to Druid daily.",
    start_date=var_props.get_datetime("daily_start_date", datetime(2023, 3, 1)),
    schedule="@daily",
    tags=["daily"] + default_tags,
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,  # type: ignore
        "sla": timedelta(hours=12),
    },
) as daily_dag:
    hive_sensor = dataset("hive_event_network_flows_internal").get_sensor_for(daily_dag)

    # Usually this DAG waits for Druid hourly segments to be present.
    # This way we avoid race conditions of the 2 DAGS (hourly and daily)
    # trying to load into the same datasource at the same time.
    # But this property can be overriden, whenver we want to use this
    # dag to do re-runs or back-filling historical data.
    wait_for_druid_segments = var_props.get("wait_for_druid_segments", True)
    if wait_for_druid_segments:
        druid_sensor = dataset("druid_wmf_network_flows_internal_hourly").get_sensor_for(daily_dag)

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_days(1) | to_ds_hour}}",
        segment_granularity="day",
        reduce_memory=16384,
        num_shards=1,
        executor_cores=4,
        executor_memory="8G",
        **common_hive_to_druid_config,
    )

    hive_sensor >> loader
    if wait_for_druid_segments:
        druid_sensor >> loader
