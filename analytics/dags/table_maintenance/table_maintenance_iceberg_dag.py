"""
In the DAGs defined below, we launch daily, weekly, and monthly maintenance tasks for Iceberg tables.

wmf_airflow_common.dataset.IcebergDataset.DEFAULT_MAINTENANCE defines the default Spark CALL procedures that will
be scheduled for all Iceberg tables. These defaults can be overridden via config file analytics/config/datasets.yaml.

Example configuration from analytics/config/datasets.yaml:
```
...
iceberg_wmf_dumps_wikitext_raw:
  datastore: iceberg
  table_name: wmf_dumps.wikitext_raw_rc2
  maintenance:
    schedule: "@daily"
    rewrite_data_files:
      enabled: True
      strategy: "sort"
      options:
        "max-concurrent-file-group-rewrites": "10"
        "partial-progress.enabled": "true"
      spark_kwargs:
        driver_memory: "32g"
        driver_cores: "4"
        executor_memory: "20g"
        executor_cores: "2"
      spark_conf:
        "spark.dynamicAllocation.maxExecutors": "64"

iceberg_wmf_dumps_wikitext_inconsistent_rows:
  datastore: iceberg
  table_name: wmf_dumps.wikitext_inconsistent_rows_rc1
  maintenance:
    data_delete:
      enabled: True
      where: "computation_dt <= TIMESTAMP '{{ data_interval_end | subtract_days(30) | to_dt() }}'"
...
```

The fact that 'iceberg_wmf_dumps_wikitext_raw' is defined as an iceberg dataset above resolves into picking up the
following default Spark procedure CALLs:

 * `spark_catalog.system.remove_orphan_files()`
    -> This removes any data files that are not tied to any Iceberg snapshot.
       These orphan files can occur when writes fail and leave files behind.
       More info at: https://iceberg.apache.org/docs/1.6.1/spark-procedures/#remove_orphan_files
 * `spark_catalog.system.expire_snapshots()`
    -> Iceberg keeps an immutable copy of the state of the table for each commit, so that we can 'go back in time'
       This procedure removes older snapshots and thus older files that are not needed anymore.
       More info at: https://iceberg.apache.org/docs/1.6.1/spark-procedures/#expire_snapshots
 * `spark_catalog.system.rewrite_manifests()`
    -> Over time, Iceberg tables may accumulate many manifest files that can slow down query planning.
       This procedure rewrites them so that reading is more efficient.
       More info at https://iceberg.apache.org/docs/1.6.1/spark-procedures/#rewrite_manifests

Similarly, data rewrites are supported, but disabled by default, via:
 * `spark_catalog.system.rewrite_data_files()`
    -> Iceberg tracks each data file in a table. More data files leads to more metadata stored in manifest files, and
       small data files causes an unnecessary amount of metadata and less efficient queries from file open costs.
       Additionally, if your table creates delete files because you are leveraging 'merge-on-read', you want rewrites.
       More info at: https://iceberg.apache.org/docs/1.6.1/spark-procedures/#rewrite_data_files
    -> Note how you can define Spark resources and extra configuration via `rewrite_data_files()`'s `spark_kwargs` and
       `spark_conf`.
    -> Although not shown in example above, you can also define a WHERE clause, with jinja templates.

Automated data deletion is also possible, but disabled by default. The example
'iceberg_wmf_dumps_wikitext_inconsistent_rows' above shows how to enable data deletion and how to define the
WHERE clause. You can include jinja templates.
"""

from datetime import datetime, timedelta
from typing import cast

from airflow.models.baseoperator import chain
from airflow.utils.task_group import TaskGroup

from analytics.config.dag_config import create_easy_dag, dataset_registry
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.dataset import IcebergDataset

props = DagProperties(
    start_date=datetime(2024, 8, 15),
    dag_sla=timedelta(days=5),
)

tags = [
    "table_maintenance",
    "uses_hql",
    "monthly",
]

for schedule in IcebergDataset.SUPPORTED_MAINTENANCE_SCHEDULES:
    with create_easy_dag(
        dag_id=f"table_maintenance_iceberg_{schedule.replace('@', '')}",
        doc_md=__doc__,
        start_date=props.start_date,
        schedule=schedule,
        tags=tags,
        sla=props.dag_sla,
        catchup=False,
    ) as dag:
        common_spark_conf = {
            "spark.sql.autoBroadcastJoinThreshold": "-1",  # avoid OOMs. See T338065#9192096.
        }

        idatasets = (
            cast(IcebergDataset, dataset)
            for dataset in dataset_registry.get_all_datasets()
            if isinstance(dataset, IcebergDataset) and dataset.maintenance["schedule"] == schedule
        )

        for idataset in idatasets:
            task_group_id = idataset.table_name.replace(".", "__")
            with TaskGroup(group_id=task_group_id):
                maintenance_tasks = idataset.get_maintenance_tasks(dag, common_spark_conf)
                chain(*maintenance_tasks)
