# TODO: this DAG is a work in progress to support T354694
# See refine_webrequest_frontend_hourly_dag_factory.py for details.
from datetime import datetime

from analytics.dags.webrequest_frontend.refine_webrequest_frontend_hourly_dag_factory import (
    generate_dag,
)

for webrequest_source in ["text", "upload"]:
    # https://github.com/apache/airflow/blob/2.5.1/airflow/utils/file.py#L358
    generate_dag(  # This generates an Airflow DAG
        dag_id=f"refine_webrequest_frontend_hourly_{webrequest_source}",
        webrequest_source=webrequest_source,
        default_start_date=datetime(2024, 11, 26, 0),
        tags=[
            "staging",
            "work_in_progress",
            "hourly",
            "uses_hql",
            "uses_email",
            "from_hive",
            "to_hive",
            "requires_wmf_raw_webrequest_frontend",
        ],
    )
