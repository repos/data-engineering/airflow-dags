from typing import Tuple, Union, Optional

from airflow.models.baseoperator import BaseOperator
from airflow.operators.python import ShortCircuitOperator

from search.config.dag_config import discolytics_conda_env_tgz
from search.shared.hdfs_cli import HdfsCliHook
from search.shared.swift_upload import swift_upload
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

var_props = VariableProperties('transfer_to_es_conf')

base_output_path = var_props.get('base_output_path', 'hdfs://analytics-hadoop/wmf/data/discovery/transfer_to_es')
table_canonical_wikis = var_props.get('table_canonical_wikis', 'canonical_data.wikis')
table_namespace_map = var_props.get('table_namespace_map', 'discovery.cirrus_namespace_index_map')
swift_container = var_props.get('swift_container', 'search_updates')


def continue_if_nonexistent(path: str) -> bool:
    """
    Returns True if the path exists, otherwise returns False.

    Intended to be passed as python_callable to ShortCircuitOperator.

    Parameters
    ----------
    path
        An HDFS path to be checked for existence
    """
    return not HdfsCliHook.exists(path)


def convert_and_upload(convert_config: str, rel_path: str, event_stream: Union[bool, str] = True,
                       partition_spec_hint: Optional[str] = None) -> Tuple[BaseOperator, BaseOperator, BaseOperator]:
    """Create operators to apply convert_to_esbulk and ship to prod

    Returned operators are
    * convert,
    * probe (ShortCircuitOperator testing if the prio convert operator implies skipping), and
    * upload.

    Returned operators are not connected,
    caller is responsible to set upstream and downstream as appropriate for their use case.

    Parameters
    ----------
    convert_config
        Named configuration of convert_to_esbulk to use
    rel_path
        Used in the output path for intermediate data. Should be unique
        per use case.
    event_stream
        Set this to False to disable sending of events. Otherwise,
        this will be used as the value of meta.stream in the produced
        swift/upload/complete event. If not set, this will default to
        swift.<container>.upload-complete.
    partition_spec_hint
        hint for determining the partition spec (required by some config)
    """
    path_out = '/'.join([
        base_output_path,
        'date={{ ds_nodash }}',
        rel_path
    ])

    args = [
        '--config', convert_config,
        '--namespace-map-table', table_namespace_map,
        '--output', path_out,
        '--datetime', "{{ execution_date.in_timezone('UTC').isoformat() }}",
    ]
    if partition_spec_hint is not None:
        args.append('--partition-spec-hint')
        args.append(partition_spec_hint)

    convert = SparkSubmitOperator.for_virtualenv(
        task_id='convert_to_esbulk',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/convert_to_esbulk.py',
        driver_memory='4g',
        driver_memory_overhead='1024',
        executor_memory='3g',
        executor_memory_overhead='768',
        executor_cores=1,
        max_executors=25,
        application_args=args)

    probe = ShortCircuitOperator(
        task_id="probe_skip_indicator",
        python_callable=continue_if_nonexistent,
        op_kwargs={
            "path": f"{path_out}_SKIPPED"
        }
    )

    # Ship to production
    upload = swift_upload(
        task_id='upload_to_swift',
        container=swift_container,
        source_directory=path_out,
        object_prefix='{{ ds_nodash }}T{{ execution_date.hour }}/' + rel_path,
        # Auto versioning will add a timestamp to the prefix, ensuring
        # retrying a task uploads to a different prefix
        auto_version=True,
        event_per_object=True,
        event_stream=event_stream)

    return convert, probe, upload
