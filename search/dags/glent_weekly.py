"""Run the glent query suggestions pipeline"""
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.latest_only import LatestOnlyOperator
from airflow.operators.python import PythonOperator
from airflow.utils.trigger_rule import TriggerRule

from search.config.dag_config import artifact, get_default_args, eventgate_datacenters, hdfs_temp_directory
from search.shared.hdfs_cli import HdfsCliHook
from search.shared.swift_upload import swift_upload
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor

var_props = VariableProperties('glent_conf')

table_canonical_wikis = var_props.get('table_canonical_wikis', 'canonical_data.wikis')
table_cirrus_event = var_props.get('table_cirrus_event', 'event.mediawiki_cirrussearch_request')
table_m0_prep = var_props.get('table_m0_prep', 'glent.m0prep')
table_m1_prep = var_props.get('table_m1_prep', 'glent.m1prep')
table_suggestions = var_props.get('table_suggestions', 'glent.suggestions')

max_n_queries_per_ident = var_props.get('max_n_queries_per_ident', 2000)
max_edit_dist_sugg = var_props.get('max_edit_dist_sugg', 3)
max_norm_edit_dist = var_props.get('max_norm_edit_dist', 1.5)
min_hits_diff = var_props.get('min_hits_diff', 100)
min_hits_perc_diff = var_props.get('min_hits_perc_diff', 10)
sugg_max_ts_diff = var_props.get('sugg_max_ts_diff', 'PT120S')

swift_delete_after_days = var_props.get('swift_delete_after_days', 7)
swift_container = var_props.get('swift_container', "search_glent")
query_normalizer_m0 = var_props.get('query_normalizer_m0', 'icutoknorm')
query_normalizer_m1 = var_props.get('query_normalizer_m1', 'icutoknorm')

glent_jar_path = artifact('glent-0.3.5-jar-with-dependencies.jar')

# Temporary paths for each run, deleted after dagrun is done (success or fail)
temp_dir = var_props.get('base_temp_dir', hdfs_temp_directory) + '/{{ dag.dag_id }}_{{ ds }}'
temp_candidates_dir = temp_dir + '/query_similarity_candidates'
temp_esbulk_dir = temp_dir + '/esbulk'

# Glent jobs were converted from oozie, which dates jobs with the end
# of a period, to airflow that dates jobs with the start. For this reason
# all of the outputs continue the way oozie dated partitions, and the
# "current" output is written to what airflow considers date + 7 days.

# timestamps used to select ranges in glent
PREV_INSTANCE = '{{ ds }}T00:00:00Z'
CUR_INSTANCE = '{{ macros.ds_add(ds, 7) }}T00:00:00Z'
LEGAL_CUTOFF = '{{ macros.ds_add(ds, -77) }}T00:00:00Z'

# partition names
PREV_PARTITION = '{{ ds_nodash }}'
CUR_PARTITION = '{{ macros.ds_format(macros.ds_add(ds, 7), "%Y-%m-%d", "%Y%m%d") }}'

# Default kwargs for all Operators
default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2020, 4, 25)),
})


def glent_op(
        task_id, conf=None, executor_memory='6G',
        executor_cores='3', driver_memory='4G',
        max_executors=70, **kwargs,
):
    """Helper applies defaults for invoking glent via spark"""
    if conf is None:
        conf = {}
    return SparkSubmitOperator(
        task_id=task_id,
        conf={
            **default_args.get('conf', {}),
            'spark.sql.shuffle.partitions': 200,
            'spark.sql.executor.memoryOverhead': '640M',
            'spark.sql.shuffle.service.enabled': 'true',
            **conf
        },
        max_executors=max_executors,
        executor_memory=executor_memory,
        executor_cores=executor_cores,
        driver_memory=driver_memory,
        application=glent_jar_path,
        **kwargs
    )


with DAG(
        'glent_weekly',
        default_args=default_args,
        # Once a week at 2am saturday morning. This lines up with the prior
        # oozie job schedule.
        # expression order: min hour month dom dow
        schedule_interval='0 2 * * 6',
        # As a weekly job there should never be more than one running at a time.
        max_active_runs=1,
        catchup=True,
) as dag:
    # Wait for backend logs from CirrusSearch
    wait_for_data = RangeHivePartitionSensor(
        task_id='wait_for_data',
        table_name=table_cirrus_event,
        from_timestamp='{{ execution_date }}',
        to_timestamp='{{ execution_date.add(days=7) }}',
        granularity='@hourly',
        pre_partitions=[[f'datacenter={dc}' for dc in eventgate_datacenters]]
    )

    # Merge the recent week of events into m0prep
    merge_session_similarity = glent_op(
        task_id='merge_session_similarity',
        # We maintain a single partition of data that has new data merged in
        # and old data dropped each week. For this to work a merge can only run
        # if the previous merge was a success.
        depends_on_past=True,
        application_args=[
            'm0prep',
            # WithCirrusLog
            '--wmf-log-name', table_cirrus_event,
            '--log-ts-from', PREV_INSTANCE,
            '--log-ts-to', CUR_INSTANCE,
            '--max-n-queries-per-ident', max_n_queries_per_ident,
            '--map-wikiid-to-lang-name', table_canonical_wikis,
            # WithGlentPartition
            '--input-table', table_m0_prep,
            '--input-partition', PREV_PARTITION,
            # WithSuggestionFilter
            '--max-edit-dist-sugg', max_edit_dist_sugg,
            '--max-norm-edit-dist', max_norm_edit_dist,
            '--min-hits-diff', min_hits_diff,
            '--min-hits-perc-diff', min_hits_perc_diff,
            # WithPrepOutput
            '--output-table', table_m0_prep,
            '--output-partition', CUR_PARTITION,
            '--max-output-partitions', '5',
            # use custom normalizer
            '--query-normalizer', query_normalizer_m0
        ]
    )

    # Merge the recent week of events into m1prep
    merge_query_similarity = glent_op(
        task_id='merge_query_similarity',
        # We maintain a single partition of data that has new data merged in
        # and old data dropped each week. For this to work a merge can only run
        # if the previous merge was a success.
        depends_on_past=True,
        application_args=[
            'm1prep',
            '--wmf-log-name', table_cirrus_event,
            '--log-ts-from', PREV_INSTANCE,
            '--log-ts-to', CUR_INSTANCE,
            '--max-n-queries-per-ident', max_n_queries_per_ident,
            '--map-wikiid-to-lang-name', table_canonical_wikis,
            '--input-table', table_m1_prep,
            '--input-partition', PREV_PARTITION,
            '--earliest-legal-ts', LEGAL_CUTOFF,

            '--output-table', table_m1_prep,
            '--output-partition', CUR_PARTITION,
            # use custom normalizer
            '--query-normalizer', query_normalizer_m1,
        ]
    )

    generate_dictionary_similarity_suggestions = glent_op(
        task_id='generate_dictionary_similarity_suggestions',
        # We maintain a single partition of data that has new data merged in
        # and old data dropped each week. For this to work a merge can only run
        # if the previous merge was a success.
        depends_on_past=True,
        application_args=[
            'm2run',
            '--wmf-log-name', table_cirrus_event,
            '--log-ts-from', PREV_INSTANCE,
            '--log-ts-to', CUR_INSTANCE,
            '--max-n-queries-per-ident', max_n_queries_per_ident,
            '--map-wikiid-to-lang-name', table_canonical_wikis,

            '--input-table', table_suggestions,
            '--input-partition', PREV_PARTITION,
            '--earliest-legal-ts', LEGAL_CUTOFF,

            '--output-table', table_suggestions,
            '--output-partition', CUR_PARTITION,
            '--max-output-partitions', '2'
        ])

    # Suggestions generation, particularly for query similarity, is an
    # expensive process. When backfilling only the most recent suggestions
    # will be used, so don't bother generating the old suggestions.
    latest_only = LatestOnlyOperator(task_id='latest_only')

    # Generate and filter session similarity suggestions.
    generate_session_similarity_suggestions = glent_op(
        task_id='generate_session_similarity_suggestions',
        application_args=[
            'm0run',
            '--input-table', table_m0_prep,
            '--input-partition', CUR_PARTITION,
            '--output-table', table_suggestions,
            '--output-partition', CUR_PARTITION,
            '--max-output-partitions', '5',
        ],
    )

    # Generate potential query similarity suggestion candidates. Separated from
    # candidate filtering to allow different cpu/memory ratio for a heavy and
    # long running job.
    generate_query_similarity_candidates = glent_op(
        task_id='generate_query_similarity_candidates',
        # This is a very heavy job, put it in the sequential queue that
        # prevents multiple heavy jobs from running concurrently.
        pool='sequential',
        conf={
            **default_args.get('conf', {}),
            # Increase required from defaults for returning
            # the FSTs to the driver
            'spark.driver.maxResultSize': '4096M',
        },
        # This allocates ~900GB and 800 cores, almost
        # half the available cores.
        max_executors=50,
        # FST evaluation has minimal memory requirements, and
        # sharing a large data structure between all tasks on
        # same executor. Size up executors accordingly.
        executor_memory='16G',
        executor_cores='16',
        # Final FST merge occurs on the driver. 24G OOMs as of may 2020.
        driver_memory='32G',
        application_args=[
            'm1run.candidates',
            '--input-table', table_m1_prep,
            '--input-partition', CUR_PARTITION,
            '--output-directory', temp_candidates_dir,
            '--num-fst', '1',
        ],
    )

    resolve_query_similarity_suggestions = glent_op(
        task_id='resolve_query_similary_suggestions',
        # This is a very heavy job, put it in the sequential queue that
        # prevents multiple heavy jobs from running concurrently.
        pool='sequential',
        conf={
            **default_args.get('conf', {}),
            'spark.sql.shuffle.partitions': 2500,
            # The joins performed in here generation billions
            # of rows. To avoid `FetchFailedException` on the largest
            # joins we need a few extra parameters.
            # https://stackoverflow.com/a/49781377
            'spark.reducer.maxReqsInFlight': 1,
            'spark.shuffle.io.retryWait': '120s',
            'spark.shuffle.io.maxRetries': 10,
        },
        max_executors=150,
        executor_memory='8G',
        executor_cores='4',
        application_args=[
            'm1run',
            '--input-table', table_m1_prep,
            '--input-partition', CUR_PARTITION,
            '--candidates-directory', temp_candidates_dir,

            '--max-edit-dist-sugg', max_edit_dist_sugg,
            '--max-norm-edit-dist', max_norm_edit_dist,
            '--min-hits-diff', min_hits_diff,
            '--min-hits-perc-diff', min_hits_perc_diff,

            '--output-table', table_suggestions,
            '--output-partition', CUR_PARTITION,
            '--max-output-partitions', '100',
        ])

    esbulk = glent_op(
        task_id='esbulk',
        application_args=[
            'esbulk',
            '--input-table', table_suggestions,
            '--input-partition', CUR_PARTITION,
            '--output-path', temp_esbulk_dir,
            '--version-marker', CUR_PARTITION,
        ])

    swift_upload = swift_upload(
        task_id='swift_upload',
        overwrite=True,
        delete_after=timedelta(days=swift_delete_after_days),
        container=swift_container,
        source_directory=temp_esbulk_dir,
        object_prefix=CUR_PARTITION)

    complete = DummyOperator(task_id='complete')

    cleanup_temp_path = PythonOperator(
        task_id='cleanup_temp_path',
        trigger_rule=TriggerRule.ALL_DONE,
        python_callable=HdfsCliHook.rm,
        op_args=[temp_dir],
        op_kwargs={'recurse': True, 'force': True})

    # Data collection stage: reads from previous run
    # glent partitions and event logs
    (wait_for_data
     >> [
         merge_session_similarity, merge_query_similarity,
         generate_dictionary_similarity_suggestions
     ] >> latest_only)

    # Session similarity suggestions (method 0)
    latest_only >> generate_session_similarity_suggestions >> esbulk

    # Query similarity suggestions (method 1). This is separate from above
    # because it has an additional dependency compared to the simpler
    # suggestion methods.
    (latest_only
     >> generate_query_similarity_candidates
     >> resolve_query_similarity_suggestions
     >> esbulk)

    # Final stage: ship to prod and cleanup.
    esbulk >> swift_upload >> complete >> cleanup_temp_path
