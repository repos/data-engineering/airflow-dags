from airflow import DAG
from airflow.models import BaseOperator
from airflow.operators.dummy_operator import DummyOperator
from dataclasses import dataclass
from datetime import datetime, timedelta
from mergedeep import merge
from search.config.dag_config import artifact, data_path, get_default_args, pool
from search.shared.auto_size_spark import AutoSizeForMatrix, AutoSizeSparkSubmitOperator
from search.shared.hive_table_path import HiveTablePath
from search.shared.swift_upload import swift_upload
from typing import Any, List, Mapping, Optional, Sequence, Tuple, Union, Callable
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator

dag_id = 'mjolnir_weekly'
var_props = VariableProperties(f'{dag_id}_config')

# extended default args for this dag
default_args = merge(
    {},
    get_default_args(),
    {
        # Move to a dedicated pool to avoid blocking other DAGs in sequence.
        # We enforce only one concurrent DAG and task run during DAG instantiation.
        'pool': pool("mjolnir_pool"),
        'executor_cores': 1,
        'executor_memory': '2g',
        'conf': {
            'spark.dynamicAllocation.maxExecutors': 400,
            'spark.sql.shuffle.partitions': 1000,
            'spark.executor.memoryOverhead': '512m',
        },
        'agg_memory_limit': '1T',
        'agg_cores_limit': 450,
    }
)

wikis_to_train = var_props.get_list('wikis', [
    'arwiki', 'dewiki', 'enwiki', 'fawiki',
    'fiwiki', 'frwiki', 'hewiki', 'idwiki',
    'itwiki', 'jawiki', 'kowiki', 'nlwiki',
    'nowiki', 'plwiki', 'ptwiki', 'ruwiki',
    'svwiki', 'viwiki', 'zhwiki',
])

# Feature set to collect. Must exist on prod search clusters.
feature_set_name = var_props.get('feature_set', '20180215-query_explorer')
pruned_feature_set_name = var_props.get('pruned_feature_set', feature_set_name + '-pruned_mrmr')

# shared keys used in table partitioning
query_clustering_algo = 'norm_query'
query_labeling_algo = 'dbn'

# Default data locations. Hive tables will resolve into a path, hdfs uris
# can be used directly.
mjolnir_data_dir = f'{data_path}/mjolnir'
hive_db = var_props.get('hive_db', 'mjolnir')
OUTPUT_BASE = {
    'query_clicks_raw': 'discovery.query_clicks_daily',
    'query_clicks': f'{hive_db}.query_clicks_ltr',
    'query_clustering': f'{hive_db}.query_clustering',
    'labeled_query_page': f'{hive_db}.labeled_query_page',
    'feature_vectors': f'{hive_db}.feature_vectors',
    'training_files': mjolnir_data_dir + '/training_files',
    'model_parameters': f'{hive_db}.model_parameters',
    'trained_models': mjolnir_data_dir + '/trained_models',
}

# Paths to deployed artifacts
mjolnir_version = '2.0'
mjolnir_jar = artifact(f'mjolnir-{mjolnir_version}.jar')

refinery_version = '0.2.55'
refinery_hive_jar = artifact(f'refinery-hive-{refinery_version}-shaded.jar')

env_version = '2.4.0'
conda_env_tgz = artifact(f'mjolnir-{env_version}.conda.tgz')

# Shared CLI args for scripts that talk with kafka
kafka_cli_args = [
    '--brokers', ','.join([
        'kafka-jumbo1007.eqiad.wmnet:9092',
        'kafka-jumbo1008.eqiad.wmnet:9092',
        'kafka-jumbo1010.eqiad.wmnet:9092']),
    '--topic-request', 'mjolnir.msearch-prod-request',
    '--topic-response', 'mjolnir.msearch-prod-response',
]

@dataclass
class MjolnirTask:
    op: AutoSizeSparkSubmitOperator
    output_base: str
    partition_spec: Sequence[Tuple[str, str]]
    output_path: str

    def __rshift__(self, other):
        if isinstance(other, MjolnirTask):
            return self.op >> other.op
        return self.op >> other

    def __lshift__(self, other):
        if isinstance(other, MjolnirTask):
            return self.op << other.op
        return self.op << other

    # Forward Airflow's task dependency methods to the underlying operator
    def set_downstream(self, task: Union['MjolnirTask', BaseOperator]):
        if isinstance(task, MjolnirTask):
            return self.op.set_downstream(task.op)
        return self.op.set_downstream(task)

    def set_upstream(self, task: Union['MjolnirTask', BaseOperator]):
        if isinstance(task, MjolnirTask):
            return self.op.set_upstream(task.op)
        return self.op.set_upstream(task)

    # Forward the update_relative method
    def update_relative(self, other: Union['MjolnirTask', BaseOperator], upstream: bool = True, edge_modifier: Optional[Callable] = None):
        if isinstance(other, MjolnirTask):
            return self.op.update_relative(other.op, upstream=upstream, edge_modifier=edge_modifier)
        return self.op.update_relative(other, upstream=upstream, edge_modifier=edge_modifier)

    # Forward task traversal attributes and methods
    @property
    def roots(self) -> list[BaseOperator]:
        return self.op.roots

    @property
    def leaves(self) -> list[BaseOperator]:
        return self.op.leaves

    def get_direct_relatives(self, upstream: bool = False):
        return self.op.get_direct_relatives(upstream)

    def get_flat_relatives(self, upstream: bool = False):
        return self.op.get_flat_relatives(upstream)


def mjolnir_task(
    transformer: str,
    transformer_args: List[str],
    output_base: str,
    partition_spec: Sequence[Tuple[str, str]],
    jars: Optional[str] = None,
    conf: Optional[Mapping] = None,
    **kwargs: Any
) -> MjolnirTask:
    base_jars = f'{mjolnir_jar},{refinery_hive_jar}'
    jars = base_jars if jars is None else f'{base_jars},{jars}'

    if conf is None:
        conf = default_args['conf']
    else:
        conf = merge({}, default_args['conf'], conf)

    output_path = f'{{{{ "{output_base}" | hive_table_path }}}}/' \
        + '/'.join(f'{k}={v}' for k, v in partition_spec)

    op = AutoSizeSparkSubmitOperator.for_virtualenv(
        virtualenv_archive=conda_env_tgz,
        jars=jars,
        conf=conf,
        entry_point='bin/mjolnir-utilities.py',
        application_args=[
            transformer,
            '--date', '{{ ds_nodash }}',
            '--output-path', output_path
        ] + transformer_args,
        **kwargs
    )

    return MjolnirTask(op, output_base, partition_spec, output_path)


def query_clicks_ltr() -> MjolnirTask:
    return mjolnir_task(
        task_id='query_clicks_ltr',
        transformer='query_clicks_ltr',
        output_base=OUTPUT_BASE['query_clicks'],
        partition_spec=[
            ('date', '{{ ds_nodash }}'),
        ],
        transformer_args=[
            '--input-table', OUTPUT_BASE['query_clicks_raw'],
            '--output-table', OUTPUT_BASE['query_clicks'],
            '--max-q-by-day', var_props.get_parsed('max_q_by_day_filter', int, 50)
        ])


def norm_query(clicks: MjolnirTask) -> MjolnirTask:
    task = mjolnir_task(
        task_id='norm_query_clustering',
        driver_memory='8g',
        transformer='norm_query_clustering',
        output_base=OUTPUT_BASE['query_clustering'],
        partition_spec=[
            ('date', '{{ ds_nodash }}'),
            ('algorithm', query_clustering_algo)
        ],
        transformer_args=kafka_cli_args  + [
            '--clicks-table', clicks.output_base,
            '--output-table', OUTPUT_BASE['query_clustering'],
            '--top-n', var_props.get_parsed('norm_query_top_n', int, 5),
            '--min-sessions-per-query', var_props.get_parsed('norm_query_min_sessions_per_query', int, 10),
        ])
    clicks.op >> task.op
    return task


def dbn(
    clicks: MjolnirTask,
    clusters: MjolnirTask,
) -> MjolnirTask:
    # TODO: Output partitioning doesn't take clusters into account
    task = mjolnir_task(
        task_id=f'dbn-{query_clustering_algo}',
        executor_cores=4,
        executor_memory='6g',
        conf={
            'spark.sql.shuffle.partitions': 5000,
        },
        transformer='dbn',
        output_base=OUTPUT_BASE['labeled_query_page'],
        partition_spec=[
            ('date', '{{ ds_nodash }}'),
            ('algorithm', 'dbn'),
        ],
        transformer_args=[
            '--clicks-table', clicks.output_base,
            '--clustering-table', clusters.output_base,
            '--clustering-algo', query_clustering_algo,
            '--output-table', OUTPUT_BASE['labeled_query_page'],
        ])
    clicks.op >> task.op
    clusters.op >> task.op
    return task


def collect_vectors(
    clicks: MjolnirTask,
    clusters: MjolnirTask,
) -> MjolnirTask:
    # TODO: Output partitioning doesn't take inputs into account. We should
    # probably at least be able to vary the elasticsearch feature set name from
    # the feature set name written to disk.
    task = mjolnir_task(
        task_id=f'feature_vectors-{query_clustering_algo}-{feature_set_name}',
        transformer='feature_vectors',
        output_base=OUTPUT_BASE['feature_vectors'],
        partition_spec=[
            ('date', '{{ ds_nodash }}'),
            ('feature_set', feature_set_name),
        ],
        transformer_args=kafka_cli_args + [
            '--clicks-table', clicks.output_base,
            '--clustering-table', clusters.output_base,
            '--clustering-algorithm', query_clustering_algo,
            '--output-table', OUTPUT_BASE['feature_vectors'],
            '--feature-set', feature_set_name,
            # Maximum number of records we can train against in a single
            # instance. Practically this only effects enwiki.
            '--samples-per-wiki', var_props.get_parsed('max_samples_per_wiki', int, 27000000),
            '--wikis'] + wikis_to_train
        )
    clicks.op >> task.op
    clusters.op >> task.op
    return task


def prune_vectors(
    vectors: MjolnirTask,
    labels: MjolnirTask,
) -> MjolnirTask:
    task = mjolnir_task(
        task_id=f'feature_selection-{feature_set_name}-{query_labeling_algo}-{pruned_feature_set_name}',
        execution_timeout=timedelta(hours=4),
        driver_memory='24G',
        skein_memory="28G",  # FIXME: sets skein container memory to account for memoryOverhead settings.
        jars=','.join([
            artifact('sramirez-spark-MDLP-discretization-1.5.0.jar'),
            artifact('sramirez-spark-infotheoretic-feature-selection-1.5.0.jar'),
        ]),
        executor_cores=2,
        executor_memory='5g',
        conf={
            'spark.locality.wait': 0,
            'spark.driver.memoryOverhead': '4g',
            'spark.executor.memoryOverhead': '1g',
            'spark.driver.maxResultSize': '6g',
        },
        transformer='feature_selection',
        output_base=vectors.output_base,
        partition_spec=[
            ('date', '{{ ds_nodash }}'),
            ('feature_set', pruned_feature_set_name),
        ],
        transformer_args=[
            '--feature-vectors-table', vectors.output_base,
            '--feature-set', feature_set_name,
            '--labels-table', labels.output_base,
            '--labeling-algorithm', query_labeling_algo,
            '--output-table', vectors.output_base,
            '--output-feature-set', pruned_feature_set_name,
            '--num-features', var_props.get_parsed('pruned_feature_set_size', int, 50),
            '--wikis'] + wikis_to_train,
    )
    labels.op >> task.op
    vectors.op >> task.op
    return task


def make_folds(wiki: str, vectors: MjolnirTask, labels: MjolnirTask) -> MjolnirTask:
    task = mjolnir_task(
        task_id=f'make_folds-{wiki}-{query_labeling_algo}-{pruned_feature_set_name}',
        autosize_for_matrix=AutoSizeForMatrix(
            metadata_uri=f'{ vectors.output_path }/_METADATA.JSON',
            bytes_per_value=30,
            num_obs_path=f'num_obs.{wiki}',
            features_path=f'wiki_features.{wiki}',
        ),
        transformer='make_folds',
        output_base=OUTPUT_BASE['training_files'],
        partition_spec=[
            ('date', '{{ ds_nodash }}'),
            ('wikiid', wiki),
            ('labeling_algorithm', query_labeling_algo),
            ('feature_set', pruned_feature_set_name),
        ],
        transformer_args=[
            '--feature-vectors-table', vectors.output_base,
            '--feature-set', pruned_feature_set_name,
            '--labels-table', labels.output_base,
            '--labeling-algorithm', query_labeling_algo,
            '--wiki', wiki,
            '--num-folds', var_props.get_parsed('num_folds', int, 5),
        ])
    vectors.op >> task.op
    labels.op >> task.op
    return task


def hyperparam(wiki: str, training_files: MjolnirTask) -> MjolnirTask:
    task = mjolnir_task(
        task_id=f'hyperparam-{wiki}-{query_labeling_algo}-{pruned_feature_set_name}',
        autosize_for_matrix=AutoSizeForMatrix(
            metadata_uri=f'{ training_files.output_path }/_METADATA.JSON',
            bytes_per_value=30,
            num_obs_path=f'metadata.num_obs',
            features_path=f'metadata.features',
        ),
        driver_memory='3g',
        executor_cores='6',
        conf={
            'spark.dynamicAllocation.executorIdleTimeout': '180s',
            'spark.task.cpus': 6,
        },
        transformer='hyperparam',
        output_base=OUTPUT_BASE['model_parameters'],
        partition_spec=training_files.partition_spec,
        transformer_args=[
            '--training-files-path', training_files.output_path,
            '--output-table', OUTPUT_BASE['model_parameters'],
            # hyperparameter search configuration
            '--initial-num-trees', '100',
            '--final-num-trees', '500',
            '--iterations', '150',
            '--num-cv-jobs', '75',
        ])
    training_files.op >> task.op
    return task


def train(
    wiki: str,
    training_files: MjolnirTask,
    model_parameters: MjolnirTask,
) -> MjolnirTask:
    task = mjolnir_task(
        task_id=f'train-{wiki}-{query_labeling_algo}-{pruned_feature_set_name}',
        autosize_for_matrix=AutoSizeForMatrix(
            metadata_uri=f'{ training_files.output_path }/_METADATA.JSON',
            bytes_per_value=22,
            num_obs_path=f'metadata.num_obs',
            features_path=f'metadata.features',
        ),
        driver_memory='2g',
        executor_cores=6,
        conf={
            'spark.task.cpus': 6,
        },
        transformer='train',
        output_base=OUTPUT_BASE['trained_models'],
        partition_spec=training_files.partition_spec,
        transformer_args=[
            '--model-parameters-table', model_parameters.output_base,
            '--training-files-path', training_files.output_path,
            '--remote-feature-set', feature_set_name,
        ])
    training_files.op >> task.op
    model_parameters.op >> task.op
    return task


def upload(wiki: str, trained_model: MjolnirTask) -> SimpleSkeinOperator:
    op = swift_upload(
        task_id=f'upload-{wiki}-{query_labeling_algo}-{pruned_feature_set_name}',
        overwrite=True,
        delete_after=timedelta(days=7),
        source_directory=trained_model.output_path,
        container='search_mjolnir_model',
        object_prefix='{{ ds_nodash }}',
        auto_version=True)
    trained_model.op >> op
    return op


with DAG(
    dag_id,
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': var_props.get_datetime('start_date', datetime(2023, 2, 20)),
    }),
    schedule=timedelta(days=7),
    # If we don't run for a given week there is no use in re-running it,
    # the process always reads the full query_clicks history.
    catchup=False,
    # Works together with catchup to ensure we don't have multiples running
    # at the same time
    max_active_runs=1,
    max_active_tasks=1,  # tasks are executed sequentially (ACTIVE state)
    concurrency=1,  # Limits the number of tasks in RUNNING state to 1. Modify if we introduce sensors.
    user_defined_filters={
        'hive_table_path': HiveTablePath(default_args['metastore_conn_id']),
    }
) as dag:
    clicks = query_clicks_ltr()
    clusters = norm_query(clicks)
    labels = dbn(clicks, clusters)
    raw_vectors = collect_vectors(clicks, clusters)
    vectors = prune_vectors(raw_vectors, labels)

    # Create sequential dependencies
    clicks >> clusters >> labels >> raw_vectors >> vectors

    # "fan-out" from vectors to all the wiki training pipelines,
    # followed by a "fan-in" to training_complete.
    training_complete = DummyOperator(task_id='complete')
    for wiki in wikis_to_train:
        training_files = make_folds(wiki, vectors, labels)
        model_parameters = hyperparam(wiki, training_files)
        trained_model = train(wiki, training_files, model_parameters)
        uploaded = upload(wiki, trained_model)

        # Make each wiki's training pipeline depend on vectors being complete
        vectors >> training_files

        # Create dependencies for model training tasks
        training_files >> model_parameters >> trained_model >> uploaded >> training_complete
