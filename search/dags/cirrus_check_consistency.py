"""
Schedule the check_cirrus_index_consistency.py scripts which attempts to compare the state
of the cirrus index dumps and the mysql page_table.
The script expects two cirrus partitions to be passed and a page_table one.
The cirrus partitions (weekly) must be made before and after the page_table (monthly) one.
Because we mix weekly and monthly schedule with use the cron expression to run on the
first sunday of the month when we know that the two cirrus snapshot we need have been scheduled
and the month db snapshot as well.
"""
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from airflow.sensors.base import BaseSensorOperator

from wmf_airflow_common.sensors.url import URLSensor

from search.config.dag_config import data_path, \
    discolytics_conda_env_tgz, get_default_args, hadoop_name_node
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = 'cirrus_check_consistency'
var_props = VariableProperties(f'{dag_id}_conf')

cirrus_index_inconsistencies_table = var_props.get('cirrus_index_inconsistencies_table',
                                                   'discovery.cirrus_index_inconsistencies')
cirrus_index_inconsistencies_location = var_props.get('cirrus_index_inconsistencies_location',
                                                      'cirrus/index_inconsistencies')

cirrus_indexes_without_content_table = var_props.get('cirrus_indexes_without_content_table',
                                                     'discovery.cirrus_index_without_content')

mysql_page_table = var_props.get('mysql_page_table',
                                 'wmf_raw.mediawiki_page')

prometheus_push_gw = var_props.get('prometheus_push_gw',
                                   'prometheus-pushgateway.discovery.wmnet')

default_args = get_default_args()

# Assuming a run starts on sunday 2023-09-03
# the interval start of the monthly dag is 2023-08-06
# we want the cirrus dumps made before and after 2023-09-01
# these should be those executed on 2023-08-27 and 2023-09-03 with partition snapshot set at the
# start period these become 2023-08-20 and 2023-08-27 (hence the -14 and -7 on the end interval of
# this dag)
cirrus_dataset_before = '{{ data_interval_end | add_days(-14) | to_ds_nodash }}'
cirrus_dataset_after = '{{ data_interval_end | add_days(-7) | to_ds_nodash }}'
# the mysql snapshot should always be at the month of the start interval
monthly_snapshot = '{{ data_interval_start | to_ds_month }}'

# Because we push data to prometheus we want to run close to the availability of the partitions
# prometheus does not allow us to provide the time of the datapoint and will use the current time
sensors_sla = timedelta(days=3)

with DAG(
    f'{dag_id}_init',
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': datetime(2023, 6, 21),
    }),
    schedule='@once',
) as dag_init:
    HiveOperator(
        task_id='create_table',
        hql=f"""
            CREATE TABLE { cirrus_index_inconsistencies_table } (
              `wiki` string,
              `page_id` bigint,
              `content_model` string,
              `namespace` bigint,
              `revision`  bigint,
              `timestamp` string,
              `inconsistency_type` string,
              `num_pages` bigint,
              `num_redirects` bigint
            )
            PARTITIONED BY (
              `cirrus_replica` string,
              `snapshot` string
            )
            STORED AS parquet
            LOCATION '{ data_path }/{ cirrus_index_inconsistencies_location }'
        """
    ) >> EmptyOperator(task_id='complete')


def consistency_check_task(dc: str) -> SparkSubmitOperator:
    return SparkSubmitOperator.for_virtualenv(
        task_id=f'inconsistency_check_{dc}',
        conf={
            **default_args.get('conf', {}),
            # This seems to help reduce the memory overhead necessary. If too
            # small a specific exception will let us know we are using more direct
            # buffers than expected, rather than a generic killed by yarn error.
            'spark.executor.extraJavaOptions': '-XX:MaxDirectMemorySize=1024M',
        },
        executor_cores=1,
        executor_memory='4g',
        # Outputing giant rows seems to need excessive memory overhead to work
        executor_memory_overhead='4g',
        max_executors=36,
        driver_memory='8g',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/check_cirrus_index_consistency.py',
        application_args=[
            '--push-gw-endpoint', prometheus_push_gw,
            '--index-partition-before',  f'{cirrus_indexes_without_content_table}/cirrus_replica={dc}/snapshot={cirrus_dataset_before}',
            '--index-partition-after', f'{cirrus_indexes_without_content_table}/cirrus_replica={dc}/snapshot={cirrus_dataset_after}',
            '--page-table-partition', f'{mysql_page_table}/snapshot={monthly_snapshot}',
            '--output', f'{cirrus_index_inconsistencies_table}/snapshot={monthly_snapshot}'
        ]
    )


def wait_for_data(dc: str) -> NamedHivePartitionSensor:
    return NamedHivePartitionSensor(
        task_id='wait_for_data_' + dc,
        mode='reschedule',
        sla=sensors_sla,
        partition_names=[
            f'{cirrus_indexes_without_content_table}/cirrus_replica={dc}/snapshot={cirrus_dataset_before}',
            f'{cirrus_indexes_without_content_table}/cirrus_replica={dc}/snapshot={cirrus_dataset_after}'
        ])


def wait_for_mediawiki_page_snapshot() -> BaseSensorOperator:
    return URLSensor(
        url=f'{hadoop_name_node}/wmf/data/raw/mediawiki/tables/page/'
            f'snapshot={monthly_snapshot}/_PARTITIONED',
        task_id='wait_for_mediawiki_page_snapshot',
        sla=sensors_sla,
    )


with DAG(
    f'{dag_id}_monthly',
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': datetime(2023, 7, 2),
    }),
    user_defined_filters=filters,
    schedule="0 0 * * 0#1"
) as dag_monthly:
    sensors = [wait_for_data('codfw'), wait_for_mediawiki_page_snapshot()]
    sensors >> consistency_check_task('codfw') >> EmptyOperator(task_id='complete')

