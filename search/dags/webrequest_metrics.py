from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.operators.bash import BashOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from datetime import datetime
from search.config.dag_config import YMD_PARTITION, data_path, \
    discolytics_conda_env_tgz, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor

dag_id = 'webrequest_metrics'
var_props = VariableProperties(f'{dag_id}_conf')

webrequest_table = var_props.get('webrequest_table', 'wmf.webrequest')
output_table = var_props.get('output_table', 'discovery.webrequest_metrics')
table_path = f'{data_path}/metrics/webrequest'

default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2024, 3, 1)),
})

with DAG(
    f'{dag_id}_init',
    default_args=default_args,
    schedule_interval='@once',
) as dag_init:
    create_webreq = HiveOperator(
        task_id='create_table',
        hql=f"""
            CREATE TABLE {output_table} (
              `access_method` STRING,
              `normalized_host` STRUCT<
                  `project_class`: STRING,
                  `project`: STRING,
                  `qualifiers`: ARRAY<STRING>,
                  `tld`: STRING,
                  `project_family`: STRING>,
              `country` STRING,
              `browser_family` STRING,
              `os_family` STRING,
              `num_actors_w_pageviews` BIGINT,
              `num_actors_w_autocomplete_pv` BIGINT,
              `num_actors_w_special_search_pv` BIGINT,
              `num_actors_w_related_articles_pv` BIGINT,
              `num_actors_w_external_search_pv` BIGINT,

              `num_actors_w_internal_pv` BIGINT,
              `num_actors_w_search_pv` BIGINT,
              `num_actors_w_discovery_pv` BIGINT,

              `num_actors_w_go_to_req` BIGINT,
              `num_actors_w_serp_req` BIGINT,
              `num_actors_w_ac_req` BIGINT,
              `num_actors_w_morelike_req` BIGINT,
              `num_actors_w_other_api_fulltext_req` BIGINT,

              `num_pageviews` BIGINT,
              `num_autocomplete_pv` BIGINT,
              `num_special_search_pv` BIGINT,
              `num_related_articles_pv` BIGINT,
              `num_external_search_pv` BIGINT,

              `num_internal_pv` BIGINT,
              `num_search_pv` BIGINT,
              `num_discovery_pv` BIGINT,

              `num_go_to_req` BIGINT,
              `num_serp_req` BIGINT,
              `num_ac_req` BIGINT,
              `num_morelike_req` BIGINT,
              `num_other_api_fulltext_req` BIGINT)
            PARTITIONED BY (
              `year` int,
              `month` int,
              `day` int
            )
            STORED AS parquet
            LOCATION '{table_path}'
        """
    ) >> EmptyOperator(task_id='complete')

with DAG(
    f'{dag_id}_daily',
    default_args=default_args,
    schedule_interval='@daily'
) as dag_daily:
    RangeHivePartitionSensor(
        task_id='wait_for_data',
        table_name=webrequest_table,
        from_timestamp='{{ execution_date }}',
        to_timestamp='{{ execution_date.add(days=1) }}',
        pre_partitions=['webrequest_source=text'],
        granularity='@hourly'
    ) >> SparkSubmitOperator.for_virtualenv(
        task_id='webrequest_metrics',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/webrequest_metrics.py',
        executor_cores=4,
        executor_memory='12g',
        max_executors=32,
        conf={
            **default_args.get('conf', {}),
            'spark.sql.mapKeyDedupPolicy': 'LAST_WIN',
            'spark.executor.memoryOverheadFactor': 0.15,
        },
        application_args=[
            '--webrequests', f'{webrequest_table}/{YMD_PARTITION}',
            '--output', f'{output_table}/{YMD_PARTITION}',
        ]
    ) >> BashOperator(
        task_id='chgrp',
        bash_command=f'hdfs dfs -chgrp -R analytics-privatedata-users {table_path}/{YMD_PARTITION}',
    ) >> EmptyOperator(task_id='complete')

