
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.hive_operator import HiveOperator
from datetime import datetime
from search.config.dag_config import get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor


dag_id = 'popularity_score'
var_props = VariableProperties(f'{dag_id}_conf')


def ymd_sql_criteria(day: int) -> str:
    """
    With hiveql we used to select the partitions with high level functions like TO_DATE(year, month, year) BETWEEN START AND END
    With SparkSQL this seems to cause much more work on the driver which is struggling with memory doing so.
    Use simpler criteria using only AND/OR and equality.
    :param day:
    :return: SQL fragment selecting a day on the year/month/day partitions for ds + 1 day
    """
    return ("( year = {{ macros.ds_format(macros.ds_add(ds, " + str(day) + "), '%Y-%m-%d', '%Y' ) }} AND" +
            " month = {{ macros.ds_format(macros.ds_add(ds, " + str(day) + "), '%Y-%m-%d', '%m' ) }} AND" +
            " day = {{ macros.ds_format(macros.ds_add(ds, " + str(day) + "), '%Y-%m-%d', '%d' ) }} )")

ymd_partition_criteria = " OR ".join([ymd_sql_criteria(d) for d in range(7)])


HQL = """
INSERT OVERWRITE TABLE discovery.popularity_score
    PARTITION(
        agg_days=7,
        year={{ execution_date.year }},
        month={{ execution_date.month }},
        day={{ execution_date.day }})
    SELECT
        /*+ COALESCE(6) */
        /*+ BROADCAST(agg) */
        hourly.project,
        hourly.page_id,
        hourly.namespace_id as page_namespace,
        SUM(hourly.view_count) / agg.view_count AS score
    FROM
        wmf.pageview_hourly hourly
    JOIN (
        SELECT
            project,
            SUM(view_count) AS view_count
        FROM
            wmf.pageview_hourly
        WHERE
            page_id IS NOT NULL
            AND (
            """ + ymd_partition_criteria + """
            )
        GROUP BY
            project
        ) agg on hourly.project = agg.project
    WHERE
        hourly.page_id IS NOT NULL
        AND (
        """ + ymd_partition_criteria + """
        )
    GROUP BY
        hourly.project,
        hourly.page_id,
        hourly.namespace_id,
        agg.view_count
"""


with DAG(
    'popularity_score_weekly',
    default_args=var_props.get_merged('default_args', {
        **get_default_args(),
        'start_date': var_props.get_datetime('start_date', datetime(2020, 1, 8)),
    }),
    # Once a week at midnight on Sunday morning
    schedule_interval='0 0 * * 0',
    # As a weekly job there should never really be more than
    # one running at a time.
    max_active_runs=1,
    catchup=True,
) as dag:
    # Require hourly partitions to exist before running
    wait_for_pageview_hourly = RangeHivePartitionSensor(
        task_id='wait_for_pageview_hourly',
        # We send a failure email once a day when the expected data is not
        # found. Since this is a weekly job we wait up to 4 days for the data
        # to show up before giving up and waiting for next scheduled run.
        timeout=60 * 60 * 24,  # 24 hours
        # partition range selection
        table_name='wmf.pageview_hourly',
        from_timestamp='{{ execution_date }}',
        to_timestamp='{{ execution_date.add(days=7) }}',
        granularity='@hourly')

    # Generate the data
    popularity_score = SparkSqlOperator(
        task_id='popularity_score',
        sql=HQL,
        driver_memory=var_props.get("driver_memory", "4G"),
        executor_memory=var_props.get("executor_memory", "10G"),
        executor_cores=var_props.get("executor_cores", 2),
        executor_memory_overhead=var_props.get("spark_executor_memoryOverhead", "6G"),
        conf={
            "spark.dynamicAllocation.maxExecutors": 20,
            "spark.hadoop.fs.permissions.umask-mode": "0022"
        }
    )
    wait_for_pageview_hourly >> popularity_score

    # Simplify external task sensor by having a common
    # 'complete' node at end of all dags
    complete = DummyOperator(task_id='complete')
    popularity_score >> complete
