from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from datetime import datetime
from search.config.dag_config import data_path, \
        discolytics_conda_env_tgz, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id = 'import_cirrus_indexes'
var_props = VariableProperties(f'{dag_id}_conf')

cirrus_indexes_table = var_props.get('cirrus_indexes_table', 'discovery.cirrus_index')
cirrus_indexes_without_content_table = var_props.get('cirrus_indexes_without_content_table',
                                                     'discovery.cirrus_index_without_content')
cirrus_indexes_location = var_props.get('cirrus_indexes_location', 'cirrus/index')
cirrus_indexes_without_content_location = var_props.get('cirrus_indexes_without_content_location',
                                                        'cirrus/index_without_content')

default_args = get_default_args()

with DAG(
    f'{dag_id}_init',
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': datetime(2023, 1, 1),
    }),
    schedule='@once',
) as dag_init:
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE { cirrus_indexes_table } (
              `index_name` string,
              `page_id` bigint,
              `auxiliary_text` array<string>,
              `category` array<string>,
              `content_model` string,
              `coordinates` array<struct<
                  `coord`: map<string, double>,
                  `country`:string,
                  `dim`:bigint,
                  `globe`:string,
                  `name`:string,
                  `primary`:boolean,
                  `region`:string,
                  `type`:string>>,
              `create_timestamp` string,
              `defaultsort` string,
              `display_title` string,
              `descriptions` map<string, array<string>>,
              `external_link` array<string>,
              `extra_source` string,
              `file_bits` bigint,
              `file_height` bigint,
              `file_media_type` string,
              `file_mime` string,
              `file_resolution` bigint,
              `file_size` bigint,
              `file_text` string,
              `file_width` bigint,
              `heading` array<string>,
              `incoming_links` bigint,
              `label_count` bigint,
              `labels` map<string, array<string>>,
              `labels_all` array<string>,
              `language` string,
              `lemma` array<string>,
              `lexeme_forms` array<struct<
                  `id`: string,
                  `representation`: array<string>,
                  `features`: array<string>>>,
              `lexeme_language` struct<`entity`: string, `code`: string>,
              `lexical_category` string,
              `local_sites_with_dupe` array<string>,
              `namespace` bigint,
              `namespace_text` string,
              `opening_text` string,
              `outgoing_link` array<string>,
              `popularity_score` double,
              `redirect` array<struct<`namespace`:bigint,`title`:string>>,
              `source_text` string,
              `sitelink_count` bigint,
              `statement_count` bigint,
              `template` array<string>,
              `text` string,
              `text_bytes` bigint,
              `timestamp` string,
              `title` string,
              `version` bigint,
              `wikibase_item` string,
              `weighted_tags` array<string>,
              `statement_keywords` array<string>)
            PARTITIONED BY (
              `cirrus_replica` string,
              `cirrus_group` string,
              `wiki` string,
              `snapshot` string)
            STORED AS avro
            LOCATION '{ data_path }/{ cirrus_indexes_location }'
        """
    ) >> EmptyOperator(task_id='complete')


with (DAG(
        f'{dag_id}_v2_alter',
        default_args=var_props.get_merged('default_args', {
            **default_args,
            'start_date': datetime(2024, 5, 28),
        }),
        schedule='@once',
) as dag_alter_table):
    new_fields = """
              `display_title` string,
              `label_count` bigint,
              `lemma` array<string>,
              `lexeme_language` struct<`entity`: string, `code`: string>,
              `lexical_category` string,
              `sitelink_count` bigint,
              `statement_count` bigint
    """
    new_fields_with_content = new_fields + """,
              `lexeme_forms` array<struct<
                  `id`: string,
                  `representation`: array<string>,
                  `features`: array<string>>>,
              `labels_all` array<string>
    """
    HiveOperator(
        task_id='update_table',
        hql=f"""
            ALTER TABLE { cirrus_indexes_table } ADD COLUMNS ({ new_fields_with_content } )
        """
    ) >> HiveOperator(
        task_id='update_table_without_content',
        hql=f"""
            ALTER TABLE { cirrus_indexes_without_content_table } ADD COLUMNS ({ new_fields } )
        """
    ) >> EmptyOperator(task_id='complete')

with DAG(
        f'{dag_id}_without_content_init',
        default_args=var_props.get_merged('default_args', {
            **default_args,
            'start_date': datetime(2023, 1, 1),
        }),
        schedule='@once',
) as dag_init_without_content:
    HiveOperator(
        task_id='create_tables_without_content',
        hql=f"""
            CREATE TABLE { cirrus_indexes_without_content_table } (
              `index_name` string,
              `page_id` bigint,
              `category` array<string>,
              `content_model` string,
              `coordinates` array<struct<
                  `coord`: map<string, double>,
                  `country`:string,
                  `dim`:bigint,
                  `globe`:string,
                  `name`:string,
                  `primary`:boolean,
                  `region`:string,
                  `type`:string>>,
              `create_timestamp` string,
              `defaultsort` string,
              `display_title` string,
              `file_bits` bigint,
              `file_height` bigint,
              `file_media_type` string,
              `file_mime` string,
              `file_resolution` bigint,
              `file_size` bigint,
              `file_width` bigint,
              `heading` array<string>,
              `incoming_links` bigint,
              `label_count` bigint,
              `language` string,
              `lemma` array<string>,
              `lexeme_language` struct<`entity`: string, `code`: string>,
              `lexical_category` string,
              `local_sites_with_dupe` array<string>,
              `namespace` bigint,
              `namespace_text` string,
              `outgoing_link` array<string>,
              `popularity_score` double,
              `redirect` array<struct<`namespace`:bigint,`title`:string>>,
              `template` array<string>,
              `text_bytes` bigint,
              `sitelink_count` bigint,
              `statement_count` bigint,
              `timestamp` string,
              `title` string,
              `version` bigint,
              `wikibase_item` string,
              `weighted_tags` array<string>,
              `cirrus_group` string,
              `wiki` string
              )
            PARTITIONED BY (
              `cirrus_replica` string,
              `snapshot` string)
            STORED AS parquet
            LOCATION '{ data_path }/{ cirrus_indexes_without_content_location }'
        """
    ) >> EmptyOperator(task_id='complete')

with DAG(
    'import_cirrus_indexes_weekly',
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': var_props.get_datetime('start_date', datetime(2023, 3, 5)),
    }),
    schedule='0 0 * * 0',
    max_active_runs=1,
    catchup=False,
) as weekly_dag:
    SparkSubmitOperator.for_virtualenv(
        task_id='import',
        conf={
            **default_args.get('conf', {}),
            # This seems to help reduce the memory overhead necessary. If too
            # small a specific exception will let us know we are using more direct
            # buffers than expected, rather than a generic killed by yarn error.
            'spark.executor.extraJavaOptions': '-XX:MaxDirectMemorySize=1024M',
            # Must be >= the number of wikis
            'spark.hive.exec.max.dynamic.partitions': '1100',
            # Our hdfs defaults do not allow world-read. This dataset contains
            # some private information (private wikis), but all of it is
            # easily accessible from the network already.
            'spark.hadoop.fs.permissions.umask-mode': '0022'
        },
        # Keep down the number of connections we are making to the prod cluster
        # by limiting executors
        executor_cores=1,
        executor_memory='4g',
        # Outputing giant rows seems to need excessive memory overhead to work
        executor_memory_overhead='4g',
        max_executors=36,
        driver_memory='8g',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/import_cirrus_indexes.py',
        application_args=[
            '--elasticsearch', ",".join(var_props.get_list('elasticsearch_clusters', [
                "https://search.svc.codfw.wmnet:9243",
                "https://search.svc.codfw.wmnet:9443",
                "https://search.svc.codfw.wmnet:9643",
            ])),
            '--output-partition',  cirrus_indexes_table + '/snapshot={{ ds_nodash }}'
        ],
        default_env_vars={
            'SPARK_CONF_DIR': '/etc/spark3/conf',
            'SPARK_HOME': '/usr/lib/spark3',
            'REQUESTS_CA_BUNDLE': '/etc/ssl/certs/ca-certificates.crt',
        }
    ) >> SparkSubmitOperator.for_virtualenv(
        task_id='trim_index',
        conf={
            **default_args.get('conf', {}),
            # This seems to help reduce the memory overhead necessary. If too
            # small a specific exception will let us know we are using more direct
            # buffers than expected, rather than a generic killed by yarn error.
            'spark.executor.extraJavaOptions': '-XX:MaxDirectMemorySize=1024M',
            # Our hdfs defaults do not allow world-read. This dataset contains
            # some private information (private wikis), but all of it is
            # easily accessible from the network already.
            'spark.hadoop.fs.permissions.umask-mode': '0022'
        },
        executor_cores=1,
        executor_memory='4g',
        # Outputing giant rows seems to need excessive memory overhead to work
        executor_memory_overhead='4g',
        max_executors=36,
        driver_memory='8g',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/prune_columns.py',
        application_args=[
            '--profile',
            'cirrus_index_without_content',
            '--input-partition',
            cirrus_indexes_table + '/snapshot={{ ds_nodash }}',
            '--output-partition',
            cirrus_indexes_without_content_table + '/snapshot={{ ds_nodash }}'
        ]
    ) >> EmptyOperator(task_id='complete')