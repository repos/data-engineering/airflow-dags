# Test dag used as an input for the Airflow metrics docker compose example.

import time

import pendulum
from airflow import DAG
from airflow.decorators import task

with DAG(
    dag_id="simple_dag_python_operator",
    schedule="@monthly",
    start_date=pendulum.datetime(2023, 1, 1, tz="UTC"),
    catchup=False,
    tags=["metrics-example"],
) as dag:

    @task()
    def print_array():
        time.sleep(10)
        a = [1, 2, 3, 4]
        print(a)
        return a

    print_array()
