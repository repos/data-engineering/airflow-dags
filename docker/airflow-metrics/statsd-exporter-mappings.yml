# Defines common statsd_exporter mappings for Airflow instances.
# Those mappings rewrite the metrics to be more Prometheus friendly (e.g. adding labels,...).

defaults:
  # without TTL, the metrics stays the same each time Prometheus scraps the statsd_exporter.
  #  ttl: 2h
  timer_type: summary
  buckets:
    - 0.005
    - 0.01
    - 0.025
    - 0.05
    - 0.1
    - 0.25
    - 0.5
    - 1
    - 2.5
    - 5
    - 10
  quantiles:
    - quantile: 0.99
      error: 0.001
    - quantile: 0.95
      error: 0.001
    - quantile: 0.75
      error: 0.001
    - quantile: 0.5
      error: 0.005
  labels:
    site: analytics_test

mappings:

  - match: airflow.scheduler.scheduler_loop_duration
    name: airflow_scheduler_loop_duration
    summary_options:
      quantiles:
        - quantile: 0.5
          error: 0.1
      max_age: 30s
      age_buckets: 30
      buf_cap: 10
    labels:
      site: analytics_test

  # Example:
  #   airflow.operator_failures__HdfsEmailOperator count
  #   => airflow_operator{operator="HdfsEmailOperator", state="failures"} count
  - match: airflow\.operator_(failures|successes)__(\w+)
    match_type: regex
    name: airflow_operator
    labels:
      state: "$1"
      operator: "$2"

  # Example:
  #   airflow.ti_failures count
  #   => airflow_ti{state="failures"} count
  - match: airflow\.ti_(failures|successes)
    match_type: regex
    name: airflow_ti
    labels:
      state: "$1"

  # Example:
  #   airflow.ti.start.example_python_operator.print_array count
  #   dropped
  - match: airflow\.ti\.start.*
    match_type: regex
    name: airflow_ti_start
    action: drop

  # Example:
  #   airflow.ti.finish.example_dag.task_1.failed count
  #   => airflow_ti_finish{dag_id="example_dag", task_id="task1", state="failed"} count
  - match: airflow\.ti\.finish.(\w+)\.(\w+)\.(queued|running|scheduled|success|failed)
    match_type: regex
    name: airflow_ti_finish
    labels:
      dag_id: "$1"
      task_id: "$2"
      state: "$3"

  # Example:
  #   airflow.ti.finished.example_python_operator.print_array.None count
  #   dropped
  - match: airflow\.ti\.finish\.(\w+)\.(\w+)\.(None|deferred|removed|restarting|shutdown|skipped|up_for_reschedule|up_for_retry|upstream_failed)
    match_type: regex
    name: airflow_ti_finish_useless
    action: drop

  # Example:
  #   airflow.dag.pageview_hourly.move_data_to_archive.duration
  #   => airflow_dag_duration{dag_id="pageview_hourly", task_id="move_data_to_archive"} count
  - match: airflow\.dag\.(\w+)\.(\w+)\.duration
    match_type: regex
    name: "airflow_task_duration"
    labels:
      dag_id: "$1"
      task_id: "$2"

  # Example:
  #   airflow.dag.pageview_hourly.move_data_to_archive.duration
  #   dropped
  - match: airflow\.dag\.(\w+)\.(\w+)\.(queued_duration|scheduled_duration)
    match_type: regex
    name: airflow_dag_other_durations
    action: drop

  # Example:
  #   airflow.dagrun.duration.pageview_hourly count
  #   dropped
  - match: airflow\.dagrun\.duration\.(success|failed)$
    match_type: regex
    name: airflow_dagrun_duration_success
    action: drop

  # Example:
  #   airflow.dagrun.duration.success.pageview_hourly count
  #   => airflow_dagrun_duration{dag_id="pageview_hourly", state="success"} count
  - match: airflow\.dagrun\.duration\.(success|failed)\.(\w+)
    match_type: regex
    name: airflow_dagrun_duration
    labels:
      state: "$1"
      dag_id: "$2"
