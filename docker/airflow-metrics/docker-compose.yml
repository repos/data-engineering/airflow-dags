# Describes docker-compose file for Airflow with Prometheus, Grafana and StatsD-Exporter

# Docker-compose cfg version
version: '3'

# Common part for all Airflow services
x-airflow-common:
  &airflow-common
  image: apache/airflow:2.10.2
  environment:
    - POSTGRES_USER=airflow
    - POSTGRES_PASSWORD=airflow
    - POSTGRES_DB=airflow
    - AIRFLOW_HOME=/opt/airflow
    - PYTHONPATH=/opt/airflow
    - AIRFLOW__CORE__LOAD_EXAMPLES=True
  volumes:
    - ./dags:/opt/airflow/dags
    - ./airflow.cfg:/opt/airflow/airflow.cfg
    - ./webserver_config.py:/opt/airflow/webserver_config.py
    - ./../../wmf_airflow_common/metrics/custom_statsd_client.py:/opt/airflow/custom_statsd_client.py

services:

  airflow-init:
    << : *airflow-common
    container_name: airflow-metrics-init
    depends_on:
      - postgres
    command: airflow db init
    profiles:
      - init

  postgres:
    container_name: airflow-metrics-postgres
    image: postgres:11
    environment:
      - POSTGRES_USER=airflow
      - POSTGRES_PASSWORD=airflow
      - POSTGRES_DB=airflow
    profiles:
      - init
      - default

  airflow-webserver:
    << : *airflow-common
    container_name: airflow-metrics-webserver
    restart: always
    depends_on:
      - postgres
      - statsd-exporter
    ports:
      - "8080:8080"
    command: airflow webserver
    healthcheck:
      test: ["CMD-SHELL", "[ -f /opt/airflow/airflow-webserver.pid ]"]
      interval: 30s
      timeout: 30s
      retries: 3
    profiles:
      - default

  airflow-scheduler:
    << : *airflow-common
    container_name: airflow-metrics-scheduler
    depends_on:
      - postgres
      - statsd-exporter
    command: airflow scheduler
    profiles:
      - default

  statsd-exporter:
    image: prom/statsd-exporter
    container_name: airflow-metrics-statsd-exporter
    command: "--statsd.listen-udp=:8125 --web.listen-address=:9102 --statsd.mapping-config=/opt/statsd_mappings.yml --log.level=debug"
    environment:
      - STATSD.MAPPING-CONFIG=/opt/statsd_mappings.yml
    ports:
      - 9123:9102
      - 8125:8125/udp
    volumes:
      - ./statsd-exporter-mappings.yml:/opt/statsd_mappings.yml
    profiles:
      - default

  prometheus:
    image: prom/prometheus:v2.48.0
    container_name: airflow-metrics-prometheus
    user: "0"
    ports:
      - 9090:9090
    volumes:
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - ./prometheus/volume:/prometheus
    profiles:
      - default

  grafana:
    image: grafana/grafana:9.4.14
    container_name: airflow-metrics-grafana
    environment:
      GF_PATHS_PROVISIONING: /grafana/provisioning
      GF_SECURITY_ADMIN_USER: usr
      GF_SECURITY_ADMIN_PASSWORD: pwd
    ports:
      - 3000:3000
    volumes:
      - ./grafana/volume/data:/grafana
      - ./grafana/volume/dashboards:/grafana/dashboards
      - ./grafana/volume/datasources:/grafana/datasources
      - ./grafana/volume/provisioning:/grafana/provisioning
    profiles:
      - default
