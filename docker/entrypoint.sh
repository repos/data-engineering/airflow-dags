#!/bin/bash

set -o xtrace

# if run as root, assume we are invoked from a linux command line that mounted
# the working directory from the host and wants to adjust the somebody user to
# match that uid/gid to make permissions happy.
if [ "$(id -u)" -eq 0 ]; then
    echo "Updating system to use external uid/gid of $(stat --format=%u:%g .)" 
    usermod -u "$(stat --format=%u .)" somebody
    groupmod -g "$(stat --format=%g .)" somebody
    # On some systems this dir is owned by root:root, even though the contents are
    # owned by somebody:somebody. Make them align.
    chown somebody:somebody .
    echo "Running pipeline"
    gosu somebody:somebody "$0" "$@"
    exit $?
else
    exec "$@"
fi
