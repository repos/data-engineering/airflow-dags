import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "browser", "browser_general_daily_dag.py"]


def test_browser_general_daily_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="browser_general_daily")
    assert dag is not None
    assert len(dag.tasks) == 4
