import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "mediacounts", "mediacounts_archive_daily_dag.py"]


def test_mediacounts_archive_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="mediacounts_archive_daily")
    assert dag is not None
    assert len(dag.tasks) == 3

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False
