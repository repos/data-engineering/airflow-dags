import os
from copy import deepcopy

import pytest
from airflow.models import DagRun, MappedOperator, TaskInstance
from airflow.utils.session import provide_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "canary_events", "canary_events_dag.py"]


@provide_session
def test_canary_events_hourly_dag_loaded(dagbag, mocker, compare_with_fixture, session=None):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag_id = "canary_events"
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == 2

    # Mock the dataset configurations input
    stream_configs_fixture = os.path.join(os.path.dirname(__file__), "stream_configs_example.json")
    with open(stream_configs_fixture, "r") as file:
        mock_stream_configs_call(file.read(), mocker)

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(minutes=33))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test data interval
    execution_dates_iter = dag.iter_dagrun_infos_between(None, dag.start_date.add(years=1))
    data_interval = next(execution_dates_iter).data_interval
    assert data_interval.start.strftime("%Y-%m-%dT%H:%M:%S") == "2024-05-21T12:02:00"
    assert data_interval.end.strftime("%Y-%m-%dT%H:%M:%S") == "2024-05-21T12:35:00"

    # Test the task in charge of getting the configuration
    fetch_stream_configs_ti = create_and_run_task("fetch_streams_configurations", dagrun)
    result: dict = fetch_stream_configs_ti.xcom_pull(task_ids=fetch_stream_configs_ti.task.task_id)
    assert len(result) == 2
    assert fetch_stream_configs_ti.pool == "canary_events"

    # Test the first task running the canary event
    produce_canary_event: MappedOperator = dagrun.dag.get_task("produce_canary_event")
    # Get the unmapped task
    #   In order to render templates and get the final spec, we need to do it on the unmapped task.
    tasks, _ = produce_canary_event.expand_mapped_task(run_id=dagrun.run_id, session=session)
    produce_canary_event_ti = tasks[0]
    produce_canary_event_ti.task = deepcopy(produce_canary_event)  # The task is not set in the TaskInstance constructor
    context = produce_canary_event_ti.get_template_context()
    unmapped_task = produce_canary_event.unmap((context, session))
    produce_canary_event_ti.task = unmapped_task
    # Build the target instance from the unmapped task and create the fixture
    produce_canary_event_ti.render_templates(context=context)
    skein_hook = unmapped_task.make_hook()
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="skein_operator_spec", fixture_id=f"{dag_id}.produce_canary_event", **kwargs)

    # Test that the rendered map_index which should be the stream name.
    # As the task can't really be executed (Spark application), and as map index is a jinja template, templated
    # at execution time, we manually render the template.
    rendered_map_index = (
        produce_canary_event_ti.task.get_template_env()
        .from_string(produce_canary_event_ti.task.map_index_template)
        .render(context)
    )
    assert rendered_map_index == "eventlogging_CentralNoticeBannerHistory"


@provide_session
def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None, session=None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        session.add(task_instance)
        session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


def mock_stream_configs_call(stream_configs_json, mocker):
    mocker.patch(
        "analytics.dags.canary_events.canary_events_dag.fsspec.open",
        mocker.mock_open(read_data=stream_configs_json),
    )
