import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "pingback", "pingback_report_weekly_dag.py"]


def test_pingback_report_weekly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="pingback_report_weekly_v2")
    assert dag is not None
    assert len(dag.tasks) == 55
