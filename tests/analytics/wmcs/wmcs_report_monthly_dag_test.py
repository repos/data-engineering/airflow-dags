import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "wmcs", "wmcs_report_monthly_dag.py"]


def test_wmcs_report_monthly_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wmcs_report_monthly")
    assert dag is not None
    assert len(dag.tasks) == 9
