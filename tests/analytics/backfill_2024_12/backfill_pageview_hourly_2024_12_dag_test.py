import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "backfill_2024_12", "backfill_pageview_hourly_2024_12_dag.py"]


def test_backfill_pageview_hourly_2024_12_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="backfill_pageview_hourly_2024_12")
    assert dag is not None
    assert len(dag.tasks) == 3
