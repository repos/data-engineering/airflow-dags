import pytest


@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "backfill_2024_12", "backfill_cassandra_load_pageview_per_article_2024_12_dag.py"]


def test_backfill_cassandra_load_pageview_per_article_2024_12_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="backfill_cassandra_load_pageview_per_article_daily_2024_12")
    assert dag is not None
    assert len(dag.tasks) == 1
