import pytest


@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "backfill_2024_12", "backfill_webrequest_actor_metrics_rollup_hourly_2024_12_dag.py"]


def test_backfill_webrequest_actor_metrics_rollup_rollup_hourly_2024_12_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="backfill_webrequest_actor_metrics_rollup_hourly_2024_12")
    assert dag is not None
    assert len(dag.tasks) == 1
    # Testing interval_start is 24 hours before interval_end
    query_parameters = dag.tasks[0]._query_parameters
    assert query_parameters["interval_start_year"] == "{{ (data_interval_start.add(hours=-23)).year }}"
    assert query_parameters["interval_start_month"] == "{{ (data_interval_start.add(hours=-23)).month }}"
    assert query_parameters["interval_start_day"] == "{{ (data_interval_start.add(hours=-23)).day }}"
    assert query_parameters["interval_start_hour"] == "{{ (data_interval_start.add(hours=-23)).hour }}"
