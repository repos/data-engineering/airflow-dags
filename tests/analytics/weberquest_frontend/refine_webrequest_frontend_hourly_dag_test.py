from copy import deepcopy

import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "webrequest_frontend", "refine_webrequest_frontend_hourly_dag.py"]


@pytest.fixture(name="dag")
def fixture_dag(dagbag):
    assert dagbag.import_errors == {}
    # Check the existence of the other dag even if we aren't creating
    assert dagbag.get_dag(dag_id="refine_webrequest_frontend_hourly_upload") is not None
    return dagbag.get_dag(dag_id="refine_webrequest_frontend_hourly_text")


def test_refine_webrequest_hourly_2_dags_loaded(dagbag):
    assert dagbag.import_errors == {}
    assert dagbag.size() == 2
    for webrequest_source in ["text", "upload"]:
        assert dagbag.get_dag(dag_id=f"refine_webrequest_frontend_hourly_{webrequest_source}") is not None


def test_refine_webrequest_hourly_dag_loaded(dag):
    assert dag is not None
    assert len(dag.tasks) == 13


def test_refine_webrequest_hourly_sensor(dag, render_task):
    sensor = deepcopy(dag.get_task("wait_for_gobblin_export"))
    rendered_sensor = render_task(sensor)
    assert (
        rendered_sensor._url
        == "hdfs://analytics-hadoop/wmf/data/raw/webrequest_frontend/webrequest_frontend_text/year=2024/month=11/day=26/hour=01/_IMPORTED"  # noqa: E501
    )
    assert rendered_sensor.poke_interval == 30


@pytest.mark.skip(reason="codepath is disabled by default")
def test_refine_webrequest_hourly_report_fork(dag, render_task):
    sensor = deepcopy(dag.get_task("data_loss_errors_hdfs_report_fork"))
    rendered_fork = render_task(sensor)
    assert rendered_fork.op_kwargs == {
        "next_tasks_with_report": ["data_loss_errors_email"],
        "next_tasks_without_report": ["extract_data_loss_warnings"],
        "report_dir_path": "hdfs://analytics-hadoop/user/analytics/staging/data/raw/webrequests_data_loss/text/2023/4/11/15/ERROR",  # noqa: E501
    }


@pytest.mark.skip(reason="codepath is disabled by default")
def test_refine_webrequest_hourly_error_email(dag, render_task):
    sensor = deepcopy(dag.get_task("data_loss_errors_email"))
    rendered_email = render_task(sensor)
    assert rendered_email.to == "data-engineering-alerts@lists.wikimedia.org"
    assert rendered_email.subject == "Data Loss ERROR - Airflow Analytics refine_webrequest_hourly_text 2023-04-11"
    assert rendered_email.html_content == (
        "The following data losses have been detected:<br/>"
        "Job: refine_webrequest_hourly_text<br/>"
        "Date: 2023-04-11T15<br/>"
        "Error threshold: 2.0%<br/>"
        "Report content:<br/>"
    )
