from datetime import datetime

import pytest
from task_sensor_instancer_helper import create_task_instance


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "druid_load", "druid_load_unique_devices_per_domain_daily_dag.py"]


def test_druid_load_unique_devices_per_domain_daily_dag_loaded(dagbag, mocker):
    # Test the daily DAG
    assert dagbag.import_errors == {}
    daily_dag = dagbag.get_dag(dag_id="druid_load_unique_devices_per_domain_daily")
    assert daily_dag is not None
    assert len(daily_dag.tasks) == 5

    # Test the monthly DAG
    monthly_dag = dagbag.get_dag(dag_id="druid_load_unique_devices_per_domain_daily_aggregated_monthly")
    assert monthly_dag is not None
    assert len(monthly_dag.tasks) == 5

    # Test the task sensor of the monthly DAG
    task_sensor = monthly_dag.tasks[0]
    assert task_sensor.task_id == "wait_for_unique_devices_per_domain_daily_over_a_month_iceberg"
    task_instance = create_task_instance(task_sensor, mocker, execution_date=datetime(2023, 4, 1))
    execution_dates = task_sensor._get_dttm_filter(task_instance.get_template_context())
    assert len(execution_dates) == 30  # April has 30 days
    assert execution_dates[0].strftime("%Y-%m-%d-%H:%M") == "2023-04-01-00:00"  # First task run to look for
    assert execution_dates[-1].strftime("%Y-%m-%d-%H:%M") == "2023-04-30-00:00"  # And the last
