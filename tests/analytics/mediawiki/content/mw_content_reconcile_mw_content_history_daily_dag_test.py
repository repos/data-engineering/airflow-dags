import os

import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType
from mw_content_testing_helper_methods import (
    create_and_run_task,
    mock_fetch_dblist_call,
)


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "mediawiki", "content", "mw_content_reconcile_mw_content_history_daily.py"]


def test_dumps_reconcile_wikitext_raw_daily_dag(dagbag, mocker, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="mw_content_reconcile_mw_content_history_daily")
    assert dag is not None
    assert len(dag.tasks) == 6

    # Mock the output of fetch_dblist()
    base_path = os.path.dirname(__file__)
    dblist_include_fixture_path = os.path.join(base_path, "dblist_include_fixture.txt")
    dblist_exclude_fixture_path = os.path.join(base_path, "dblist_exclude_fixture.txt")
    with open(dblist_include_fixture_path, "r") as file:
        dblist_include_fixture_content = file.read()

    with open(dblist_exclude_fixture_path, "r") as file:
        dblist_exclude_fixture_content = file.read()

    mock_fetch_dblist_call(
        mocker,
        [
            dblist_include_fixture_content,
            dblist_exclude_fixture_content,
        ],
    )

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(days=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test the task in charge of getting the dblist
    calculate_effective_dblist_ti = create_and_run_task("calculate_effective_dblist", dagrun)
    result = calculate_effective_dblist_ti.xcom_pull(task_ids=calculate_effective_dblist_ti.task.task_id)
    assert len(result) == 3
    assert result == ["abwiki", "acewiki", "zuwiki"]

    # Test the spark_consistency_check task
    spark_consistency_check = dag.get_task("reconcile.spark_consistency_check")
    spark_consistency_check_ti = TaskInstance(spark_consistency_check, run_id=dagrun.run_id, map_index=0)
    spark_consistency_check_ti.dag_run = dagrun
    context = spark_consistency_check_ti.get_template_context()
    spark_consistency_check_ti.render_templates(context=context)
    skein_hook = spark_consistency_check._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    spark_consistency_check_fixture_id = (
        "analytics_dags_mediawiki_content_mw_content_reconcile_mw_content_history_daily_dag.py"
        "-reconcile_spark_consistency_check"
    )
    compare_with_fixture(group="spark_skein_specs", fixture_id=spark_consistency_check_fixture_id, **kwargs)

    # Test the spark_emit_reconcile_events_to_kafka task
    spark_emit_reconcile_events_to_kafka = dag.get_task("reconcile.spark_emit_reconcile_events_to_kafka")
    spark_emit_reconcile_events_to_kafka_ti = TaskInstance(
        spark_emit_reconcile_events_to_kafka, run_id=dagrun.run_id, map_index=0
    )
    spark_emit_reconcile_events_to_kafka_ti.dag_run = dagrun
    context = spark_emit_reconcile_events_to_kafka_ti.get_template_context()
    spark_emit_reconcile_events_to_kafka_ti.render_templates(context=context)
    skein_hook = spark_emit_reconcile_events_to_kafka._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    spark_emit_reconcile_events_to_kafka_fixture_id = (
        "analytics_dags_mediawiki_content_mw_content_reconcile_mw_content_history_daily_dag.py"
        "-reconcile_spark_emit_reconcile_events_to_kafka"
    )
    compare_with_fixture(
        group="spark_skein_specs", fixture_id=spark_emit_reconcile_events_to_kafka_fixture_id, **kwargs
    )
