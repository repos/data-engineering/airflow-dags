from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


def mock_fetch_dblist_call(mocker, content_list):
    mock_one = mocker.Mock()
    mock_one.status_code = 200
    mock_one.text = content_list[0]

    mock_two = mocker.Mock()
    mock_two.status_code = 200
    mock_two.text = content_list[1]

    mock_get = mocker.patch("analytics.dags.mediawiki.content.mw_content_utils.requests.get")
    mock_get.side_effect = [mock_one, mock_two]
