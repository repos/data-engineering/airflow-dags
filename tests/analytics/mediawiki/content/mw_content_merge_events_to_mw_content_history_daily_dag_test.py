import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "mediawiki", "content", "mw_content_merge_events_to_mw_content_history_daily.py"]


def test_page_content_change_to_wikitext_raw_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="mw_content_merge_events_to_mw_content_history_daily")
    assert dag is not None
    assert len(dag.tasks) == 6
