import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.session import provide_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_test", "dags", "refine", "refine_to_hive_hourly_dag.py"]


def test_refine_to_hive_hourly_analytics_test_dag_loaded(dagbag, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag_id = "refine_to_hive_hourly_test"
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == 6  # The task groups are not counted as tasks.

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(hours=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    raw_base_path = "hdfs://analytics-test-hadoop/wmf/data/raw/event"

    # Test the task in charge of getting the configuration
    fetch_datasets_configurations_ti = create_and_run_task("fetch_stream_configurations", dagrun)
    configurations = fetch_datasets_configurations_ti.xcom_pull(task_ids=fetch_datasets_configurations_ti.task.task_id)
    assert len(configurations) == 3
    config = next((c for c in configurations if c["stream"] == "eventgate-analytics.test.event"), None)
    assert config == {
        "diff_hive_table": "event.eventgate_analytics_test_event",
        "hdfs_source_path_flags": (
            f"{raw_base_path}/eqiad.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17/_IMPORTED,"
            f"{raw_base_path}/codfw.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17/_IMPORTED"
        ),
        "hdfs_source_paths": (
            "/wmf/data/raw/event/eqiad.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17,"
            "/wmf/data/raw/event/codfw.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17"
        ),
        "hive_partition_columns": "datacenter:STRING,year:LONG,month:LONG,day:LONG,hour:LONG",
        "hive_partition_paths": (
            "datacenter=eqiad/year=2024/month=8/day=8/hour=17," "datacenter=codfw/year=2024/month=8/day=8/hour=17"
        ),
        "hive_pre_partitions": ["datacenter=eqiad", "datacenter=codfw"],
        "hive_table": "event_alt.eventgate_analytics_test_event",
        "ignore_missing_input_paths": False,
        "legacy_hive_partition_paths": (
            "event.eventgate_analytics_test_event/datacenter=eqiad/year=2024/month=8/day=8/hour=17,"
            "event.eventgate_analytics_test_event/datacenter=codfw/year=2024/month=8/day=8/hour=17"
        ),
        "schema_uri": "/test/event/latest",
        "spark_refine_job_scale": "medium",
        "spark_refine_job_scale_params": {
            "driver_cores": 1,
            "driver_memory": "4G",
            "executor_cores": 4,
            "executor_memory": "8G",
            "master": "yarn",
            "max_executors": 4,
            "spark.executor.memoryOverhead": "800MB",
        },
        "stream": "eventgate-analytics.test.event",
        "table_format": "hive",
        "table_location": "hdfs://analytics-test-hadoop/user/analytics/event_alt/eventgate_analytics_test_event",
        "transform_functions": (
            "org.wikimedia.analytics.refinery.job.refine.remove_canary_events,"
            "org.wikimedia.analytics.refinery.job.refine.deduplicate,"
            "org.wikimedia.analytics.refinery.job.refine.geocode_ip,"
            "org.wikimedia.analytics.refinery.job.refine.parse_user_agent,"
            "org.wikimedia.analytics.refinery.job.refine.add_is_wmf_domain,"
            "org.wikimedia.analytics.refinery.job.refine.add_normalized_host,"
            "org.wikimedia.analytics.refinery.job.refine.normalizeFieldNamesAndWidenTypes"
        ),
        "wait_for_gobblin_export_soft_fail": True,
    }

    # Test the sensor
    wait_for_gobblin_export_ti = create_and_run_pre_execute(
        "refine_hive_dataset.wait_for_gobblin_export", dagrun, map_index=2
    )
    assert wait_for_gobblin_export_ti.task._url == (
        f"{raw_base_path}/eqiad.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17/_IMPORTED,"
        f"{raw_base_path}/codfw.eventgate-analytics.test.event/year=2024/month=08/day=08/hour=17/_IMPORTED"
    )
    assert wait_for_gobblin_export_ti.task.soft_fail is False

    # Test the task which call the evolve table job
    evolve_table = dag.get_task("refine_hive_dataset.evolve_hive_table")
    evolve_table_ti = TaskInstance(evolve_table, run_id=dagrun.run_id, map_index=2)
    evolve_table_ti.dag_run = dagrun
    context = evolve_table_ti.get_template_context()
    evolve_table_ti.render_templates(context=context)
    skein_hook = evolve_table._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="skein_operator_spec", fixture_id=f"{dag_id}.evolve_hive_table", **kwargs)

    # Test the task in charge of actually refining the dataset
    refine_hourly = dag.get_task("refine_hive_dataset.refine_to_hive_hourly")
    refine_hourly_ti = TaskInstance(refine_hourly, run_id=dagrun.run_id, map_index=2)
    refine_hourly_ti.dag_run = dagrun
    context = refine_hourly_ti.get_template_context()
    refine_hourly_ti.render_templates(context=context)
    skein_hook = refine_hourly._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id=f"{dag_id}.refine_to_hive_hourly", **kwargs)

    # Test legacy refine table sensor
    wait_for_legacy_refine_table = create_and_run_pre_execute(
        "refine_hive_dataset.wait_for_legacy_refine_table", dagrun, map_index=2
    )
    hive_partition_ending = "year=2024/month=8/day=8/hour=17"
    assert wait_for_legacy_refine_table.task.partition_names == [
        f"event.eventgate_analytics_test_event/datacenter=eqiad/{hive_partition_ending}",
        f"event.eventgate_analytics_test_event/datacenter=codfw/{hive_partition_ending}",
    ]

    # test diff_check
    diff_check = dag.get_task("refine_hive_dataset.diff_check")
    diff_check_ti = TaskInstance(diff_check, run_id=dagrun.run_id, map_index=2)
    diff_check_ti.dag_run = dagrun
    context = diff_check_ti.get_template_context()
    diff_check_ti.render_templates(context=context)
    skein_hook = diff_check._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    compare_with_fixture(group="spark_skein_specs", fixture_id=f"{dag_id}.diff_check", **kwargs)


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task_instance = create_task_instance(task_id, dag_run, map_index)
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


@provide_session
def create_task_instance(task_id: str, dag_run: DagRun, map_index: int | None = None, session=None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        session.add(task_instance)
        session.commit()
    return task_instance


def create_and_run_pre_execute(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task_instance = create_task_instance(task_id, dag_run, map_index)
    task_instance.task.pre_execute(task_instance.get_template_context())
    return task_instance
