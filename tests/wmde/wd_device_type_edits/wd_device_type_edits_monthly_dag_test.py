"""
Tests for wd_device_type_edits_monthly_dag.
"""

import pytest


# This fixture defines the dag_path for the shared dagbag one.
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["wmde", "dags", "wd_device_type_edits", "wd_device_type_edits_monthly_dag.py"]


def test_wd_device_type_edits_monthly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wd_device_type_edits_monthly")
    assert dag is not None
    assert len(dag.tasks) == 4  # without CSV export
    # assert len(dag.tasks) == 6  # with CSV export
