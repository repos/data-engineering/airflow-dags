import pytest

from platform_eng.dags.cassandra_dag import build_cassandra_insert_query


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'cassandra_dag.py']


def test_cassandra_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="cassandra")
    assert dag is not None
    assert len(dag.tasks) == 6


def test_build_cassandra_insert_query():
    expected = "INSERT INTO aqs.image_suggestions.ct SELECT /* + COALESCE(6) */ a,b,c FROM hdb.ht WHERE snapshot='2025-01-16'"  # noqa: E501
    actual = build_cassandra_insert_query(
        cassandra_table="ct",
        coalesce=6,
        hive_columns="a,b,c",
        hive_db="hdb",
        hive_table="ht",
        snapshot="2025-01-16"
    )

    assert expected == actual
