#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'wikidata_and_lead_images_dag.py']


def test_slis_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wikidata_and_lead_images")
    assert dag is not None
    assert len(dag.tasks) == 8
