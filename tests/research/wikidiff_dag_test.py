from unittest.mock import patch

import pytest
from airflow.models import DagBag, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState

from research.config.wikis import WIKIS


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath() -> list[str]:
    return ["research", "dags", "wikidiff_dag.py"]


def test_wikidiff_pipeline_dag_loaded(dagbag: DagBag) -> None:
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wikidiff")
    assert dag
    assert len(dag.tasks) == 5


def test_wikidiff_pipeline_mapped_tasks(dagbag: DagBag) -> None:
    dag = dagbag.get_dag(dag_id="wikidiff")
    assert dag

    with (
        create_session() as session,
        patch("research.dags.wikidiff_dag.fsspec.filesystem") as mocked_fsspec,
    ):
        dag_run = dag.create_dagrun(
            run_id="test",
            session=session,
            state=DagRunState.RUNNING,
            start_date=dag.start_date,
            execution_date=dag.start_date,
        )
        assert dag

        # Mock filesystem's `ls` method which gets invoked inside `create_batch`
        mocked_filesystem = mocked_fsspec.return_value
        mocked_filesystem.ls.return_value = [{"size": 600 * 1024 * 1024 * 1024}]

        # Check that the `create_batch` task creates batches correctly
        create_batch_task_instance = TaskInstance(
            task=dag.get_task(task_id="create_batches"),
            run_id=dag_run.run_id,
        )
        create_batch_task_instance.run(
            ignore_all_deps=True,
            ignore_task_deps=True,
            ignore_ti_state=True,
            test_mode=True,
        )

        batches = create_batch_task_instance.xcom_pull(create_batch_task_instance.task_id)
        expected = [
            [
                "wikidiff.pipeline.runwikis",
                f'{{"hive_table_dir": "/tmp/research/wikidiff", "snapshot": "2023-12", "wikis": ["{wiki}"], "num_files": [2458]}}',
            ]
            for wiki in WIKIS
        ]
        assert batches == expected

        # Check that `wikidiff_batcher` expands into the correct number of tasks
        wikidiff_batcher_task = dag.get_task(task_id="wikidiff_batcher")
        expanded_tasks, _ = wikidiff_batcher_task.expand_mapped_task(dag_run.run_id, session=session)
        assert len(expanded_tasks) == len(batches)
