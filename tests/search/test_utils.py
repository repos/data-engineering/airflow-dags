import pytest
from search.shared.utils import parse_memory_to_mb


@pytest.mark.parametrize('expected,value', [
    (20, '20m'),
    (20, '20M'),
    (2**10, '1g'),
    (4 * 2**10, '4g'),
    (2 * 2**20, '2t'),
])
def test_parse_memory_to_mb(expected, value):
    assert expected == parse_memory_to_mb(value)
