import pytest
from search.shared.auto_size_spark import \
    AutoSizeForMatrix, AutoSizeSparkSubmitOperator
from search.shared.utils import parse_memory_to_mb


def test_matrix_sizing(fixture_dir):
    matrix = AutoSizeForMatrix(
        metadata_uri=f'file://{fixture_dir}/auto_size/metadata.json',
        bytes_per_value=50,
        num_obs_path='metadata.num_obs',
        features_path='metadata.features')

    dims = matrix.extract_dimensions(matrix.read_metadata())
    assert dims == (1234, 5)
    # not a super useful test, but at least it runs the function
    assert matrix.calc_overhead((1e7, 10)) == 5245


@pytest.mark.parametrize('executor_cores', [1, 4])
@pytest.mark.parametrize('agg_memory_limit', ['10g', '500g', '1t'])
@pytest.mark.parametrize('agg_cores_limit', [50, 100, 500])
@pytest.mark.parametrize('memory_overhead', [None, '512m'])
def test_respects_limits(executor_cores, agg_memory_limit, agg_cores_limit, memory_overhead):
    conf = {}
    if memory_overhead is not None:
        conf['spark.executor.memoryOverhead'] = memory_overhead
    op = AutoSizeSparkSubmitOperator(
        task_id='...',
        conf=conf,
        executor_cores=executor_cores,
        executor_memory=f'{executor_cores*2}g',
        agg_memory_limit=agg_memory_limit,
        agg_cores_limit=agg_cores_limit,
    )


    op._get_auto_sizer().apply(op)

    num_executors = op.conf['spark.dynamicAllocation.maxExecutors']
    agg_cores = num_executors* op._executor_cores
    assert agg_cores <= agg_cores_limit

    executor_memory = parse_memory_to_mb(op._executor_memory) + \
            parse_memory_to_mb(op.conf['spark.executor.memoryOverhead'])
    agg_memory = num_executors * executor_memory
    assert agg_memory <= parse_memory_to_mb(agg_memory_limit)
