from datetime import timedelta

import pendulum
import pytest
from airflow.timetables.base import DagRunInfo, DataInterval, TimeRestriction
from pendulum import UTC, DateTime

from wmf_airflow_common.plugins.delayed_timetables import DelayedHourlyTimetable


@pytest.mark.parametrize("delay", [timedelta(hours=1), timedelta(hours=2)])
def test_schedules_dag_runs_on_hourly_schedule_with_delay(delay):
    timetable = DelayedHourlyTimetable(delay)
    assert timetable.schedule_compatibility() == "@hourly"


@pytest.mark.parametrize("delay", [timedelta(hours=1), timedelta(hours=2)])
def test_infers_manual_data_interval_correctly(delay):
    timetable = DelayedHourlyTimetable(delay)
    run_after = DateTime(2023, 10, 10, 10, 30, tzinfo=UTC)
    data_interval = timetable.infer_manual_data_interval(run_after)
    assert data_interval.start == (run_after - delay).replace(
        minute=0, second=0, microsecond=0, tzinfo=UTC
    ) - timedelta(hours=1)
    assert data_interval.end == (run_after - delay).replace(minute=0, second=0, microsecond=0, tzinfo=UTC)


@pytest.mark.parametrize("delay", [timedelta(hours=1), timedelta(hours=2)])
def test_calculates_next_dagrun_info_correctly(delay):
    timetable = DelayedHourlyTimetable(delay)
    last_data_interval = DataInterval(
        DateTime(2023, 10, 10, 9, 0, tzinfo=UTC), DateTime(2023, 10, 10, 10, 0, tzinfo=UTC)
    )
    restriction = TimeRestriction(earliest=DateTime(2023, 10, 10, 0, 0, tzinfo=UTC), latest=None, catchup=True)
    dagrun_info: DagRunInfo = timetable.next_dagrun_info(
        last_automated_data_interval=last_data_interval, restriction=restriction
    )
    assert dagrun_info.data_interval.start == DateTime(2023, 10, 10, 10, 0, tzinfo=UTC)
    assert dagrun_info.data_interval.end == DateTime(2023, 10, 10, 11, 0, tzinfo=UTC)
    assert dagrun_info.run_after == DateTime(2023, 10, 10, 11, 0, tzinfo=UTC) + delay


def test_does_not_schedule_if_no_start_date():
    timetable = DelayedHourlyTimetable(timedelta(hours=1))
    restriction = TimeRestriction(earliest=None, latest=None, catchup=True)
    dagrun_info = timetable.next_dagrun_info(last_automated_data_interval=None, restriction=restriction)
    assert dagrun_info is None


def test_does_not_schedule_if_expired_end_time():
    timetable = DelayedHourlyTimetable(timedelta(hours=1))
    restriction = TimeRestriction(
        earliest=DateTime(2023, 10, 10, 0, 0, tzinfo=UTC),
        latest=DateTime(2023, 10, 10, 10, 0, tzinfo=UTC),
        catchup=True,
    )
    last_data_interval = DataInterval(
        DateTime(2023, 10, 10, 10, 0, tzinfo=UTC), DateTime(2023, 10, 10, 11, 0, tzinfo=UTC)
    )
    dagrun_info = timetable.next_dagrun_info(last_automated_data_interval=last_data_interval, restriction=restriction)
    assert dagrun_info is None


# test when catchup is False, it should find the most recent data interval
def test_finds_most_recent_data_interval_when_no_catchup(mocker):
    timetable = DelayedHourlyTimetable(timedelta(hours=1))
    restriction = TimeRestriction(
        earliest=DateTime(2023, 9, 10, 0, 0, tzinfo=UTC),
        latest=DateTime(2023, 11, 10, 0, 0, tzinfo=UTC),
        catchup=False,
    )
    last_data_interval = DataInterval(
        DateTime(2023, 10, 10, 9, 0, tzinfo=UTC), DateTime(2023, 10, 10, 10, 0, tzinfo=UTC)
    )
    mock_now = pendulum.datetime(2023, 10, 13, 8, 5)
    mocker.patch("pendulum.DateTime.now", return_value=mock_now)
    dagrun_info: DagRunInfo = timetable.next_dagrun_info(
        last_automated_data_interval=last_data_interval, restriction=restriction
    )
    assert dagrun_info.data_interval.start == DateTime(2023, 10, 13, 6, 0, tzinfo=UTC)
    assert dagrun_info.data_interval.end == DateTime(2023, 10, 13, 7, 0, tzinfo=UTC)
    assert dagrun_info.run_after == DateTime(2023, 10, 13, 8, 0, tzinfo=UTC)
