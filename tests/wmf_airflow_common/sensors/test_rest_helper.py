from unittest import mock

import pytest
from airflow_client.client import ApiClient as AirflowApiClient

from wmf_airflow_common.clients.airflow import AirflowApiClientKerberos
from wmf_airflow_common.config.dag_default_args import get as get_dag_default_args
from wmf_airflow_common.easy_dag import EasyDAGFactory
from wmf_airflow_common.sensors.rest_external_task import RestExternalTaskSensor
from wmf_airflow_common.sensors.rest_helper import get_api_client


@pytest.mark.parametrize(
    "env, kwargs, enabled_by_default, client_cls, expected",
    [
        ("wmf", {}, True, AirflowApiClientKerberos, True),
        ("wmf", {"airflow_api_kerberos_enabled": False}, True, AirflowApiClient, False),
        ("_default_", {}, None, AirflowApiClient, False),
        (
            "_default_",
            {"airflow_api_kerberos_enabled": True},
            None,
            AirflowApiClientKerberos,
            True,
        ),
    ],
)
def test_rest_external_task_sensor_with_kerberos_auth_enabled_implicitly(
    env, kwargs, enabled_by_default, client_cls, expected
):
    """
    Test that the airflow_api_kerberos_enabled environment default arugment is passed
    down to the RestExternalTaskSensor constructor, and that any manual override takes
    precedence.

    """
    with (
        mock.patch(
            "wmf_airflow_common.config.dag_default_args.airflow_environment_name",
            return_value=env,
        ),
        mock.patch(
            "wmf_airflow_common.util.airflow_environment_name",
            return_value=env,
        ),
        mock.patch(
            "wmf_airflow_common.clients.airflow.AirflowApiClientKerberos.inject_gssapi_token_in_default_headers"
        ),
    ):
        default_args = get_dag_default_args()
        assert default_args.get("airflow_api_kerberos_enabled") is enabled_by_default
        with EasyDAGFactory(factory_default_args=dict(default_args)).create_easy_dag("test_sensor"):
            sensor = RestExternalTaskSensor(
                task_id="test_dag",
                external_instance_uri="https://airflow-test.test.wmnet:30443",
                external_dag_id="test_task",
                check_existence=True,
                allowed_states=["success"],
                **kwargs,
            )
            assert sensor.airflow_api_kerberos_enabled is expected
            ApiClientClass = get_api_client(
                config=None,
                kerberos_enabled=sensor.airflow_api_kerberos_enabled,
            )
            assert isinstance(ApiClientClass, client_cls)
