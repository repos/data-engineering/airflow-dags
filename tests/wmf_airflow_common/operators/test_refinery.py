from unittest.mock import patch

import pytest
from airflow.operators.bash import BashOperator

import wmf_airflow_common
from wmf_airflow_common.operators.kubernetes import WMFKubernetesPodOperator
from wmf_airflow_common.operators.refinery import docker_image as refinery_docker_image
from wmf_airflow_common.operators.refinery import (
    drop_older_than,
    drop_older_than_script_path,
    drop_older_than_script_pod_path,
)
from wmf_airflow_common.util import airflow_pod_template_path


def test_drop_older_than_no_path():
    # drop_older_than tests for existence of refinery python deps
    # on the host via os.path.file. If the dep is missing (e.g. in CI containers)
    # it would throw a FileNotFoundError (see test_drop_older_than_missing_refinery
    # test case).
    # Patch os.path.file for the lifetime of this method, to make
    # sure that drop_older_than returns valid output.
    # Validate the command for no-path parameters
    with patch("os.path.isfile", return_value=True):
        drop_webrequest_frontend_raw = drop_older_than(
            operator_config={"task_id": "drop_webrequest_frontend_raw"},
            database="wmf_staging_raw",
            tables=["webrequest"],
            checksum="dbc64f19c5acc0c02e01f20fde10f67b",
        )

    assert isinstance(drop_webrequest_frontend_raw, BashOperator)
    assert drop_webrequest_frontend_raw.bash_command == (
        "/usr/bin/env PYTHONPATH=/srv/deployment/analytics/refinery/python "
        "/usr/bin/python3 "
        "/srv/deployment/analytics/refinery/bin/refinery-drop-older"
        "-than --verbose --database=wmf_staging_raw '--tables=^("
        "webrequest)$' --older-than=90 --allowed-interval=3 "
        "--execute=dbc64f19c5acc0c02e01f20fde10f67b"
    )
    assert drop_webrequest_frontend_raw.task_id == "drop_webrequest_frontend_raw"


def test_drop_older_than_with_path_and_skip_trash():
    # Validates the command when HDFS path parameters are set
    with patch("os.path.isfile", return_value=True):
        drop_webrequest_frontend_raw = drop_older_than(
            operator_config={"task_id": "drop_webrequest_frontend_raw"},
            database="wmf_staging_raw",
            tables=["webrequest"],
            base_path="/wmf/data/raw/webrequest_frontend",
            path_format=(
                ".+/year=(?P<year>[0-9]+)(/month=(?P<month>[0-9]+)(/day=(?P<day>[0-9]+)(/hour=(?P<hour>[0-9]+))?)?)?"
            ),
            checksum="dbc64f19c5acc0c02e01f20fde10f67b",
            skip_trash=True,
        )

    assert isinstance(drop_webrequest_frontend_raw, BashOperator)
    assert drop_webrequest_frontend_raw.bash_command == (
        "/usr/bin/env PYTHONPATH=/srv/deployment/analytics/refinery/python "
        "/usr/bin/python3 "
        "/srv/deployment/analytics/refinery/bin/refinery-drop-older"
        "-than --verbose --database=wmf_staging_raw '--tables=^("
        "webrequest)$' --older-than=90 --allowed-interval=3 "
        "--execute=dbc64f19c5acc0c02e01f20fde10f67b "
        "--base-path=/wmf/data/raw/webrequest_frontend "
        "'--path-format=.+/year=(?P<year>[0-9]+)(/month=(?P<month>[0-9]+)"
        "(/day=(?P<day>[0-9]+)(/hour=(?P<hour>[0-9]+))?)?)?' "
        "--skip-trash"
    )
    assert drop_webrequest_frontend_raw.task_id == "drop_webrequest_frontend_raw"


def test_drop_older_than_on_k8s():
    # drop_older_than tests for existence of refinery python deps
    # on the host via os.path.file. If the dep is missing (e.g. in CI containers)
    # it would throw a FileNotFoundError (see test_drop_older_than_missing_refinery
    # test case).
    # Patch os.path.file for the lifetime of this method, to make
    # sure that drop_older_than returns valid output.
    with (
        patch("os.path.isfile", return_value=True),
        patch("wmf_airflow_common.util.is_running_in_kubernetes", return_value=True),
    ):
        # We need to reload wmf_airflow_common to make sure the patched is_running_in_kubernetes()
        # method propagates to submodules.
        import importlib

        importlib.reload(wmf_airflow_common.operators.refinery)
        importlib.reload(wmf_airflow_common.operators.python)

        drop_webrequest_frontend_raw = wmf_airflow_common.operators.refinery.drop_older_than(
            operator_config={"task_id": "drop_webrequest_frontend_raw"},
            database="wmf_staging_raw",
            tables=["webrequest"],
            checksum="dbc64f19c5acc0c02e01f20fde10f67b",
        )

        assert isinstance(drop_webrequest_frontend_raw, WMFKubernetesPodOperator)
        assert drop_webrequest_frontend_raw.image == refinery_docker_image
        assert drop_webrequest_frontend_raw.cmds == [drop_older_than_script_pod_path]
        assert drop_webrequest_frontend_raw.name == "drop-older-than"
        assert drop_webrequest_frontend_raw.pod_template_file == airflow_pod_template_path(
            "kubernetes_pod_operator_hadoop_pod_template"
        )
        assert drop_webrequest_frontend_raw.arguments == [
            "--verbose",
            "--database=wmf_staging_raw",
            "--tables=^(webrequest)$",
            "--older-than=90",
            "--allowed-interval=3",
            "--execute=dbc64f19c5acc0c02e01f20fde10f67b",
        ]

        assert drop_webrequest_frontend_raw.task_id == "drop_webrequest_frontend_raw"


def test_drop_older_than_missing_refinery():
    with (
        patch("os.path.isfile", return_value=False),
        patch("wmf_airflow_common.util.is_running_in_kubernetes", return_value=False),
    ):
        import importlib

        importlib.reload(wmf_airflow_common.operators.refinery)
        importlib.reload(wmf_airflow_common.operators.python)

        with pytest.raises(FileNotFoundError) as exc_info:
            wmf_airflow_common.operators.refinery.drop_older_than(
                operator_config={"task_id": "drop_webrequest_frontend_raw"},
                database="wmf_staging_raw",
                tables=["webrequest"],
                checksum="dbc64f19c5acc0c02e01f20fde10f67b",
            )

        expected_error = (
            f"Script not found at {drop_older_than_script_path}. "
            "Please ensure refinery is properly installed on your target airflow instance,"
            "and the path is correct."
        )
        assert str(exc_info.value) == expected_error
