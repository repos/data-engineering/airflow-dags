from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator


def test_hdfs_archive_operator_forces_zero_retries(render_task):
    archive_default = HDFSArchiveOperator(
        task_id="move_data_to_archive1",
        source_directory="/tmp/a",
        check_done=True,
        archive_file="/not-tmp/a",
        archive_parent_umask="022",
        archive_perms="644",
        hdfs_tools_shaded_jar_path="/some/path",
    )

    archive_override = HDFSArchiveOperator(
        task_id="move_data_to_archive2",
        source_directory="/tmp/a",
        check_done=True,
        archive_file="/not-tmp/a",
        archive_parent_umask="022",
        archive_perms="644",
        hdfs_tools_shaded_jar_path="/some/path",
        retries=10,  # this not allowed; should WARN and force to 0
    )

    assert archive_default.retries == 0
    assert archive_override.retries == 0
