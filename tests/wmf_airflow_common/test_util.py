import warnings

from wmf_airflow_common import util
from wmf_airflow_common.util import warn_experimental


def test_wmf_airflow_instance_name(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is unset so we get dev env variants.
    monkeypatch.delenv("AIRFLOW_INSTANCE_NAME", raising=False)
    assert util.wmf_airflow_instance_name() is None


def test_wmf_airflow_instance_name_with_env_var(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv("AIRFLOW_INSTANCE_NAME", "analytics-test")
    assert util.wmf_airflow_instance_name() == "analytics-test"


def test_airflow_environment_name(monkeypatch):
    monkeypatch.delenv("AIRFLOW_INSTANCE_NAME", raising=False)
    monkeypatch.delenv("AIRFLOW_ENVIRONMENT_NAME", raising=False)
    assert util.airflow_environment_name() is None


def test_airflow_environment_name_with_env_var(monkeypatch):
    monkeypatch.setenv("AIRFLOW_ENVIRONMENT_NAME", "my-env-name")
    assert util.airflow_environment_name() == "my-env-name"


def test_airflow_environment_name_with_instance_name_env_var(monkeypatch):
    monkeypatch.delenv("AIRFLOW_ENVIRONMENT_NAME", raising=False)
    monkeypatch.setenv("AIRFLOW_INSTANCE_NAME", "analytics-test")
    assert util.airflow_environment_name() == "wmf"


def test_is_wmf_airflow_instance(monkeypatch):
    # Make sure AIRFLOW_{ENVIRONMENT,INSTANCE}_NAME is unset so we get dev env variants.
    monkeypatch.delenv("AIRFLOW_ENVIRONMENT_NAME", raising=False)
    monkeypatch.delenv("AIRFLOW_INSTANCE_NAME", raising=False)
    assert util.is_wmf_airflow_instance() is False


def test_is_wmf_airflow_instance_with_env_var(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv("AIRFLOW_INSTANCE_NAME", "analytics-test")
    assert util.is_wmf_airflow_instance() is True


def test_is_relative_uri():
    assert util.is_relative_uri("rel1")
    assert not util.is_relative_uri("/rel1")
    assert not util.is_relative_uri("file:///rel1")


def test_helpers_in_dev_environment(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv("AIRFLOW_INSTANCE_NAME", "airflow-development-analytics-aqu")
    monkeypatch.setenv("AIRFLOW_ENVIRONMENT_NAME", "dev_wmf")
    assert util.is_wmf_airflow_instance() is False
    assert util.airflow_environment_name() == "dev_wmf"


def test_resolve_kwargs_default_args(monkeypatch):
    assert util.resolve_kwargs_default_args({}, "k") is None
    assert util.resolve_kwargs_default_args({"k": "v"}, "k") == "v"
    assert util.resolve_kwargs_default_args({"default_args": {"k": "v"}}, "k") == "v"
    assert util.resolve_kwargs_default_args({"k": "v", "default_args": {"k": "v2"}}, "k") == "v"


def test_dict_add_or_append_string_value():
    # test append with default separator
    t1 = {"a": "1", "b": "2"}
    util.dict_add_or_append_string_value(t1, "a", "3")
    assert t1 == {"a": "1 3", "b": "2"}

    # test append with custom separator
    t2 = {"a": "1", "b": "2"}
    util.dict_add_or_append_string_value(t2, "a", "3", ", ")
    assert t2 == {"a": "1, 3", "b": "2"}

    # test add if not found
    t3 = {"b": "2"}
    util.dict_add_or_append_string_value(t3, "a", "3", ", ")
    assert t3 == {"a": "3", "b": "2"}


def test_one_line():
    s = """
    SELECT *
    FROM t

    """

    assert util.one_line(s) == "SELECT * FROM t"


def test_warn_experimental():
    @warn_experimental("This function is experimental.")
    def experimental_function():
        return 42

    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("always")

        result = experimental_function()

        assert len(w) == 1
        assert issubclass(w[0].category, FutureWarning)
        assert str(w[0].message) == f"{experimental_function.__name__}: This function is experimental."
        assert result == 42


def test_k8s_proof_url(monkeypatch):
    # fmt: off
    non_k8s_test_cases = [
        (
            "https://meta.wikimedia.org/w/api.php?format=json&action=streamconfigs",
            "https://meta.wikimedia.org/w/api.php?format=json&action=streamconfigs",
        ),
        (
            "https://noc.wikimedia.org/conf/dblists/open.dblist",
            "https://noc.wikimedia.org/conf/dblists/open.dblist",
        ),
        (
            "https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags",
            "https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags",
        ),
        (
            "just_a_random_string",
            "just_a_random_string",
        ),
    ]
    # fmt: on

    for test_case, expected in non_k8s_test_cases:
        result = util.k8s_proof_url(test_case)
        assert result == expected

    # Modify the environment so that util.is_running_in_kubernetes returns True
    from pathlib import Path

    monkeypatch.setattr(Path, "exists", lambda x: True)
    monkeypatch.setenv("AIRFLOW_PRODUCTION_TLS_SERVICE_PORT", "whatever")

    # fmt: off
    k8s_test_cases = [
        (
            "https://meta.wikimedia.org/w/api.php?format=json&action=streamconfigs",
            "http://envoy:6501/w/api.php?format=json&action=streamconfigs",
        ),
        (
            "http://meta.wikimedia.org/w/api.php?format=json&action=streamconfigs",
            "http://envoy:6501/w/api.php?format=json&action=streamconfigs",
        ),
        (
            "https://noc.wikimedia.org/conf/dblists/open.dblist",
            "http://envoy:6509/conf/dblists/open.dblist",
        ),
        (
            "https://analytics.wikimedia.org/published/datasets/automoderator/automoderator_config.tsv",
            "http://envoy:6206/published/datasets/automoderator/automoderator_config.tsv",
        ),
        (
            "https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags",
            "https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags",
        ),
        (
            "just_a_random_string",
            "just_a_random_string",
        ),
    ]
    # fmt: on

    for test_case, expected in k8s_test_cases:
        result = util.k8s_proof_url(test_case)
        assert result == expected
