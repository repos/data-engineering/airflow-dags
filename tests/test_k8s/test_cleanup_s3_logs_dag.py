import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["test_k8s", "dags", "cleanup_s3_logs_dag.py"]


def test_dag_structure(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="purge_old_logs_from_s3")
    assert dag is not None
    assert dag.task_count == 1
    assert callable(dag.task_dict["purge_old_logs_from_s3"].python_callable)
    assert "maintenance" in dag.tags
