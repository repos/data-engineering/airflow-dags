import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["test_k8s", "dags", "addition_dag.py"]


def test_seal_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="addition")
    assert dag is not None
    assert len(dag.tasks) == 1
    task_fn = dag.task_dict["add_one_and_two"].python_callable
    assert task_fn() == 3
