[build-system]
requires = [ "setuptools>=61.2",]
build-backend = "setuptools.build_meta"

[project]
name = "wmf_airflow_dags"
# The version of the package is picked from our main dependency: Airflow
# The version of our environment could be found in:
#   * .gitlab-ci.yml
#   * debian/Dockerfile
version = "2.6.3"
description = "WMF Airflow libraries and DAGs"
readme = "README.md"

[project.urls]
Homepage = "https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags"

[project.optional-dependencies]
test = [
    "pytest ==7.3.*",
    "pytest-cov ==3.*",
    "pytest-mock ==3.10.*",
    "moto[s3]",
    "freezegun",
]
dev = [
    "ipython ==8.13.*",
    "pre-commit",
]
lint = [
    "flake8 ==6.0.*",
    "mypy ==1.3.*",
    "types-PyYAML",
    "types-requests",
    "types-python-dateutil",
    "types-retry",
    "black ==23.11.*",
    "isort",
]

[tool.files]
packages = "wmf_airflow_common"

[tool.setuptools]
include-package-data = false

[tool.isort]
py_version = 310
profile = "black"
skip_gitignore = true  # Will skip file marked in the gitignore
skip = [
    # Bug: Marking those folders here makes isort run faster than only declaring it in the .gitignore.
    ".tox",

    # TODO Remove progressively:
    "analytics_product", "tests/analytics_product",
    "platform_eng", "tests/platform_eng",
    "research", "tests/research",
    "search", "tests/search",
]

[tool.black]
line-length = 120
target-version = ['py310']
include = '\.pyi?$'
# 'extend-exclude' excludes files or directories in addition to the defaults
extend-exclude = '''
# A regex preceded with ^/ will apply only to files and directories
# in the root of the project.
(
  ^/(tests/)?analytics_product
  | ^/(tests/)?platform_eng
  | ^/(tests/)?research
  | ^/(tests/)?search
)
'''

[tool.mypy]
files = [ ".",]
explicit_package_bases = true
ignore_missing_imports = true
follow_imports = "skip"
disallow_untyped_defs = false
no_implicit_optional = true
check_untyped_defs = true
warn_return_any = true
show_error_codes = true
warn_unused_ignores = true
exclude = [
    ".build", "build",
    # TODO Remove progressively:
    "analytics_product", "tests/analytics_product",
    "platform_eng", "tests/platform_eng",
    "research", "tests/research",
    "search", "tests/search",
    "analytics/dags/refine/output_diff/output_diff.py"
]
disable_error_code = ["attr-defined"]

[tool.pytest.ini_options]
minversion = "7.0"
#addopts = "--ignore=docs --failed-first --no-cov"
#ignore = [
#    "docs"
#]
filterwarnings = [
    "ignore:.*:DeprecationWarning",
    "ignore:User attempted to set retries=10, but HDFSArchiveOperator is not idempotent and thus cannot retry. Forcing retries=0.:UserWarning"
]
testpaths = [
    "tests",
    "wmf_airflow_common",
]

# dict_keys(['markers', 'empty_parameter_set_mark', 'norecursedirs',
# 'testpaths', 'filterwarnings', 'usefixtures', 'python_files', 'python_classes',
# 'python_functions', 'disable_test_id_escaping_and_forfeit_all_rights_to_community_support',
# 'console_output_style', 'xfail_strict', 'tmp_path_retention_count',
# 'tmp_path_retention_policy', 'enable_assertion_pass_hook', 'junit_suite_name',
# 'junit_logging', 'junit_log_passing_tests', 'junit_duration_report', 'junit_family',
# 'doctest_optionflags', 'doctest_encoding', 'cache_dir', 'log_level', 'log_format',
# 'log_date_format', 'log_cli', 'log_cli_level', 'log_cli_format', 'log_cli_date_format',
# 'log_file', 'log_file_level', 'log_file_format', 'log_file_date_format', 'log_auto_indent', 'pythonpath',
# 'faulthandler_timeout', 'addopts', 'minversion', 'required_plugins',
# 'mock_traceback_monkeypatch', 'mock_use_standalone_module'])

[tool.coverage.run]
branch = true
source = [
    # TODO Add progressively:
    "wmf_airflow_common"
]

[tool.coverage.report]
show_missing = true
ignore_errors = true

[tool.setuptools.packages.find]
namespaces = false
