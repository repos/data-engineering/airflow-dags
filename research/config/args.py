"""NOTE: This file has been auto-generated using the command:
$ python3 -m research_datasets args --output args.py

Do not edit. Use the above command to regenerate.
See the research-datasets readme for more information:
https://gitlab.wikimedia.org/repos/research/research-datasets/-/blob/main/README.md
"""

from __future__ import annotations

from datetime import datetime
from enum import Enum
from pathlib import Path

from pydantic import BaseModel, Extra, Field


class Period(BaseModel):
    class Config:
        extra = Extra.forbid

    start: datetime = Field(..., title="Start")
    end: datetime = Field(..., title="End")


class Granularity(Enum):
    """
    Article unit to be used for embeddings.
    """

    lead = "lead"
    sections = "sections"
    full_text = "full_text"


class WikitextTable(Enum):
    """
    Name of the wikitext dataset in the data lake.
    """

    wmf_mediawiki_wikitext_current = "wmf.mediawiki_wikitext_current"
    wmf_mediawiki_wikitext_history = "wmf.mediawiki_wikitext_history"


class ArticleembeddingsPipelineRun(BaseModel):
    """
    Arguments for the command `articleembeddings.pipeline.run`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    wikis: list[str] = Field(..., title="Wikis")
    output: Path = Field(..., title="Output")
    period: Period
    model: str = Field(default="LaBSE", title="Model")
    granularity: Granularity = Granularity.lead
    wikitext_table: WikitextTable = WikitextTable.wmf_mediawiki_wikitext_current
    max_files_per_wiki: int = Field(default=4, title="Max Files Per Wiki")


class ArticletopicsPipelineRun(BaseModel):
    """
    Arguments for the command `articletopics.pipeline.run`
    """

    class Config:
        extra = Extra.forbid

    mediawiki_snapshot: str = Field(..., title="Mediawiki Snapshot")
    wikidata_snapshot: str = Field(..., title="Wikidata Snapshot")
    model_url: str = Field(..., title="Model Url")
    output: Path = Field(..., title="Output")
    max_files_per_wiki: int = Field(..., title="Max Files Per Wiki")
    wikis: list[str] | None = Field(default=None, title="Wikis")


class ReferencequalityPipelineRun(BaseModel):
    """
    Arguments for the command `referencequality.pipeline.run`
    """

    class Config:
        extra = Extra.forbid

    mediawiki_snapshot: str = Field(..., title="Mediawiki Snapshot")
    output: Path = Field(..., title="Output")
    partitions: int = Field(default=4, title="Partitions")
    sqlite_db: Path | None = Field(default=None, title="Sqlite Db")
    wikis: list[str] | None = Field(default=None, title="Wikis")


class RevertriskBasefeaturesRun(BaseModel):
    """
    Arguments for the command `revertrisk.basefeatures.run`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    wikis: list[str] = Field(..., title="Wikis")
    period: Period
    output: Path = Field(..., title="Output")
    partition_output: list[str | int] = Field(
        default=["wiki_db", 10], max_items=2, min_items=2, title="Partition Output"
    )


class ModelKind(Enum):
    """
    An enumeration.
    """

    language_agnostic = "language-agnostic"
    multilingual = "multilingual"


class RevertriskFeaturesRun(BaseModel):
    """
    Arguments for the command `revertrisk.features.run`
    """

    class Config:
        extra = Extra.forbid

    base_features: Path = Field(..., title="Base Features")
    output: Path = Field(..., title="Output")
    model: ModelKind = ModelKind.language_agnostic
    partition_output: list[str | int] | None = Field(
        default=None, max_items=2, min_items=2, title="Partition Output"
    )
    split_dataset: list[str | float] | None = Field(
        default=None, max_items=2, min_items=2, title="Split Dataset"
    )
    max_rows_per_wiki: int | None = Field(default=None, title="Max Rows Per Wiki")
    disputed_reverts: bool = Field(default=False, title="Disputed Reverts")
    drop_non_permanent_user_edits: bool = Field(
        default=False, title="Drop Non Permanent User Edits"
    )
    bot_edits: bool = Field(default=True, title="Bot Edits")


class RevertriskTrainRun(BaseModel):
    """
    Arguments for the command `revertrisk.train.run`
    """

    class Config:
        extra = Extra.forbid

    training_dataset: Path = Field(..., title="Training Dataset")
    model_uri: str = Field(..., title="Model Uri")
    balance_labels: bool = Field(default=False, title="Balance Labels")
    xgboost_param_grid: str | None = Field(default=None, title="Xgboost Param Grid")


class RevertriskEvaluateRunmodelevaluation(BaseModel):
    """
    Arguments for the command `revertrisk.evaluate.runmodelevaluation`
    """

    class Config:
        extra = Extra.forbid

    predictions_dataset: Path = Field(..., title="Predictions Dataset")
    output: Path = Field(..., title="Output")


class RevertriskEvaluateRunmodelcomparison(BaseModel):
    """
    Arguments for the command `revertrisk.evaluate.runmodelcomparison`
    """

    class Config:
        extra = Extra.forbid

    candidate_model_dir: Path = Field(..., title="Candidate Model Dir")
    reference_model_dir: Path = Field(..., title="Reference Model Dir")
    output: Path = Field(..., title="Output")


class RevertriskPredictRun(BaseModel):
    """
    Arguments for the command `revertrisk.predict.run`
    """

    class Config:
        extra = Extra.forbid

    features_dataset: Path = Field(..., title="Features Dataset")
    model_uri: str = Field(..., title="Model Uri")
    output: Path = Field(..., title="Output")


class RiskobservatoryPipelineRunrevertriskprediction(BaseModel):
    """
    Arguments for the command `riskobservatory.pipeline.runrevertriskprediction`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    wikis: list[str] = Field(..., title="Wikis")
    period: Period
    model_uri: str = Field(..., title="Model Uri")
    hdfs_dir: Path = Field(..., title="Hdfs Dir")
    hive_database: str | None = Field(default=None, title="Hive Database")


class RiskobservatoryPipelineRunhighriskthresholdcomputation(BaseModel):
    """
    Arguments for the command `riskobservatory.pipeline.runhighriskthresholdcomputation`
    """

    class Config:
        extra = Extra.forbid

    hdfs_dir: Path = Field(..., title="Hdfs Dir")
    hive_database: str = Field(..., title="Hive Database")
    predictions_hive_database: str | None = Field(
        default=None, title="Predictions Hive Database"
    )
    start_time: datetime | None = Field(default=None, title="Start Time")
    end_time: datetime | None = Field(default=None, title="End Time")


class RiskobservatoryPipelineRunriskstatisticscomputation(BaseModel):
    """
    Arguments for the command `riskobservatory.pipeline.runriskstatisticscomputation`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    hdfs_dir: Path = Field(..., title="Hdfs Dir")
    hive_database: str = Field(..., title="Hive Database")


class ToppagesPipelineRun(BaseModel):
    """
    Arguments for the command `toppages.pipeline.run`
    """

    class Config:
        extra = Extra.forbid

    output: Path = Field(..., title="Output")
    hour_to_process: datetime = Field(..., title="Hour To Process")
    top_n: int = Field(default=100, title="Top N")
    pageviews_threshold: int = Field(default=500, title="Pageviews Threshold")
    fingerprint_threshold: int = Field(default=100, title="Fingerprint Threshold")


class WikidiffPipelineRunbatch(BaseModel):
    """
    Arguments for the command `wikidiff.pipeline.runbatch`
    """

    class Config:
        extra = Extra.forbid

    hive_table_dir: Path = Field(..., title="Hive Table Dir")
    snapshot: str = Field(..., title="Snapshot")
    wiki_db: str = Field(..., title="Wiki Db")
    current_batch: int = Field(..., title="Current Batch")
    num_batches: int = Field(..., title="Num Batches")
    num_files: int | None = Field(default=None, title="Num Files")


class WikidiffPipelineRunwikis(BaseModel):
    """
    Arguments for the command `wikidiff.pipeline.runwikis`
    """

    class Config:
        extra = Extra.forbid

    hive_table_dir: Path = Field(..., title="Hive Table Dir")
    snapshot: str = Field(..., title="Snapshot")
    wikis: list[str] = Field(..., title="Wikis")
    num_files: list[int] = Field(..., title="Num Files")


class MwaddlinkGenerateanchordictionaryRun(BaseModel):
    """
    Arguments for the command `mwaddlink.generateanchordictionary.run`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    wiki_dbs: list[str] = Field(..., title="Wiki Dbs")
    output: Path = Field(..., title="Output")


class MwaddlinkGeneratewdpropertiesRun(BaseModel):
    """
    Arguments for the command `mwaddlink.generatewdproperties.run`
    """

    class Config:
        extra = Extra.forbid

    wikidata_snapshot: str = Field(..., title="Wikidata Snapshot")
    wiki_dbs: list[str] = Field(..., title="Wiki Dbs")
    wikidata_properties: list[str] = Field(..., title="Wikidata Properties")
    directory: Path = Field(..., title="Directory")


class MwaddlinkFilterdictanchorRun(BaseModel):
    """
    Arguments for the command `mwaddlink.filterdictanchor.run`
    """

    class Config:
        extra = Extra.forbid

    directory: Path = Field(..., title="Directory")


class MwaddlinkGeneratebacktestingdataRun(BaseModel):
    """
    Arguments for the command `mwaddlink.generatebacktestingdata.run`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    wiki_dbs: list[str] = Field(..., title="Wiki Dbs")
    directory: Path = Field(..., title="Directory")
    max_sentences_per_wiki: int = Field(default=200000, title="Max Sentences Per Wiki")


class MwaddlinkGeneratetrainingdataRun(BaseModel):
    """
    Arguments for the command `mwaddlink.generatetrainingdata.run`
    """

    class Config:
        extra = Extra.forbid

    snapshot: str = Field(..., title="Snapshot")
    wiki_dbs: list[str] = Field(..., title="Wiki Dbs")
    directory: Path = Field(..., title="Directory")
    files_per_wiki: int = Field(default=20, title="Files Per Wiki")


class MwaddlinkGenerateaddlinkmodelRun(BaseModel):
    """
    Arguments for the command `mwaddlink.generateaddlinkmodel.run`
    """

    class Config:
        extra = Extra.forbid

    wiki_dbs: list[str] = Field(..., title="Wiki Dbs")
    model_id: str = Field(..., title="Model Id")
    directory: Path = Field(..., title="Directory")
    grid_search: bool = Field(default=False, title="Grid Search")


class MwaddlinkGeneratebacktestingevalRun(BaseModel):
    """
    Arguments for the command `mwaddlink.generatebacktestingeval.run`
    """

    class Config:
        extra = Extra.forbid

    model_id: str = Field(..., title="Model Id")
    wiki_dbs: list[str] = Field(..., title="Wiki Dbs")
    directory: Path = Field(..., title="Directory")
    thresholds: list[float] = Field(
        default=[0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], title="Thresholds"
    )
    n_max: int | None = Field(default=None, title="N Max")


class WriteMode(Enum):
    append = "append"
    overwrite = "overwrite"


class ArticlequalityPipelineRunarticlefeatures(BaseModel):
    """
    Arguments for the command `articlequality.pipeline.runarticlefeatures`
    """

    class Config:
        extra = Extra.forbid

    period: Period
    output_table: str = Field(..., title="Output Table")
    write_mode: WriteMode = Field(..., title="Write Mode")
    wiki_ids: list[str] = Field(..., title="Wiki Ids")
    coalesce_features: int = Field(default=5, title="Coalesce Features")


class LanguageAgnosticCoefficients(BaseModel):
    class Config:
        extra = Extra.forbid

    length: float = Field(default=0.395, title="Length")
    media: float = Field(default=0.114, title="Media")
    ref: float = Field(default=0.181, title="Ref")
    heading: float = Field(default=0.123, title="Heading")
    category: float = Field(default=0.07, title="Category")
    link: float = Field(default=0.115, title="Link")


class StandardQualityThresholds(BaseModel):
    class Config:
        extra = Extra.forbid

    min_page_length: int = Field(default=8192, title="Min Page Length")
    min_num_categories: int = Field(default=1, title="Min Num Categories")
    min_num_headings: int = Field(default=7, title="Min Num Headings")
    min_num_media: int = Field(default=1, title="Min Num Media")
    min_num_references: int = Field(default=4, title="Min Num References")
    min_num_wikilinks: int = Field(default=2, title="Min Num Wikilinks")
    min_num_conditions: int = Field(default=5, title="Min Num Conditions")


class ArticlequalityPipelineRunarticlescores(BaseModel):
    """
    Arguments for the command `articlequality.pipeline.runarticlescores`
    """

    class Config:
        extra = Extra.forbid

    wikidata_snapshot: str = Field(..., title="Wikidata Snapshot")
    output_table: str = Field(..., title="Output Table")
    wiki_ids: list[str] = Field(..., title="Wiki Ids")
    article_features_table: str = Field(
        default="research.article_features", title="Article Features Table"
    )
    language_agnostic_coefficients: LanguageAgnosticCoefficients = Field(
        default_factory=lambda: LanguageAgnosticCoefficients.parse_obj(
            {
                "length": 0.395,
                "media": 0.114,
                "ref": 0.181,
                "heading": 0.123,
                "category": 0.07,
                "link": 0.115,
            }
        ),
        title="Language Agnostic Coefficients",
    )
    standard_quality_thresholds: StandardQualityThresholds = Field(
        default_factory=lambda: StandardQualityThresholds.parse_obj(
            {
                "min_page_length": 8192,
                "min_num_categories": 1,
                "min_num_headings": 7,
                "min_num_media": 1,
                "min_num_references": 4,
                "min_num_wikilinks": 2,
                "min_num_conditions": 5,
            }
        ),
        title="Standard Quality Thresholds",
    )
    coalesce_scores: int = Field(default=200, title="Coalesce Scores")
