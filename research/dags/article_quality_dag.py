"""A DAG to generate article quality scores."""

import getpass
import os
from dataclasses import dataclass, field
from datetime import datetime, timedelta

from airflow import DAG
from airflow.decorators import task, task_group
from airflow.operators.empty import EmptyOperator

from research.config import args, dag_config
from research.config.wikis import WIKIS
from research.dags import snapshot_sensor
from wmf_airflow_common.hooks.spark import kwargs_for_virtualenv
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.rest_external_task import RestExternalTaskSensor
from wmf_airflow_common.templates.time_filters import filters

# Note: Get Certificate Authority bundle for TLS encryption using the CA public key.
os.environ["REQUESTS_CA_BUNDLE"] = "/etc/ssl/certs/ca-certificates.crt"


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    # article features config
    skip_features: bool = False
    start_time: str = "{{ data_interval_start | to_ds }}"
    end_time: str = "{{ data_interval_end | to_ds }}"
    features_table: str = "article_quality_dev.features"
    features_write_mode: args.WriteMode = args.WriteMode.append
    wikis: list[str] = field(default_factory=lambda: WIKIS)
    coalesce_features_files: int = 5
    # article scores config
    skip_scores: bool = False
    wikidata_snapshot: str = "{{data_interval_end | start_of_current_week | to_ds}}"
    scores_table: str = "article_quality_dev.scores"
    language_agnostic_coefficients: args.LanguageAgnosticCoefficients = field(
        default_factory=lambda: args.LanguageAgnosticCoefficients()
    )
    standard_quality_thresholds: args.StandardQualityThresholds = field(
        default_factory=lambda: args.StandardQualityThresholds()
    )
    coalesce_scores_files: int = 100
    # spark configs
    shuffle_partitions: int = 4096
    driver_memory: str = "8G"
    executor_cores: int = 4
    executor_memory: str = "24G"
    executor_memory_overhead: str = "4G"
    max_executors: int = 98


dag_id = "article_quality"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {
    "do_xcom_push": True,
}
username = getpass.getuser()


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=datetime(2025, 2, 12),
    schedule_interval="@daily",
    dagrun_timeout=timedelta(days=30),
    catchup=True,
    user_defined_filters=filters,
    tags=["spark", "article-features", "research", "article-quality"],
    default_args=default_args,
) as dag:
    spark_conf = {
        "spark.shuffle.service.enabled": "true",
        "spark.executor.memoryOverhead": "2048",
        "spark.sql.sources.partitionOverwriteMode": "dynamic",
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.sql.adaptive.enabled": "true",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
    }

    common_args = kwargs_for_virtualenv(
        launcher="skein",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
        packages="org.apache.spark:spark-avro_2.12:3.1.2",
    )

    def compute_article_features():
        if props.skip_features:
            return EmptyOperator(task_id="article_features_skipped")

        @task()
        def pipeline_args(start_time: str, end_time: str) -> str:
            # Resolve the template at execution time
            return args.ArticlequalityPipelineRunarticlefeatures(
                period=args.Period(
                    start=datetime.fromisoformat(start_time),
                    end=datetime.fromisoformat(end_time),
                ),
                output_table=props.features_table,
                write_mode=props.features_write_mode,
                wiki_ids=props.wikis,
                coalesce_features=props.coalesce_features_files,
            ).json()

        return SparkSubmitOperator(
            task_id="compute_article_features",
            application_args=[
                "articlequality.pipeline.runarticlefeatures",
                pipeline_args(props.start_time, props.end_time),
            ],
            **common_args,
        )

    def compute_article_scores():
        if props.skip_scores:
            return EmptyOperator(task_id="article_scores_skipped")

        pipeline_args = args.ArticlequalityPipelineRunarticlescores(
            wikidata_snapshot=props.wikidata_snapshot,
            output_table=props.scores_table,
            wiki_ids=props.wikis,
            article_features_table=props.features_table,
            language_agnostic_coefficients=props.language_agnostic_coefficients,
            standard_quality_thresholds=props.standard_quality_thresholds,
        )
        return SparkSubmitOperator(
            task_id="compute_article_scores",
            application_args=[
                "articlequality.pipeline.runarticlescores",
                pipeline_args.json(),
            ],
            **common_args,
        )

    @task_group(group_id="article_quality_sensors")
    def sensors():
        _ = [
            RestExternalTaskSensor(
                task_id="wait_for_analytics_iceberg_wmf_content_mediawiki_content_history_v1",
                external_instance_uri="https://airflow-analytics.discovery.wmnet:30443",
                external_dag_id="mw_content_merge_events_to_mw_content_history_daily",
                allowed_states=["success"],
                check_existence=True,
            ),
            snapshot_sensor.wait_for_wikidata_item_page_link_snapshot(
                props.wikidata_snapshot
            ),
        ]

    _ = sensors() >> compute_article_features() >> compute_article_scores()
