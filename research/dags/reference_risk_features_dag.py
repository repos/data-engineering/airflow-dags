from datetime import datetime, timedelta
from pathlib import Path

from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import BranchPythonOperator
from pydantic.dataclasses import dataclass

from research.config import args, dag_config
from research.dags.snapshot_sensor import (
    wait_for_mediawiki_wikitext_current_snapshot,
    wait_for_mediawiki_wikitext_history_snapshot,
)
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters


@dataclass(frozen=True)
class SwiftConfig:
    container: str
    container_permissions: str
    object: str
    checksum_object: str

    def construct_publish_task(self, hdfs_path: Path | None) -> SimpleSkeinOperator:
        return SimpleSkeinOperator(
            task_id="publish_to_swift",
            files={"swift_auth.env": dag_config.swift_auth_file},
            script=" && ".join(
                [
                    f"hdfs dfs -get {hdfs_path}",
                    "source swift_auth.env",
                    f"sha512sum -b {hdfs_path.name} > checksum",
                    f"swift upload --object-name {self.object} {self.container} {hdfs_path.name}",
                    f"swift upload --object-name {self.checksum_object} {self.container} checksum",
                    f"swift post {self.container} --read-acl {self.container_permissions}",
                ]
            )
            if hdfs_path
            else None,
        )


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    # Dag configuration
    alerts_email: str = "research-engineering-alerts@lists.wikimedia.org"
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    sla: timedelta = timedelta(days=3)
    # Pipeline arguments
    pipeline_args: args.ReferencequalityPipelineRun = args.ReferencequalityPipelineRun(
        mediawiki_snapshot="{{ data_interval_start | to_ds_month }}",
        output=(Path(dag_config.hdfs_temp_directory) / "reference_risk" / "features"),
        partitions=8,
        sqlite_db=(
            Path(dag_config.hdfs_temp_directory)
            / "reference_risk"
            / "inference"
            / "{{ data_interval_start | to_ds_nodash }}"
            / "features.db"
        ),
        wikis=None,
    )
    # Swift configuration
    publish_to_swift: bool = False
    swift_config: SwiftConfig = SwiftConfig(
        container="feature-sets",
        container_permissions="'.r:*,.rlistings'",
        object="reference-risk/{{ data_interval_start | to_ds_nodash }}/features.db",
        checksum_object="reference-risk/{{ data_interval_start | to_ds_nodash }}/features.sha512",
    )
    # Spark configuration
    driver_cores: int = 4
    driver_memory: str = "32G"
    executor_cores: int = 4
    executor_memory: str = "20G"
    executor_memory_overhead: str = "4G"
    max_executors: int = 128
    shuffle_partitions: int = 16384


dag_id = "reference_risk_features"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {"email": props.alerts_email, "sla": props.sla}
pipeline_args = props.pipeline_args


with DAG(
    dag_id=dag_id,
    schedule="@monthly",
    start_date=datetime(2024, 8, 1),
    user_defined_filters=filters,
    default_args=default_args,
    tags=["research", "ml", "reference-risk"],
    catchup=False,
) as dag:
    wait_for_data = (
        wait_for_mediawiki_wikitext_history_snapshot(pipeline_args.mediawiki_snapshot),
        wait_for_mediawiki_wikitext_current_snapshot(pipeline_args.mediawiki_snapshot),
    )

    spark_conf = {
        "spark.hadoop.fs.permissions.umask-mode": "022",
        "spark.sql.sources.partitionOverwriteMode": "dynamic",
        "spark.sql.shuffle.partitions": props.shuffle_partitions,
        "spark.driver.maxResultSize": "2G",
        "spark.network.timeout": "512s",
        "spark.shuffle.io.retryWait": "60s",
        "spark.shuffle.io.maxRetries": 10,
    }
    compute_features = SparkSubmitOperator.for_virtualenv(
        task_id="compute_reference_risk_features",
        virtualenv_archive=props.conda_env,
        entry_point="bin/research_datasets_pipelines.py",
        application_args=["referencequality.pipeline.run", pipeline_args.json()],
        driver_cores=props.driver_cores,
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        executor_memory_overhead=props.executor_memory_overhead,
        max_executors=props.max_executors,
        conf=spark_conf,
    )

    should_publish_to_swift = BranchPythonOperator(
        task_id="should_publish_to_swift",
        python_callable=lambda: "publish_to_swift" if props.publish_to_swift else "no_op",
    )

    publish_database_to_swift = props.swift_config.construct_publish_task(
        hdfs_path=pipeline_args.sqlite_db,
    )

    no_op = EmptyOperator(task_id="no_op")

    wait_for_data >> compute_features >> should_publish_to_swift >> [publish_database_to_swift, no_op]
