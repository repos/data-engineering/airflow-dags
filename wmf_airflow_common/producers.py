from abc import ABC, abstractmethod
from typing import Any, Dict, Optional

from airflow import DAG
from airflow.configuration import conf as airflow_conf
from airflow.sensors.base import BaseSensorOperator
from airflow.sensors.external_task import ExternalTaskSensor

from wmf_airflow_common.config import AIRFLOW_DEPLOYMENT_MAP, AIRFLOW_WEBSERVER_MAP
from wmf_airflow_common.sensors.rest_external_task import RestExternalTaskSensor
from wmf_airflow_common.util import abbreviate


class DatasetProducer(ABC):
    """Abstract class for objects describing metadata about entities
    producing datasets. These entities can provide their own Airflow sensors
    for determining dataset readiness.
    """

    def __init__(self, **kwargs) -> None:
        pass

    @abstractmethod
    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None, **kwargs) -> BaseSensorOperator:
        """Get a sensor to determine dataset readiness."""

    @abstractmethod
    def _get_task_id(self) -> str:
        """Construct a task ID."""


class AirflowDatasetProducer(DatasetProducer):
    """Dataset producer metadata for Airflow-produced datasets.

    Metadata includes:
    - Airflow instance label
    - Airflow DAG in that instance
    - task OR task_group
    """

    def __init__(
        self,
        instance: str,
        dag_id: str,
        task_group_id: Optional[str] = None,
        task_id: Optional[str] = None,
    ) -> None:
        self.airflow_instance: str = instance
        self.external_dag_id: str = dag_id
        self.external_task_group_id: Optional[str] = task_group_id
        self.external_task_id: Optional[str] = task_id

        if self.airflow_instance not in AIRFLOW_DEPLOYMENT_MAP:
            raise ValueError(f"Unknown Airflow instance {self.airflow_instance}")

        if self.external_task_group_id and self.external_task_id:
            raise ValueError("Exactly one of task or task_group can be defined")

        self.local_airflow_instance: Optional[str] = None

        webserver_url = airflow_conf.get("webserver", "base_url")
        for label, url in AIRFLOW_WEBSERVER_MAP.items():
            if webserver_url in url:
                self.local_airflow_instance = label
                break

    def __repr__(self) -> str:
        return (
            f"AirflowDatasetProducer[{self.airflow_instance=}, "
            f"{self.external_dag_id=}, "
            f"{self.external_task_group_id=}, "
            f"{self.external_task_id=}]"
        )

    def _get_task_id(self) -> str:
        """
        Returns the normalized task name of the sensor.
        """
        if self.external_task_id is not None or self.external_task_group_id is not None:
            if self.external_task_id is not None:
                task_label = abbreviate(self.external_task_id)
            elif self.external_task_group_id is not None:
                task_label = abbreviate(self.external_task_group_id)

            return f"wait_for_{self.airflow_instance}_{self.external_dag_id}_{task_label}"

        return f"wait_for_{self.airflow_instance}_{self.external_dag_id}"

    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None, **kwargs) -> BaseSensorOperator:
        """
        Returns a Sensor that will check for the existence of the appropriate partitions given
        the DAG's schedule interval, the dataset's granularity and the dag_run's execution date.
        :param dag:
            DAG object for the sensor to run in.
        :param custom_task_id:
            Custom name for the sensor task. If not provided, the name is going to be auto-generated.
        :param kwargs:
            Extra keyword arguments provided for the expected sensor.
        """

        sensor_task_id = custom_task_id or self._get_task_id()

        common_params: Dict[str, Optional[Any]] = {
            "task_id": sensor_task_id,  # task_id of the sensor itself
            "external_dag_id": self.external_dag_id,
            # execution_delta=timedelta(hours=-1),
            "allowed_states": ["success"],
            "check_existence": True,
            **kwargs,
        }

        task_params: Dict[str, Optional[Any]] = {}

        if self.external_task_id:
            task_params["external_task_id"] = self.external_task_id
        elif self.external_task_group_id:
            task_params["external_task_group_id"] = self.external_task_group_id

        if "execution_delta" in kwargs:
            task_params["execution_delta"] = kwargs["execution_delta"]

        not_on_local_instance = self.local_airflow_instance != self.airflow_instance

        sensor_specific_params: Dict[str, Any] = {}
        if not_on_local_instance:
            sensor_specific_params = {
                "external_instance_uri": AIRFLOW_DEPLOYMENT_MAP[self.airflow_instance],
            }

        sensor_params = {
            **common_params,
            **task_params,
            **sensor_specific_params,
        }

        if not_on_local_instance:
            return RestExternalTaskSensor(**sensor_params)  # type: ignore[arg-type]

        return ExternalTaskSensor(**sensor_params)


DATASET_PRODUCERS_MAP = {
    "airflow": AirflowDatasetProducer,
}
