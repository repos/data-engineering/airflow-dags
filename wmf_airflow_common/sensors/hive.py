from typing import Any, Optional, Sequence

from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from wmf_airflow_common.partitions_builder import PrePartitions, build_partition_names


class RangeHivePartitionSensor(NamedHivePartitionSensor):
    """
    Waits for a set of partitions to show up in Hive.

    :param
    partition_specs:
      - table_name: the hive full table name
      - from_timestamp: from which point in time to create the partitions
      - to_timestamp: from which point in time to stop the partitions
      - granularity: (@hourly, @daily, ...) check for all the partitions matching
        this parameter.
      - pre_partitions: adds some extra partitions before the time ones.
      - custom_partition_format: specify a custom partition format

    example of partition_specs:
        { 'table_name': 'wmf.mediawiki_page_move',
          'from': '{{data_interval_start}}',
          'to': '2022-02-19T15:47:03.051064+01:00',
          'granularity': '@daily',
          'pre_partitions': "datacenter='codfw' }

    All other parameters are forwarded to NamedHivePartitionSensor.
    """

    ui_color = "#8d99ae"

    poke_context_fields = ("partition_names", "metastore_conn_id")

    template_fields = [
        "table_name",
        "from_timestamp",
        "to_timestamp",
        "granularity",
        "pre_partitions",
    ]

    def __init__(
        self,
        table_name: str,
        from_timestamp: str,
        to_timestamp: str,
        granularity: str,
        pre_partitions: Optional[PrePartitions] = None,
        custom_partition_format: Optional[str] = None,
        *args: Any,
        **kwargs: Any,
    ):
        self.table_name = table_name
        self.from_timestamp = from_timestamp
        self.to_timestamp = to_timestamp
        self.granularity = granularity
        self.pre_partitions = pre_partitions
        self.custom_partition_format = custom_partition_format

        super().__init__(partition_names=[], *args, **kwargs)

    def pre_execute(self, context: Any = None) -> None:
        """Fill the partition_names list before execution."""
        self.partition_names = build_partition_names(
            self.table_name,
            self.from_timestamp,
            self.to_timestamp,
            self.granularity,
            pre_partitions=self.pre_partitions,
            custom_partition_format=self.custom_partition_format,
        )


class NamedHivePartitionsSensor(NamedHivePartitionSensor):
    """
    A variation of the NamedHivePartitionSensor that allows for a variable list of partitions to be passed in
    as a template field.
    """

    template_fields: Sequence[str] = ("_partition_names_template", "_soft_fail")

    def __init__(
        self,
        partition_names_template: str,
        *args: Any,
        **kwargs: Any,
    ):
        self._partition_names_template = partition_names_template
        self._soft_fail = kwargs.pop("soft_fail", False)
        super().__init__(partition_names=[], *args, **kwargs)

    def pre_execute(self, context: Any = None) -> None:
        self.render_template_fields(context)  # Need to render the list of partitions.
        self.partition_names = self._partition_names_template.split(",")
        self.soft_fail = str(self._soft_fail) == "True"  # Xcom is a string.
        super().pre_execute(context)
