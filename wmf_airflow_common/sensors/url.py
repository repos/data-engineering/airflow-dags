from typing import Any, List
from urllib.parse import urlparse

import fsspec
from airflow.sensors.base import BaseSensorOperator
from airflow.utils.context import Context

# TODO: after https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/merge_requests/5 is deployed,
# use this to automate using new pyarrow HDFS API via fsspec:
from workflow_utils.util import fsspec_use_new_pyarrow_api

# This should be removed once https://github.com/fsspec/filesystem_spec/issues/874 is resolved.
fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars=True)


def fsspec_exists(url: str) -> bool:
    """
    :param url:
        A URL to check if exists via fsspec.
    """
    # exists is a method on fsspecn FileSystem, so
    # we 'open' the file (with fsspec, not python file IO),
    # get the fsspec FileSystem, and then
    # ask the FileSystem if the url path exists on that FileSystem.
    url_parsed = urlparse(url)
    return bool(fsspec.open(url).fs.exists(url_parsed.path))


class URLSensor(BaseSensorOperator):
    """
    A Sensor that detects if a URL exists.
    This Sensor uses fsspec, so any URL
    supported by fsspec will work.
    """

    template_fields = ("_url", "_soft_fail")

    def __init__(self, url: str | List[str], **kwargs: Any):
        """
        :param url:
            url to check for existence.
            Any URL protocol supported by fsspec can be used,
            e.g. file:// http://, hdfs:// etc.
            Could be a single URL, or a list of URLs (a list or a comma-separated string).
        """

        self._url = url
        self._soft_fail = kwargs.pop("soft_fail", False)
        super().__init__(**kwargs)

    def poke(self, context: Context) -> bool:
        self.log.info("Checking for existence of %s ", self._url)
        urls = self._url.split(",") if isinstance(self._url, str) else self._url
        return all(fsspec_exists(url) for url in urls)

    def pre_execute(self, context: Any):
        self.render_template_fields(context)  # Need to render templated _soft_fail.
        self.soft_fail = str(self._soft_fail) == "True"  # Xcom is a string.
        super().pre_execute(context)
