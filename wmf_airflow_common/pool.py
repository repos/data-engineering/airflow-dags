from typing import Dict, List

import yaml
from airflow.models.pool import Pool


class PoolRegistry:
    """
    Class containing all Airflow Pool objects parsed from a list of Yaml pool definitions.
    Provides a get_or_create() method that ensures the pool has been registered in the Airflow database.
    """

    def __init__(self, pool_file_paths: List[str]) -> None:
        """
        Initializes a PoolRegistry by parsing from a set of Yaml pool files.
        """
        self.pools: Dict[str, Dict] = {}
        for path in pool_file_paths:
            with open(path, "r") as pool_file:
                for pool_name, pool_params in yaml.safe_load(pool_file).items():
                    self.pools[pool_name] = pool_params

    def get_or_create(self, pool_name: str) -> str:
        """
        Verify whether the pool_name is valid against the declared pools in Yaml.
        If not valid, fail. If valid, continue to check whether the pool has been
        created in the Airflow database. If not, create it.

        Return the pool_name, so that we can use this helper method in a 'pool' task parameter.
        """

        if pool_name not in self.pools:
            raise KeyError(f"Pool name {pool_name} not declared.")

        pool = Pool.get_pool(pool_name)
        if pool is None:
            # First time this pool has been used. Materialize the Yaml config read on __init__
            # into the Airflow database
            pool_params = self.pools[pool_name]
            Pool.create_or_update_pool(
                name=pool_name,
                slots=pool_params["slots"],
                description=pool_params["description"],
                include_deferred=False,
            )

        return pool_name
