import logging
from datetime import date, datetime, timedelta

from airflow.models import Variable

from wmf_airflow_common.clients.s3 import get_s3_client


def delete_old_dag_task_logs(s3_paginator, s3_log_bucket: str, s3_log_retention_days: int) -> list:
    """Return a list of DAG task logfiles that are past the retention policy"""
    expired_logs = []
    today = date.today()

    for page in s3_paginator.paginate(Bucket=s3_log_bucket, Prefix="dag_id="):
        for obj in page.get("Contents", []):
            filepath = obj["Key"]
            # filepath is of the format
            # dag_id=addition/run_id=scheduled__2024-09-21T08:00:00+00:00/task_id=add_one_and_two/attempt=1.log
            try:
                task_scheduling_time_str = filepath.split("/")[1].split("__")[1]
                task_scheduling_date = datetime.fromisoformat(task_scheduling_time_str).date()
            except Exception:
                logging.exception(f"Could not parse {filepath}. Skipping.")
                continue
            if today - task_scheduling_date >= timedelta(days=s3_log_retention_days):
                logging.info(f"{filepath} will be deleted")
                expired_logs.append({"Key": obj["Key"]})
    return expired_logs


def delete_old_scheduler_logs(s3_paginator, s3_log_bucket: str, s3_log_retention_days: int) -> list:
    """Return a list of scheduler logfiles that are past the retention policy"""
    expired_logs = []
    today = date.today()

    for page in s3_paginator.paginate(Bucket=s3_log_bucket, Prefix="scheduler/"):
        for obj in page.get("Contents", []):
            filepath = obj["Key"]
            # filepath is of the format scheduler/2024-11-20/aqs/aqs_hourly_dag.py.log
            try:
                task_scheduling_date_str = filepath.split("/")[1]
                task_scheduling_date = date.fromisoformat(task_scheduling_date_str)
            except Exception:
                logging.exception(f"Could not parse {filepath}. Skipping.")
                continue
            if today - task_scheduling_date >= timedelta(days=s3_log_retention_days):
                logging.info(f"{filepath} will be deleted")
                expired_logs.append({"Key": obj["Key"]})
    return expired_logs


def purge_old_logs_from_s3() -> int:
    """
    Iterate over every element in the airflow logs bucket and delete all
    logs older than the retention variable.

    """
    s3_client = get_s3_client("s3_dpe")
    s3_log_retention_days = int(Variable.get("s3_log_retention_days"))
    s3_log_bucket = Variable.get("s3_log_bucket")
    s3_paginator = s3_client.get_paginator("list_objects_v2")
    to_delete = []

    logging.info(f"Logs older than {s3_log_retention_days} days will be deleted from s3")
    to_delete.extend(
        delete_old_dag_task_logs(
            s3_paginator=s3_paginator,
            s3_log_bucket=s3_log_bucket,
            s3_log_retention_days=s3_log_retention_days,
        )
    )
    to_delete.extend(
        delete_old_scheduler_logs(
            s3_paginator=s3_paginator,
            s3_log_bucket=s3_log_bucket,
            s3_log_retention_days=s3_log_retention_days,
        )
    )

    logging.info(f"Deleting {len(to_delete)} logfiles")
    s3_client.delete_objects(Bucket=s3_log_bucket, Delete={"Objects": to_delete})
    return len(to_delete)
