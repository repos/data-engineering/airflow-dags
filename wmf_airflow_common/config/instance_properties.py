"""Place to keep metadata about Airflow instances.
"""

from wmf_airflow_common.util import is_running_in_kubernetes

# URLs found on
# https://wikitech.wikimedia.org/wiki/Data_Platform/Systems/Airflow/Instances#List_of_instances
AIRFLOW_INSTANCE_MAP = {
    "analytics": "http://an-launcher1002.eqiad.wmnet:8600",
    "analytics_test": "http://an-test-client1002.eqiad.wmnet:8600",
    "search": "http://an-airflow1005.eqiad.wmnet:8600",
    "research": "http://an-airflow1002.eqiad.wmnet:8600",
    "platform_eng": "http://an-airflow1004.eqiad.wmnet:8600",
    "analytics_product": "http://an-airflow1006.eqiad.wmnet:8600",
    "wmde": "http://an-airflow1007.eqiad.wmnet:8600",
}

# instance to Kubernetes deployments API URLs map.
AIRFLOW_K8s_API_MAP = {
    "analytics": "https://airflow-analytics.discovery.wmnet:30443",
    "analytics_test": "https://airflow-analytics-test.discovery.wmnet:30443",
    "search": "https://airflow-search.discovery.wmnet:30443",
    "research": "https://airflow-research.discovery.wmnet:30443",
    "platform_eng": "https://airflow-platform-eng.discovery.wmnet:30443",  # kerberos not enabled on this one
    "analytics_product": "https://airflow-analytics-product.discovery.wmnet:30443",  # kerberos not enabled on this one
    "wmde": "https://airflow-wmde.discovery.wmnet:30443",
}

# instance to publicly accessible Kubernetes deployment URL map.
# Used to determine which instance the DAG code is executing on.
AIRFLOW_K8s_PUBLIC_MAP = {
    "analytics": "https://airflow-analytics.wikimedia.org",
    "analytics_test": "https://airflow-analytics-test.wikimedia.org",
    "search": "https://airflow-search.wikimedia.org",
    "research": "https://airflow-research.wikimedia.org",
    "platform_eng": "https://airflow-platform-eng.wikimedia.org",
    "analytics_product": "https://airflow-analytics-product.wikimedia.org",
    "wmde": "https://airflow-wmde.wikimedia.org",
}

AIRFLOW_DEPLOYMENT_MAP = AIRFLOW_K8s_API_MAP if is_running_in_kubernetes() else AIRFLOW_INSTANCE_MAP

AIRFLOW_WEBSERVER_MAP = AIRFLOW_K8s_PUBLIC_MAP if is_running_in_kubernetes() else AIRFLOW_INSTANCE_MAP
