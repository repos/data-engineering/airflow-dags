import boto3
from airflow.configuration import get_custom_secret_backend


def get_s3_client(conn_id: str = "s3_dpe"):
    """Return an s3 client configured with the details of the provided connection id"""
    secret_backend = get_custom_secret_backend()
    s3_connection = secret_backend.get_connection(conn_id=conn_id)
    session = boto3.session.Session()
    return session.client(
        service_name="s3",
        aws_access_key_id=s3_connection.login,
        aws_secret_access_key=s3_connection.password,
        **s3_connection.extra_dejson,
    )
