"""
Custom Airflow API client adding Kerberos auth support
"""

import os
from urllib.parse import urlparse

import airflow_client
import requests_kerberos
import spnego


class AirflowApiClientKerberos(airflow_client.client.ApiClient):
    """An Airflow API client adding Kerberos authentication support."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.inject_gssapi_token_in_default_headers()

    def inject_gssapi_token_in_default_headers(self):
        """Generate the GSSAPI token and inject it in the default headers.

        This way, any subsequent request made to the API host will include the Kerberos authentication
        token, and thus will be authenticated.

        Now, why do we do things this way? Well.. _cracks knuckles_.

        request_kerberos intercepts the first request made to the service, and generates a negotiation
        token when getting a 401 response, after which it re-issues the same request with the `authorization`
        header (see https://github.com/requests/requests-kerberos/blob/master/requests_kerberos/kerberos_.py#L189).

        However, the generated api_client does not use requests. It uses a rest api helper that directly hooks
        into urllib3, thus completely bypassing the hooks setup by requests_kerberos.

        What we do here is pre-emptively send a request to the KDC, asking for a ticket that will allow
        us to establish an authenticated communication with the API host, and inject the token in the
        default headers, as part of the `Authorization` token. These default tokens are added to all request
        sent to the API server,
        cf https://github.com/apache/airflow-client-python/blob/main/airflow_client/client/api_client.py#L143

        By doing this, we transparently turn the generated api_client module into a Kerberos authenticated
        API client.

        """
        cred = spnego.KerberosKeytab(
            keytab=f"FILE:{os.environ['KRB5_KEYTAB']}",
            principal=os.environ["KRB5_PRINCIPAL"],
        )
        auth = requests_kerberos.HTTPKerberosAuth(
            principal=cred,
            # airflow hardcodes its Kerberos service name to `airflow`, cf
            # https://github.com/apache/airflow/blob/main/providers/src/airflow/providers/fab/auth_manager/api/auth/backend/kerberos_auth.py#L66
            service="airflow",
        )
        gssapi_token = auth.generate_request_header(
            host=urlparse(self.configuration.host).hostname,
            response=None,
            is_preemptive=True,
        )
        self.set_default_header("Authorization", gssapi_token)
