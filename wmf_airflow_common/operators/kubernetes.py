from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator

from wmf_airflow_common.util import airflow_pod_template_path


class WMFKubernetesPodOperator(KubernetesPodOperator):
    """Provides the necessary default pod templates to ease the creation of pods via the KubernetesPodOperator"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # The default template file to be used to generate the pod created by the KubernetesPodOoperator
        self.pod_template_file: str = self.pod_template_file or airflow_pod_template_path(
            "kubernetes_pod_operator_default_pod_template"
        )
        # The default template to be used to create the pod that will run the KubernetesPodOperator
        if not self.executor_config.get("pod_template_file"):
            self.executor_config["pod_template_file"] = airflow_pod_template_path(
                "kubernetes_executor_pod_template_kubeapi_enabled"
            )
