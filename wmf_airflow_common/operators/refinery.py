# Airflow operators that wrap and expose refinery capabilities.
import os
from typing import List, Sequence

from wmf_airflow_common.operators.python import python_script_executor
from wmf_airflow_common.util import is_running_in_kubernetes, warn_experimental

# As of 2025-01 we run refinery script both in k8s and ganeti VMs.
# Executable paths are dependent on the runtime
refinery_python_env = "/srv/deployment/analytics/refinery/python"
drop_older_than_script_path = "/srv/deployment/analytics/refinery/bin/refinery-drop-older-than"
drop_older_than_script_pod_path = "/opt/refinery/bin/refinery-drop-older-than"

docker_tag = "2025-01-16-124629-dc160ba98355aab28add89f314345f6b5b633ac0"
docker_image = f"docker-registry.discovery.wmnet/repos/data-engineering/refinery:{docker_tag}"


@warn_experimental(message="This method is experimental, and might be deprecated in the future.")
@python_script_executor(
    drop_older_than_script_pod_path if is_running_in_kubernetes() else drop_older_than_script_path,
    env=None if is_running_in_kubernetes() else refinery_python_env,
    docker_image=docker_image if is_running_in_kubernetes() else None,
)
def drop_older_than(
    database: str,
    tables: Sequence[str],
    checksum: str,
    base_path: str = "",
    path_format: str = "",
    older_than_days: int = 90,
    allowed_interval: int = 3,
    skip_trash: bool = False,
) -> List[str]:
    """Prepare arguments for dropping expired partitions.
    Wraps the refinery-drop-older-than script
    and executes it using the system provided python interpreter.

    :param database: Database name to operate on
    :param tables: Sequence of table names to drop partitions from
    :param base_path: The base HDFS path to use when deleting data, optional
    :param path_format: The path format to use when deleting data from HDFS, optional
    :param checksum: Manual execution checksum from refinery script test run
    :param older_than_days: Days threshold override, defaults to 90
    :param allowed_interval: Allowed execution interval in days, defaults to 3
    :param skip_trash: whether to skip HDFS trash when deleting, defaults to False
    :returns: List of prepared script arguments
    """
    # On bare metal, the refinery script is available on the scheduler host.
    # On k8s it will only be available in a dedicated executor pod.
    # Check if drop_older_than_script_path exists on bare metal, but delegate
    # k8s logic to python_script_executor. The decorator is responsible for
    # providing a pod with the necessary dependencies.
    if not is_running_in_kubernetes() and not os.path.isfile(drop_older_than_script_path):
        error_msg = (
            f"Script not found at {drop_older_than_script_path}. "
            "Please ensure refinery is properly installed on your target airflow instance,"
            "and the path is correct."
        )
        raise FileNotFoundError(error_msg)

    parameters = [
        "--verbose",
        f"--database={database}",
        f'--tables=^({"|".join(tables)})$',
        f"--older-than={older_than_days}",
        f"--allowed-interval={allowed_interval}",
        f"--execute={checksum}",
    ]
    if base_path and path_format:
        parameters += [
            f"--base-path={base_path}",
            f"--path-format={path_format}",
        ]
    if skip_trash:
        parameters += ["--skip-trash"]

    return parameters
