import functools
import shlex
from typing import Any, Callable, Dict, Optional, Union

from airflow.operators.bash import BashOperator

from wmf_airflow_common.operators.kubernetes import WMFKubernetesPodOperator
from wmf_airflow_common.util import airflow_pod_template_path, is_running_in_kubernetes


def python_script_executor(
    script: str,
    env: Optional[str],
    docker_image: Optional[str],
) -> Callable:
    """Creates a wrapper that converts a Python function into a script.

    The wrapper executes the decorated function with script parameters and combines its output
    with Airflow operator configurations to create a BashOperator, or if Airflow is running in
    Kubernetes, into a KubernetesPod. The operator runs the script in a specified Python
    environment with proper argument escaping.


    The wrapper executes the decorated function with script parameters and combines its output
    with Airflow operator configurations to create a BashOperator. The operator runs the script
    in a specified Python environment with proper argument escaping.

    :param script_path: path to the Python script to be executed
    :param env: python environment to execute the script in.
        Only applies to bash scripts not running in k8s.
    :param docker_image: default docker image to execute the script in, when running on k8s.
        Can be overridden via wrapped func `operator_config`
    :param func: Function that generates script arguments. Should accept keyword
                arguments and return a list of strings representing command-line arguments.
    :type func: Callable

    :return: A wrapper function that accepts:
             operator_config: Airflow operator configurations (e.g., task_id, retries)
             **script_kwargs: Keyword arguments passed to the decorated function
    :rtype: Callable[[Dict[str, Any], Any], BashOperator]

    :example:

        @python_script_executor("/path/to/script.py")
        def my_task(param1: str, param2: int):
            return ["--param1", param1, "--param2", str(param2)]

        task = my_task(
            operator_config={"task_id": "my_task"},
            param1="value",
            param2=42
        )
    """

    def decorator(func: Callable):
        @functools.wraps(func)
        def wrapper(
            *, operator_config: Optional[Dict[str, Any]] = None, **script_kwargs: Any
        ) -> Union[WMFKubernetesPodOperator, BashOperator]:
            operator_kwargs = operator_config or {}
            script_args = func(**script_kwargs)

            # FIXME: Temporary hack to enable execution on both k8s and Ganeti VM
            # for different Airflow instances. Once `search` and `analytics`
            # are migrated to k8s, the BashOperator should be removed.
            if is_running_in_kubernetes():
                operator_kwargs.setdefault("image", docker_image)
                operator_kwargs.setdefault("name", func.__name__)
                operator_kwargs.setdefault(
                    "pod_template_file", airflow_pod_template_path("kubernetes_pod_operator_hadoop_pod_template")
                )

                return WMFKubernetesPodOperator(
                    cmds=[script],
                    arguments=script_args,
                    **operator_kwargs,
                )
            else:
                script_path_with_args = [
                    f"PYTHONPATH={env}",
                    "/usr/bin/python3",
                    script,
                ] + script_args

                safe_arguments = " ".join(shlex.quote(x) for x in script_path_with_args)
                bash_command = "/usr/bin/env" + " " + safe_arguments
                return BashOperator(bash_command=bash_command, **operator_kwargs)

        return wrapper

    return decorator
