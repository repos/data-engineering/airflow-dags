from datetime import timedelta

from statsd import StatsClient
from statsd.client.base import StatsClientBase


class CustomStatsClientBase(StatsClientBase):
    def timing(self, stat, delta, rate=1):
        """
        Send timing metrics as gauge. This is an override.
        """
        if isinstance(delta, timedelta):
            # Convert timedelta to number of milliseconds.
            delta = delta.total_seconds() * 1000.0
        # Original: self._send_stat(stat, '%0.6f|ms' % delta, rate)
        self._send_stat(stat, "%0.6f|g" % delta, rate)


class CustomStatsClient(StatsClient, CustomStatsClientBase):
    pass
