import logging
from datetime import datetime

from airflow import DAG
from airflow.exceptions import AirflowException
from airflow.operators.python_operator import PythonOperator

from wmf_airflow_common.clients.s3 import get_s3_client

logger = logging.getLogger(__name__)


def list_buckets():
    """Make sure that S3 can be connected to"""
    logging.info("Attempting to connect to S3 and perform a basic list bucket action")
    try:
        s3_client = get_s3_client("s3_dpe")
        s3_client.list_buckets()
    except Exception as exc:
        logging.exception("S3 connection/basic action failed", exc_info=exc)
        raise AirflowException(str(exc))
    else:
        logging.info("S3 server connection and basic list bucket action were successful")


def check_logs_for_previous_task(*args, **kwargs):
    """Check whether the previous task logs can be found in S3"""
    ti = kwargs["ti"]
    s3_path_of_upstream_task = f"dag_id={ti.dag_id}/run_id={ti.run_id}/task_id=test_s3_connection/attempt=1.log"
    s3_client = get_s3_client()
    s3_client.get_object(Bucket="logs.airflow-test-k8s.dse-k8s-eqiad", Key=s3_path_of_upstream_task)
    print(f"{s3_path_of_upstream_task} was found in S3. All good.")


with DAG(
    "test_s3_connection",
    default_args={
        "owner": "analytics",
        "depends_on_past": False,
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG that checks whether S3 can be connected to",
    start_date=datetime(2024, 10, 14),
    schedule="@hourly",
    catchup=False,
    tags=["monitoring", "s3"],
) as dag:
    list_buckets_task = PythonOperator(
        task_id="test_s3_connection",
        python_callable=list_buckets,
        dag=dag,
    )
    check_previous_task_logs = PythonOperator(
        task_id="check_previous_task_logs",
        python_callable=check_logs_for_previous_task,
        dag=dag,
    )
    list_buckets_task >> check_previous_task_logs
