from datetime import datetime

from airflow import DAG

from wmf_airflow_common.operators.skein import SimpleSkeinOperator

with DAG(
    "run_cmd_via_skein",
    default_args={
        "owner": "analytics",
        "depends_on_past": False,
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A simple DAG that uses the SimpleSkeinOperator to list files in HDFS",
    start_date=datetime(2024, 10, 23),
    schedule=None,
    catchup=False,
    tags=["integration_test", "kerberos", "skein", "hadoop"],
) as dag:
    hdfs_list_files = SimpleSkeinOperator(
        task_id="list_hdfs_files",
        script="hdfs dfs -ls /wmf/cache",
    )
    hdfs_list_files
