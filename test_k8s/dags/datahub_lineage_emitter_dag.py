"""
This DAG performs an integration test of emitting dataset lineage metadata to Datahub, through Kafka.
"""

from datetime import datetime

from airflow import DAG

from wmf_airflow_common.dataset import HiveDataset
from wmf_airflow_common.operators.datahub import DatahubLineageEmitterOperator

downstream_dataset = HiveDataset(
    table_name="wmf.aqs_hourly",
    partitioning="@hourly",
)

upstream_datasets = [
    HiveDataset(
        table_name="wmf.webrequest",
        partitioning="@hourly",
        pre_partitions=[["webrequest_source=text", "webrequest_source=upload"]],
    )
]

with DAG(
    dag_id="datahub_lineage_emitter",
    doc_md=__doc__,
    default_args={
        "owner": "analytics",
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
        "datahub_conn_id": "datahub_kafka_test",
        "datahub_environment": "TEST",
    },
    start_date=datetime(2025, 2, 11),
    schedule="@daily",
    tags=["integration_test", "datahub"],
    catchup=False,
) as dag:
    emit_lineage = DatahubLineageEmitterOperator(
        downstream=downstream_dataset, upstreams=upstream_datasets  # type: ignore
    )
    emit_lineage
