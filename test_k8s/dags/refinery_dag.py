"""
This DAG is a test for the KubernetesPodOperator and the integration with refinery.

It is intended to be used on the analytics_test airflow instance and should launch a
pod using the refinery image.

It should be able to use the hive CLI to list the tables and partitions that would
be deleted, but the operation is a noop since there is no --execute argument provided.
"""

from datetime import datetime

from airflow import DAG

from wmf_airflow_common.operators.kubernetes import WMFKubernetesPodOperator
from wmf_airflow_common.util import airflow_pod_template_path

refinery_image_name = "docker-registry.discovery.wmnet/repos/data-engineering/refinery"
refinery_image_tag = "2025-01-16-124629-dc160ba98355aab28add89f314345f6b5b633ac0"

with DAG(
    "refinery_drop_old_partitions_dry_run",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG running a refinery command in a separate Kubernetes Pod",
    doc_md=__doc__,
    start_date=datetime(2025, 1, 20),
    schedule="@daily",
    catchup=False,
    tags=["integration_test", "refinery", "kubernetes_pod_operator"],
) as dag:
    drop_partitions = WMFKubernetesPodOperator(
        task_id="drop_wmf_webrequest_partitions_dry_run",
        name="drop-wmf-webrequest-partitions-dry-run",
        pod_template_file=airflow_pod_template_path("kubernetes_pod_operator_hadoop_pod_template"),
        image=f"{refinery_image_name}:{refinery_image_tag}",
        cmds=["/opt/refinery/bin/refinery-drop-older-than"],
        arguments=[
            "--older-than=90",
            "--allowed-interval=3",
            "--verbose",
            "--database=wmf",
            "--tables=webrequest",
        ],
    )
    drop_partitions
