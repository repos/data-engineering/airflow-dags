from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from wmf_airflow_common.tasks.delete_old_task_logs import purge_old_logs_from_s3

with DAG(
    "purge_old_logs_from_s3",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG that cleans up task logs stored in S3 older than a configurable retention",
    start_date=datetime(2024, 9, 26),
    schedule="@daily",
    catchup=False,
    tags=["maintenance", "s3"],
) as dag:
    task = PythonOperator(
        task_id="purge_old_logs_from_s3",
        python_callable=purge_old_logs_from_s3,
        dag=dag,
    )
    task
