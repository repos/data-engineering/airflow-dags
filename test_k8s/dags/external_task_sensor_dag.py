from datetime import datetime, timedelta

from airflow import DAG

from wmf_airflow_common.sensors.rest_external_task import RestExternalTaskSensor

with DAG(
    dag_id="external_task_sensor",
    description="A DAG using RestExternalTaskSensor to check that a DAG executed successfully via the Airflow API",
    start_date=datetime(2024, 11, 16),
    schedule="@daily",
    catchup=False,
    tags=["integration_test", "kerberos", "airflow_api"],
) as dag:
    sensor_with_kerberos_auth = RestExternalTaskSensor(
        task_id="sensor_airflow_test_k8s_purge_old_logs_from_s3",
        external_instance_uri="https://airflow-test-k8s.discovery.wmnet:30443",
        external_dag_id="purge_old_logs_from_s3",
        check_existence=True,
        allowed_states=["success"],
        execution_delta=timedelta(days=1),
        airflow_api_kerberos_enabled=True,
    )
    sensor_with_kerberos_auth
