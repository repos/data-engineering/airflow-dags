"""
This dag is in charge of creating a Pod via the Kubernetes API, itself
requesting a resource within the service mesh.
"""

from datetime import datetime

from airflow import DAG

from wmf_airflow_common.operators.kubernetes import WMFKubernetesPodOperator

docker_image = "docker-registry.discovery.wmnet/repos/data-engineering/airflow"
docker_tag = "ebd2d6ee4509445c05572ab5fa6e6afb588fb285-production"

# cf https://gerrit.wikimedia.org/r/plugins/gitiles/operations/puppet/+/37f0dde4ced71922d85cfe6399cc638a2b3796de/hieradata/common/profile/services_proxy/envoy.yaml#311 # noqa: E501, W505
wmf_api_mesh_port = 6501
wmf_api_mesh_url = (
    f"http://envoy:{wmf_api_mesh_port}/w/api.php?action=streamconfigs&constraints=canary_events_enabled=1"
)


with DAG(
    dag_id="request_internal_endpoint_via_service_mesh",
    doc_md=__doc__,
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
    },
    start_date=datetime(2025, 1, 28),
    tags=["integration_test", "kubernetes_pod_operator", "service_mesh"],
    catchup=False,
) as dag:
    request_mesh_endpoint = WMFKubernetesPodOperator(
        task_id="request_endpoint_via_service_mesh",
        name="wmf-api-requester",
        image=f"{docker_image}:{docker_tag}",
        cmds=["python3"],
        arguments=[
            "-c",
            'import requests;print(requests.get("%s",headers={"Host": "meta.wikimedia.org"}))' % (wmf_api_mesh_url),
        ],
    )

    request_mesh_endpoint
