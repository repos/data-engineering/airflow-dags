from datetime import datetime

from airflow import DAG
from airflow.operators.bash import BashOperator

with DAG(
    "Datahub_Cleanup",
    default_args={
        "owner": "analytics",
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG that cleans up deleted tasks and pipelines from Datahub",
    start_date=datetime(2024, 10, 16),
    schedule="@weekly",
    catchup=False,
    tags=["maintenance", "datahub"],
) as dag:
    task = BashOperator(
        task_id="cleanup_obsolete_data",
        dag=dag,
        bash_command="echo 'cleaning up the obsolete data from datahub'",
    )
    task
