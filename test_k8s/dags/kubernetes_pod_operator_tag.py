"""
This dag is in charge of creating a Pod via the Kubernetes API, to run a specific command,
in a custom image.
"""

from datetime import datetime

from airflow import DAG

from wmf_airflow_common.operators.kubernetes import WMFKubernetesPodOperator

docker_image = "docker-registry.discovery.wmnet/repos/data-engineering/airflow"
docker_tag = "ebd2d6ee4509445c05572ab5fa6e6afb588fb285-production"

with DAG(
    dag_id="k8s_pod_operator",
    doc_md=__doc__,
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
    },
    start_date=datetime(2025, 1, 10),
    tags=["integration_test", "kubernetes_pod_operator"],
    catchup=False,
) as dag:
    run_k8s_pod = WMFKubernetesPodOperator(
        task_id="run-cat-os-release",
        name="run-cat-os-release",
        # the container can run anything, not just python code
        image=f"{docker_image}:{docker_tag}",
        cmds=["/usr/bin/cat"],
        arguments=["/etc/os-release"],
    )

    run_k8s_pod
