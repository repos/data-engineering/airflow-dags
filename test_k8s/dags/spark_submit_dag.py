import os
from datetime import datetime

from airflow import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator

with DAG(
    "spark_submit_pi_yarn_cluster_mode",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "email_on_failure": False,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG that submits a simple Spark job to YARN, in cluster deploy mode",
    start_date=datetime(2024, 11, 7),
    schedule="@daily",
    catchup=False,
    tags=["integration_test", "kerberos", "spark"],
) as dag:
    task = SparkSubmitOperator(
        task_id="spark_pi",
        name="spark_pi",
        conn_id="spark_yarn_cluster",
        application=f"{os.environ['SPARK_HOME']}/examples/src/main/python/pi.py",
        application_args=["10"],
    )
    task
