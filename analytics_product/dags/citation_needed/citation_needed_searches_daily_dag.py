"""
Get the daily counts of searches made by the experimental Citation Needed API
and insert it into the storage table
"""

from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    product_analytics_alerts_email,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "citation_needed_searches_daily"

props = DagProperties(
    # dag start date
    start_date=datetime(2024, 3, 1),
    # path to query file
    cx_query_file="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/28d961ef773753e964bdcf6c4819cb1eabf96f75/citation_needed/searches/citation_needed_searches_daily.hql",
    # sources
    source_table="event.mediawiki_cirrussearch_request",
    # destinations
    destination_table="wmf_product.citation_needed_searches_daily",
    # various
    coalesce_partitions="1",
    # SLA and alert email
    dag_sla=timedelta(days=1),
    alerts_email=product_analytics_alerts_email,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=["daily", "uses_hql", "from_hive", "to_iceberg", "requires_event_mediawiki_cirrussearch_request", "future_audiences", "citation_needed_experiment"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # check if partition for the day is available
    sensor = dataset("hive_event_mediawiki_cirrussearch_request").get_sensor_for(dag)

    # count the searches
    compute = SparkSqlOperator(
        task_id="compute_citation_needed_searches_daily",
        sql=f"{props.cx_query_file}",
        query_parameters={
            "source_table": props.source_table,
            "destination_table": props.destination_table,
            "target_day": "{{ data_interval_start.day }}",
            "target_month": "{{ data_interval_start.month }}",
            "target_year": "{{ data_interval_start.year }}",
            "coalesce_partitions": props.coalesce_partitions,
        },
    )

    sensor >> compute
