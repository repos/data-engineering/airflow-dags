"""
Waits for mediawiki_history and mediawiki_logging Hive partitions.
Get the monthly counts of deleted pages, reverted edits, protected pages, and edit rollbacks; insert them into the storage table.
"""

from datetime import datetime, timedelta
from airflow import DAG

from analytics_product.config.dag_config import (
    create_easy_dag,
    dataset,
    product_analytics_alerts_email,
    hadoop_name_node,
)

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.url import URLSensor

dag_id = "tsp_admin_action_monthly"
mediawiki_snapshot = "{{data_interval_start | to_ds_month}}"

props = DagProperties(
    # dag start date
    start_date=datetime(2024, 9, 1),
    # path to query file
    query_file="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/4d71234e9e1988d82f718e007c04883e10fba7cd/trust_safety_metrics/generate_admin_action_monthly.hql",
    # sources
    source_history_table="wmf.mediawiki_history",
    source_logging_table="wmf_raw.mediawiki_logging",
    canonical_table="canonical_data.wikis",
    # destinations
    destination_table="wmf_product.trust_safety_admin_action_monthly",
    # various
    coalesce_partitions="1",
    # SLA and alert email
    dag_sla=timedelta(days=10),
    alerts_email=product_analytics_alerts_email,
    # spark config (defaults fail, these settings have been successfully tested)
    driver_cores=2,
    driver_memory="2G",
    executor_cores=4,
    executor_memory="8G",
    max_executors=64,
    memory_overhead=2048,
)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "uses_hql", "from_hive", "to_iceberg", "requires_wmf_mediawiki_history", "requires_wmf_raw_mediawiki_logging", "trust_safety_metrics"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    sensors = []
    sensors.append(dataset("hive_wmf_mediawiki_history").get_sensor_for(dag))
    sensors.append(URLSensor(
            task_id="wait_for_logging",
            url=(
                f"{hadoop_name_node}/wmf/data/raw/mediawiki/tables/logging"
                f"/snapshot={mediawiki_snapshot}/_PARTITIONED"
            ),
            poke_interval=timedelta(hours=1).total_seconds(),
        )
    )

    compute = SparkSqlOperator(
        task_id="compute_tsp_admin_action_monthly",
        sql=f"{props.query_file}",
        query_parameters={
            "source_history_table": props.source_history_table,
            "source_logging_table": props.source_logging_table,
            "destination_table": props.destination_table,
            "canonical_table": props.canonical_table,
            "snapshot": mediawiki_snapshot,
            "coalesce_partitions": props.coalesce_partitions,
        },
        driver_cores=props.driver_cores,
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.yarn.executor.memoryOverhead": props.memory_overhead
        }
    )

    sensors >> compute


