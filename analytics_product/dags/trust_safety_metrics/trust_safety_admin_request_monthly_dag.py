"""
This DAG computes the trust and safety admin request monthly data from 
the mediawiki_private_cu_log table, and writes it to the wmf_product.trust_safety_admin_request_monthly table
"""

from datetime import datetime, timedelta

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    hadoop_name_node,
)
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

dag_id = "trust_safety_admin_request_monthly"

props = DagProperties(
    # dag start date
    start_date=datetime(2024, 12, 9),
    # path to query file
    query_file = "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/main/trust_safety_metrics/generate_admin_request_monthly_table.hql",
    base_directory = "/wmf/data/wmf_product/trust_safety_metrics",
    # destination table
    destination_table = "wmf_product.trust_safety_admin_request_monthly",
    # source tables
    source_logging_table = "wmf_raw.mediawiki_private_cu_log",
    canonical_table = "canonical_data.wikis",
    # get mediawiki_private_cu_log partition
    partition = "{{data_interval_start | to_ds_month}}",
    # SLA and alert email
    dag_sla=timedelta(days=1),
    alerts_email=product_analytics_alerts_email,

)

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "uses_hql", "from_hive", "to_iceberg", "requires_wmf_raw_mediawiki_private_cu_log"],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:

    # check if partition for the month is available
    wait_for_mediawiki_private_cu_log_export = URLSensor(
        task_id="wait_for_mediawiki_private_cu_log_export",
        url=f"{hadoop_name_node}/wmf/data/raw/mediawiki_private/tables/cu_log/month={props.partition}/_PARTITIONED",
        poke_interval=timedelta(hours=1).total_seconds()
    )

    # generate table
    etl = SparkSqlOperator(
        task_id=f"generate_trust_safety_admin_request_monthly_data",
        sql=f"{props.query_file}",
        query_parameters={
            "destination_table": props.destination_table,
            "snapshot": "{{ data_interval_start | to_ds_month }}",
            "source_logging_table": props.source_logging_table,
            "canonical_table": props.canonical_table,
            "coalesce_partitions": 1,
        },
        driver_memory="4G",
        driver_cores=2,
        executor_memory="4G",
        executor_cores=2,
        conf={
            "spark.dynamicAllocation.maxExecutors": 32,
            "spark.executor.memoryOverhead": "2G",
        },
    )

    wait_for_mediawiki_private_cu_log_export >> etl
