"""
Fetches Automoderator's activity snapshot from mediawiki_history, monthly.

Steps:
    - waits for mediawiki_history snapshot to be released.
    - fetches and process activity related to Automoderator on wiki where it is deployed on, based on config.
    - each snapshot is appended to wmf_product.automoderator_activity_snapshot_monthly.
    - a latest snapshot is published as TSV to https://analytics.wikimedia.org/published/datasets/
    - snapshots older than 3 months are purged from the destination table.

Variables for testing: 
{
  "start_date": "2024-08-01T00:00:00",
  "sla": "P5D",
  "alerts_email": "kcvelaga@wikimedia.org",
  "catchup": true,
  "generate_snapshots_query": "https://w.wiki/BNdX",
  "create_tsv_query": "https://w.wiki/BNda",
  "purge_snapshots_query": "https://w.wiki/BNde",
  "automod_config_table": "wmf_product.automoderator_config",
  "automod_small_wikis_table": "wmf_product.automoderator_small_wikis_classification",
  "source_mwh_table": "wmf.mediawiki_history",
  "snapshot_destination_table": "kcvelaga.automoderator_activity_snapshot_monthly",
  "coalesce_partitions": "1",
  "tmp_dir_path": "hdfs://analytics-hadoop/tmp/kcvelaga/automoderator/monthly_snapshot",
  "web_output_path": "hdfs://analytics-hadoop/tmp/kcvelaga/automoderator/monthly_snaphot_archive/snapshot.tsv.bz2",
  "driver_cores": 4,
  "driver_memory": "8G",
  "driver_max_result_size": "16G",
  "executor_cores": 16,
  "executor_memory": "8G",
  "max_executors": 64,
  "memory_overhead": "8G"
}
"""

from datetime import datetime, timedelta

from airflow.operators.dummy import DummyOperator
from airflow.models.baseoperator import chain

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    artifact,
    hadoop_name_node,
    hdfs_temp_directory,
    dataset
)

from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator

dag_id = "automoderator_activity_snapshot_monthly"
mwh_snapshot = "{{ data_interval_start | to_ds_month }}"

props = DagProperties(

    start_date=datetime(2024, 8, 1),
    sla=timedelta(days=5),
    alerts_email=product_analytics_alerts_email,
    catchup=True,

    generate_snapshots_query = (
        "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/"
        "b8173f6b744c7211e4dd8c1ba6a004dc12afd722/"
        "automoderator/generate_automoderator_activity_snapshot_monthly.hql"
    ),
    create_tsv_query=(
        "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/"
        "b8173f6b744c7211e4dd8c1ba6a004dc12afd722/"
        "automoderator/publish_automoderator_activity_snapshot_monthly.hql"
    ),
    purge_snapshots_query=(
        "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/raw/"
        "b8173f6b744c7211e4dd8c1ba6a004dc12afd722/"
        "automoderator/purge_automoderator_activity_snapshot_monthly.hql"
    ),

    automod_config_table="wmf_product.automoderator_config",
    automod_small_wikis_table="wmf_product.automoderator_small_wikis_classification",
    source_mwh_table="wmf.mediawiki_history",
    snapshot_destination_table="wmf_product.automoderator_activity_snapshot_monthly",
    coalesce_partitions="1",

    tmp_dir_path=f"{hadoop_name_node}{hdfs_temp_directory}/automoderator/monthly_snapshot",
    web_output_path=f"{hadoop_name_node}/wmf/data/published/datasets/automoderator/monthly_snapshot/snapshot.tsv.bz2",

    # defaults fail, these have been tested; only required for generation step
    driver_cores=4,
    driver_memory="8G",
    driver_max_result_size="16G",
    executor_cores=16,
    executor_memory="8G",
    max_executors=64,
    memory_overhead="8G",
) 

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "uses_hql", "from_hive", "to_iceberg", "requires_wmf_mediawiki_history", "automoderator", "web_publication"],
    sla=props.sla,
    email=props.alerts_email,
    max_active_runs=1,
    catchup=props.catchup,
) as dag:

    operators = []

    mwh_sensor = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)
    operators.append(mwh_sensor)

    generate_snapshots = SparkSqlOperator(
        task_id="generate_automoderator_snapshot_monthly",
        sql=f"{props.generate_snapshots_query}",
        query_parameters={
            "automod_config_table": props.automod_config_table,
            "automod_small_wikis_table": props.automod_small_wikis_table,
            "source_mwh_table": props.source_mwh_table,
            "destination_table": props.snapshot_destination_table,
            "snapshot": mwh_snapshot,
            "coalesce_partitions": props.coalesce_partitions,
        },
        driver_cores=props.driver_cores,
        driver_memory=props.driver_memory,
        executor_cores=props.executor_cores,
        executor_memory=props.executor_memory,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.yarn.executor.memoryOverhead": props.memory_overhead,
            "spark.yarn.driver.maxResultSize": props.driver_max_result_size
        }
    )
    operators.append(generate_snapshots)

    write_snapshot_tsv = SparkSqlOperator(
        task_id="write_automoderator_snapshot_tsv",
        sql=f"{props.create_tsv_query}",
        query_parameters={
            # the source for creating the TSV is the table containing the snapshot 
            "source_table": props.snapshot_destination_table,
            "tmp_directory": props.tmp_dir_path,
            "snapshot": mwh_snapshot
        }
    )
    operators.append(write_snapshot_tsv)

    publish_snapshot_tsv = HDFSArchiveOperator(
        task_id="publish_automoderator_snapshot_tsv",
        source_directory=props.tmp_dir_path,
        archive_file=props.web_output_path,
        expected_filename_ending=".bz2",
        check_done=True
    )
    operators.append(publish_snapshot_tsv)

    purge_snapshots = SparkSqlOperator(
        task_id="purge_automoderator_snaphosts_3M_old",
        sql=f"{props.purge_snapshots_query}",
        query_parameters={
            "source_table": props.snapshot_destination_table,
            "snapshot": mwh_snapshot
        }
    )
    operators.append(purge_snapshots)

    complete=DummyOperator(task_id="complete")
    operators.append(complete)

    chain(*operators)