from os import environ, path
from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.dataset import DatasetRegistry
from wmf_airflow_common.easy_dag import EasyDAGFactory
from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = 'hdfs://analytics-hadoop'
hdfs_temp_directory = '/wmf/tmp/analytics_product'

product_analytics_alerts_email = 'product-analytics+alerts@wikimedia.org'
movement_insights_alerts_email = 'movement-insights+alerts@wikimedia.org'

# default_args for this Airflow Instance.
instance_default_args = {
    'owner': 'analytics-product',
    'hadoop_name_node': hadoop_name_node,
    'metastore_conn_id': 'analytics-hive',
}

# config for jobs that require read or write to the AQS Cassandra cluster.
cassandra_default_conf = dag_default_args.cassandra_default_conf

# Config for jobs that ingest to the Druid cluster.
druid_default_conf = dag_default_args.druid_default_conf

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args['queue'] = 'production'

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in DAGs.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance('analytics_product')
artifact = artifact_registry.artifact_url

# Dataset registry for easier Sensor configuration.
# Provides a syntactic sugar to use when creating Sensors in DAGs.
dataset_file_paths = [path.join(path.dirname(__file__), "datasets.yaml")]
dataset_registry = DatasetRegistry(dataset_file_paths)
dataset = dataset_registry.get_dataset

_extra_default_args = {
    **instance_default_args,
    # Arguments for operators that have artifact dependencies
    'hdfs_tools_shaded_jar_path': artifact('hdfs-tools-0.0.6-shaded.jar'),
    'spark_sql_driver_jar_path': artifact('wmf-sparksqlclidriver-1.0.0.jar'),
}

# For the development instance, set the owner to $USER.
if not is_wmf_airflow_instance():
    user = environ.get("USER")
    if user:
        _extra_default_args["owner"] = user

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get(_extra_default_args)

# Helper for creating DAGs with a nicer interface.
# Passes all the defaults and shortcuts to the DAG factory.
create_easy_dag = EasyDAGFactory(
    factory_default_args = {
        **default_args,
        **cassandra_default_conf,
        **druid_default_conf,
    },
    factory_default_args_shortcuts=["email", "sla"],
    factory_user_defined_filters=filters,
).create_easy_dag
